import numpy as np
from q1wine import *
from q1breast import *


class lr(object):

    def __init__(self, alpha=0.0001, max_cycle=10000):
        self.alpha = alpha
        self.max_cycle = max_cycle
               
    def sig(self, z):
        sigmoid = 1 / (1 + np.exp(-z))
        sigmoid = np.mat(sigmoid)
        return sigmoid

    def fit(self, features, label):
        s = len(features[0])
        sta = np.mat(np.ones((s, 1)))
        i = 0
        while i < self.max_cycle:
            sta = sta + self.alpha * (np.transpose(features) * (label - self.sig(features * sta)))
            i = i + 1
        return sta

    def predict(self, w, feature_data):
        x = w * feature_data
        h = self.sig(x)
        g = h.shape[0]
        for i in range(0, g):
            if h[i] > 0.5:
                h[i] = 1
            elif h[i] < 0.5:
                h[i] = 0
        return h

'''
def evaluate_acc(h, label_data):
    acc_num = 0
    numbel = label_data.shape[0]
    for i in range(0, numbel):
        if h[i] == label_data[i]:
            acc_num = acc_num+1
    acc_num = acc_num/numbel
    return acc_num


if __name__ == "__main__":

    print("-----1.load data -----")
    #x, y = get_redwine()
    #feature_data = x[1:1000]
    #label_data = y[1:1000]
    #text_data = x[1000:]
    #text_label_data = y[1000:]

    x, y = get_cancer()
    #print(x.shape)
    feature_data = x[1:500]
    label_data = y[1:500]
    text_data = x[500:]
    text_label_data = y[500:]

    print("-----2.training------")
    w = fit(feature_data, label_data, 10900, 0.0001)
    print("------3.model---------")
    h = predict(w, text_data)
    print(w)
    print("-------4.accuracy value-----")
    acc = evaluate_acc(h, text_label_data)
    print(acc)
'''

