import numpy as np

class lda():

    def __init__(self, X, y):
        self.X = X
        self.y = y
        self.n0 = 0
        self.n1 = 0
        self.w0 = 0
        self.w = 0

    def fit(self):
        for i in self.y:

            if int(i) == 0:
                self.n0 += 1
            else:
                self.n1 += 1

        p0 = self.n0 / (self.n0 + self.n1)
        p1 = self.n1 / (self.n0 + self.n1)

        s0 = np.zeros((1, len(self.X[0])))
        s1 = np.zeros((1, len(self.X[0])))

        for i in range(len(self.X)):
            if int(self.y[i]) == 0:
                s0 = np.add(s0, self.X[i])
            else:
                s1 = np.add(s1, self.X[i])

        u0 = s0 / self.n0
        u1 = s1 / self.n1

        u0 = np.transpose(u0)
        u1 = np.transpose(u1)

        result = 0
        for i in range(len(self.X)):

            if int(self.y[i]) == 0:
                result += np.add(self.X[i].reshape(-1, 1), -u0).dot(np.transpose(np.add(self.X[i].reshape(-1, 1), -u0)))
            else:
                result += np.add(self.X[i].reshape(-1, 1), -u1).dot(np.transpose(np.add(self.X[i].reshape(-1, 1), -u1)))
        sigma = np.divide(result, (self.n0 + self.n1 - 2))

        self.w0 = np.log(p1/p0) - float(np.transpose(u1).dot(np.linalg.inv(sigma)).dot(u1) / 2) + \
                  float(np.transpose(u0).dot(np.linalg.inv(sigma)).dot(u0) / 2)

        self.w = np.linalg.inv(sigma).dot((u1 - u0))


    def predict(self, X):
        result = []
        for x in X:

            if float(np.transpose(x).dot(self.w)+self.w0) > 0:
                result.append(1)
            else:
                result.append(0)

        return result


