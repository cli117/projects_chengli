/*
----------------- COMP 310/ECSE 427 Winter 2018 -----------------
I declare that the awesomeness below is a genuine piece of work
and falls under the McGill code of conduct, to the best of my knowledge.
-----------------------------------------------------------------
*/

//Please enter your name and McGill ID below
//Name: Cheng Li
//McGill ID: 260706615

#include <stdio.h>
#include <unistd.h>

int main(){
    //Create a char array to store the ls information
    char ls[1000];
    int stdoutDup = dup(1);
    int f[2];
    pipe(f);
    if(fork() == 0){
        //This is child
        //Redirect output
        dup2(f[1],1);
        //Executing ls using execvp
        char *argv[] = {"ls", 0};
        execvp("ls", argv);
        close(f[1]);
    }
    else{
        //Read output from pipe
        read(f[0],ls,1000);
        //Then print out
        printf("%s",ls);
        close(f[0]);
    }
    return 0;
}
