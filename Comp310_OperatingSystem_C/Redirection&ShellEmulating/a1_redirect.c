/*
----------------- COMP 310/ECSE 427 Winter 2018 -----------------
I declare that the awesomeness below is a genuine piece of work
and falls under the McGill code of conduct, to the best of my knowledge.
-----------------------------------------------------------------
*/

//Please enter your name and McGill ID below
//Name: Cheng Li
//McGill ID: 260706615

#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>

int main(){
    //Print to stdout
    printf("First : Print to stdout\n");
    //Save stdout
    int stdoutDup = dup(1);
    //Create redirect_out.txt
    int fp = open("redirect_out.txt", O_CREAT|O_RDWR);
    //Redirection
    dup2(fp,1);
    //Write to file
    printf("Second : Print to redirect_out.txt\n");
    //Close file
    close(fp);
    //Restore stdout
    dup2(stdoutDup,1);
    //Print to stdout
    printf("Third : Print to stdout\n");
    //Close
    close(stdoutDup);
    return 0;
}
