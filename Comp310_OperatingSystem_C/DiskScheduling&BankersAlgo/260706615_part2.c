#include <stdio.h>  //for printf and scanf
#include <stdlib.h> //for malloc

#define LOW 0
#define HIGH 199
#define START 53
//compare function for qsort
//you might have to sort the request array
//use the qsort function
// an argument to qsort function is a function that compares 2 quantities
//use this there.
int cmpfunc (const void * a, const void * b) {
   return ( *(int*)a - *(int*)b );
}

//function to swap 2 integers
void swap(int *a, int *b)
{
    if (*a != *b)
    {
        *a = (*a ^ *b);
        *b = (*a ^ *b);
        *a = (*a ^ *b);
        return;
    }
}

//Prints the sequence and the performance metric
void printSeqNPerformance(int *request, int numRequest)
{
    int i, last, acc = 0;
    last = START;
    printf("\n");
    printf("%d", START);
    for (i = 0; i < numRequest; i++)
    {
        printf(" -> %d", request[i]);
        acc += abs(last - request[i]);
        last = request[i];
    }
    printf("\nPerformance : %d\n", acc);
    return;
}

//access the disk location in FCFS
void accessFCFS(int *request, int numRequest)
{
    //simplest part of assignment
    printf("\n----------------\n");
    printf("FCFS :");
    printSeqNPerformance(request, numRequest);
    printf("----------------\n");
    return;
}

//access the disk location in SSTF
void accessSSTF(int *request, int numRequest)
{
    //write your logic here
    int difference, currentPosition = START, minSoFar = 200;
    for(int i = 0; i < numRequest; i++){
        for(int j = i; j < numRequest; j++){
            difference = abs(request[j] - currentPosition);
            if(difference < minSoFar){
                minSoFar = difference;
                swap(&request[i], &request[j]);
            }
        }
        minSoFar = 200;
        currentPosition = request[i];
    }
    printf("\n----------------\n");
    printf("SSTF :");
    printSeqNPerformance(request, numRequest);
    printf("----------------\n");
    return;
}

//access the disk location in SCAN
void accessSCAN(int *request, int numRequest)
{

	//write your logic here
    int direction, markSmaller, markLarger, newIndex = 0,
    *newRequest = malloc((numRequest + 1) * sizeof(int)),newCnt = numRequest;

    //If LOW end is closer, set direction to -1
    if(abs(START - LOW) < abs(START - HIGH)) direction = -1;
    //otherwise, set it to 1
    else direction = 1;

    //First, sort the array
    for(int i = 0; i < numRequest; i++){
        for(int j = i; j < numRequest; j++){
            if(cmpfunc(&request[i], &request[j]) > 0){
                swap(&request[i], &request[j]);
            }
        }
    }
    //Array sorted, find the index of two points near the START
    for(int i = 0; i < numRequest; i++){
        if(request[i] <= START) markSmaller = i;
        else{
            markLarger = i;
            break;
        }
    }

    //If LOW end is closer(i.e. go left first)
    if(direction == -1){
        for(int i = markSmaller; i > -1; i--){
            newRequest[newIndex] = request[i];
            newIndex++;
        }
            newRequest[newIndex] = LOW;
            newIndex++;
            newCnt++;
    //Turn around continue scan
        for(int i = markLarger; i < numRequest; i++){
            newRequest[newIndex] = request[i];
            newIndex++;
        }
    }
    //Else, go right first
    else{
        for(int i = markLarger; i < numRequest; i++){
            newRequest[newIndex] = request[i];
            newIndex++;
        }
            newRequest[newIndex] = HIGH;
            newIndex++;
            newCnt++;
    //Turn around continue scan
    for(int i = markSmaller; i > -1; i--){
            newRequest[newIndex] = request[i];
            newIndex++;
        }
    }



    printf("\n----------------\n");
    printf("SCAN :");
    printSeqNPerformance(newRequest, newCnt);
    printf("----------------\n");
    return;
}

//access the disk location in CSCAN
void accessCSCAN(int *request, int numRequest)
{
    //write your logic here
    int direction, markSmaller, markLarger, newIndex = 0,
    *newRequest = malloc((numRequest + 2) * sizeof(int)),newCnt = numRequest;

    //If LOW end is closer, set direction to -1
    if(abs(START - LOW) < abs(START - HIGH)) direction = -1;
    //otherwise, set it to 1
    else direction = 1;

    //First, sort the array
    for(int i = 0; i < numRequest; i++){
        for(int j = i; j < numRequest; j++){
            if(cmpfunc(&request[i], &request[j]) > 0){
                swap(&request[i], &request[j]);
            }
        }
    }

    //Array sorted, find the index of two points near the START
    for(int i = 0; i < numRequest; i++){
        if(request[i] <= START) markSmaller = i;
        else{
            markLarger = i;
            break;
        }
    }
    //If LOW end is closer(i.e. go left first)
    if(direction == -1){
        for(int i = markSmaller; i > -1; i--){
            newRequest[newIndex] = request[i];
            newIndex++;
        }
    //Till the end
            newRequest[newIndex] = LOW;
            newIndex++;
    //Flyback
            newRequest[newIndex] = HIGH;
            newIndex++;
    //Then newCnt should be increased by 2
            newCnt = newCnt + 2;
    //Points remaining
        for(int i = numRequest - 1; i > markSmaller; i--){
            newRequest[newIndex] = request[i];
            newIndex++;
        }
    }

    //Else, HIGH is closer
    else{
        for(int i = markLarger; i < numRequest; i++){
            newRequest[newIndex] = request[i];
            newIndex++;
        }
    //Till the end
        newRequest[newIndex] = HIGH;
        newIndex++;
    //Flyback
        newRequest[newIndex] = LOW;
        newIndex++;
    //Then newCnt should be increased by 2
        newCnt = newCnt + 2;
    //Points remaining
        for(int i = 0; i < markLarger; i++){
            newRequest[newIndex] = request[i];
            newIndex++;
        }
    }

    printf("\n----------------\n");
    printf("CSCAN :");
    printSeqNPerformance(newRequest, newCnt);
    printf("----------------\n");
    return;
}

//access the disk location in LOOK
void accessLOOK(int *request, int numRequest)
{
    //write your logic here
    int direction, markSmaller, markLarger, newIndex = 0,
    *newRequest = malloc(numRequest * sizeof(int)),newCnt = numRequest;

    //If LOW end is closer, set direction to -1
    if(abs(START - LOW) < abs(START - HIGH)) direction = -1;
    //otherwise, set it to 1
    else direction = 1;

    //First, sort the array
    for(int i = 0; i < numRequest; i++){
        for(int j = i; j < numRequest; j++){
            if(cmpfunc(&request[i], &request[j]) > 0){
                swap(&request[i], &request[j]);
            }
        }
    }

    //Array sorted, find the index of two points near the START
    for(int i = 0; i < numRequest; i++){
        if(request[i] <= START) markSmaller = i;
        else{
            markLarger = i;
            break;
        }
    }
    //If LOW end is closer(i.e. go left first)
    if(direction == -1){
        for(int i = markSmaller; i > -1; i--){
            newRequest[newIndex] = request[i];
            newIndex++;
        }
        for(int i = markLarger; i < numRequest; i++){
            newRequest[newIndex] = request[i];
            newIndex++;
        }
    }

    else{
        for(int i = markLarger; i < numRequest; i++){
            newRequest[newIndex] = request[i];
            newIndex++;
        }
        for(int i = markSmaller; i > -1; i--){
            newRequest[newIndex] = request[i];
            newIndex++;
        }
    }

    printf("\n----------------\n");
    printf("LOOK :");
    printSeqNPerformance(newRequest, newCnt);
    printf("----------------\n");
    return;
}

//access the disk location in CLOOK
void accessCLOOK(int *request, int numRequest)
{
    //write your logic here
    int direction, markSmaller, markLarger, newIndex = 0,
    *newRequest = malloc((numRequest + 1) * sizeof(int)),newCnt = numRequest;

    //If LOW end is closer, set direction to -1
    if(abs(START - LOW) < abs(START - HIGH)) direction = -1;
    //otherwise, set it to 1
    else direction = 1;

    //First, sort the array
    for(int i = 0; i < numRequest; i++){
        for(int j = i; j < numRequest; j++){
            if(cmpfunc(&request[i], &request[j]) > 0){
                swap(&request[i], &request[j]);
            }
        }
    }

    //Array sorted, find the index of two points near the START
    for(int i = 0; i < numRequest; i++){
        if(request[i] <= START) markSmaller = i;
        else{
            markLarger = i;
            break;
        }
    }
    //If LOW end is closer(i.e. go left first)
    if(direction == -1){
        for(int i = markSmaller; i > -1; i--){
            newRequest[newIndex] = request[i];
            newIndex++;
        }
    //Flyback
        newRequest[newIndex] = HIGH;
        newIndex++;
        newCnt++;
        for(int i = numRequest - 1; i > markSmaller; i--){
            newRequest[newIndex] = request[i];
            newIndex++;
        }
    }

    else{
        for(int i = markLarger; i < numRequest; i++){
            newRequest[newIndex] = request[i];
            newIndex++;
        }
    //Flyback
        newRequest[newIndex] = LOW;
        newIndex++;
        newCnt++;
        for(int i = 0; i < markLarger; i++){
            newRequest[newIndex] = request[i];
            newIndex++;
        }
    }
    printf("\n----------------\n");
    printf("CLOOK :");
    printSeqNPerformance(newRequest,newCnt);
    printf("----------------\n");
    return;
}

int main()
{
    int *request, numRequest, i,ans;

    //allocate memory to store requests
    printf("Enter the number of disk access requests : ");
    scanf("%d", &numRequest);
    request = malloc(numRequest * sizeof(int));

    printf("Enter the requests ranging between %d and %d\n", LOW, HIGH);
    for (i = 0; i < numRequest; i++)
    {
        scanf("%d", &request[i]);
    }

    printf("\nSelect the policy : \n");
    printf("----------------\n");
    printf("1\t FCFS\n");
    printf("2\t SSTF\n");
    printf("3\t SCAN\n");
    printf("4\t CSCAN\n");
    printf("5\t LOOK\n");
    printf("6\t CLOOK\n");
    printf("----------------\n");
    scanf("%d",&ans);

    switch (ans)
    {
    //access the disk location in FCFS
    case 1: accessFCFS(request, numRequest);
        break;

    //access the disk location in SSTF
    case 2: accessSSTF(request, numRequest);
        break;

        //access the disk location in SCAN
     case 3: accessSCAN(request, numRequest);
        break;

        //access the disk location in CSCAN
    case 4: accessCSCAN(request,numRequest);
        break;

    //access the disk location in LOOK
    case 5: accessLOOK(request,numRequest);
        break;

    //access the disk location in CLOOK
    case 6: accessCLOOK(request,numRequest);
        break;

    default:
        break;
    }
    return 0;
}
