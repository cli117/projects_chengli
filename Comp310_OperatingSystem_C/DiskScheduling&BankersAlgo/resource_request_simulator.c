int numOfProcesses, numOfRecources, *availableRecources, *request_vector, *maxResClaim, *hold, *need;


//Simulates resource requests by processes
void request_simulator(int pr_id, int* request_vector){
    for(int i = 0; i < numOfRecources; i++){
            request_vector[i] = rand()%(need[pr_id*numOfRecources + i]+1);
    }
}


//Simulates processes running on the system.
void* process_simulator(void* pr_id){
    for(int i = 0; i < numOfRecources; i++){
        need[*(int *)pr_id*numOfRecources + i] = maxResClaim[*(int *)pr_id*numOfRecources + i];
        hold[*(int *)pr_id*numOfRecources + i] = 0;
    }
    while(1){
        int isDone = 0;
        for(int i = 0; i < numOfRecources; i++){
            if(need[*(int *)pr_id*numOfRecources + i] != 0) isDone++;
        }
        if(isDone == 0){
            for(int i = 0; i < numOfRecources; i++){
                availableRecources[i] += hold[*(int *)pr_id*numOfRecources + i];
            }
            break;
        }
        else{
            request_simulator(*(int *)pr_id, request_vector);
            bankers_algorithm(*(int *)pr_id, request_vector);
        }
    }

}


//Implementation of Bankers Algorithm as described in the slides
//returns 1 if safe allocation 0 if not safe

int bankers_algorithm(int pr_id, int* request_vector){
    printf("The Available Resources array is : ");
    for(int i = 0; i < numOfRecources; i++){
        printf("%d ", availableRecources[i]);
    }
    printf("\nRequesting resources for process %d\nThe Resource vector requested array is : ", pr_id);
    for(int i = 0; i < numOfRecources; i++){
        printf("%d ", request_vector[i]);
    }

    while(1){
        for(int i = 0; i < numOfRecources; i++){
            //Check if there are illegal requests
            if(request_vector[i] > need[pr_id*numOfRecources + i]){
                printf("\nProcess %d's request is illegal!\n", pr_id);
                exit(-1);
            }
            //If no enough resources, wait and check from the very beginning
            if(request_vector[i] > availableRecources[i]){
                sleep(3);
                i = 0;
            }
        }
        for(int i = 0; i < numOfRecources; i++){
            availableRecources[i] -= request_vector[i];
            hold[pr_id*numOfRecources + i] += request_vector[i];
            need[pr_id*numOfRecources + i] -= request_vector[i];
        }
        printf("\nChecking if allocation is safe\n");
        if(isSafe()){
            printf("System is safe : allocating\n");
            break;
        }
        else{
            printf("System is not safe : canceling\n");
            for(int i = 0; i < numOfRecources; i++){
            availableRecources[i] += request_vector[i];
            hold[pr_id*numOfRecources + i] -= request_vector[i];
            need[pr_id*numOfRecources + i] += request_vector[i];
            }
            sleep(3);
        }
    }

    return 0;

}

//Implementation of isSafe() as described in the slides
int isSafe(){
    int isSafe = 0, *work, *isFinish;
    work = (int*)malloc(sizeof(int)*numOfRecources);
    isFinish = (int*)malloc(sizeof(int)*numOfProcesses);
    //Initialize work and isFinish
    for(int i = 0; i < numOfRecources; i++){
        work[i] = availableRecources[i];
    }
    for(int i = 0; i < numOfProcesses; i++){
        isFinish[i] = 0;
    }
    for(int i = 0; i < numOfProcesses; i++){
        if(isFinish[i] == 0){
            int doableCheck = numOfRecources;
            for(int j = 0; j < numOfRecources; j++){
                if(need[i*numOfRecources + j] <= work[j]) doableCheck--;
            }
            if(doableCheck == 0){
                for(int k = 0; k < numOfRecources; k++){
                    work[k] += hold[i*numOfRecources + k];
                }
                i = 0;
                isFinish[i] = 1;
            }
        }
    }
    int allFinish = numOfProcesses;
    for(int i = 0; i < numOfProcesses; i++){
        if(isFinish[i] == 1) allFinish--;
    }
    //If all processes are finished
    if(allFinish == 0) isSafe = 1;
    else isSafe = 0;

    return isSafe;

}

int main()
{
    //Initialize all inputs to banker's algorithm
    printf("Enter number of processes: ");
    scanf("%d",&numOfProcesses);
    printf("Enter number of resources: ");
    scanf("%d",&numOfRecources);
    request_vector = (int*)malloc(sizeof(int)*numOfRecources);
    availableRecources = (int*)malloc(sizeof(int)*numOfRecources);
    printf("Enter Available Resources: ");
    for(int i = 0; i < numOfRecources; i++){
        scanf("%d", &availableRecources[i]);
    }
    maxResClaim = (int*)malloc(sizeof(int*)*numOfProcesses*numOfRecources);
    hold = (int*)malloc(sizeof(int*)*numOfProcesses*numOfRecources);
    need = (int*)malloc(sizeof(int*)*numOfProcesses*numOfRecources);
    printf("Enter Maximum Resources Each Process Can Claim: ");
    for(int i = 0; i < numOfProcesses; i++){
        for(int j = 0; j < numOfRecources; j++){
            scanf("%d", &maxResClaim[i*numOfRecources + j]);
        }
    }

    printf("The Allocated Resources table is :\n");
    for(int i = 0; i < numOfProcesses; i++){
        for(int i = 0; i < numOfRecources; i++){
            printf("0 ");
        }
        printf("\n");
    }

    printf("\nThe Maximum Claim table is :\n");
    for(int i = 0; i < numOfProcesses; i++){
        for(int j = 0; j < numOfRecources; j++){
            printf("%d ", maxResClaim[i*numOfRecources + j]);
        }
        printf("\n");
    }
    printf("\n");

    //create threads simulating processes (process_simulator)

    pthread_t tid[numOfProcesses];

    for(int i = 0; i < numOfProcesses; i++){
        pthread_create(&tid[i], NULL, process_simulator, &i);
        sleep(1);
    }

    return 0;
}
