/*
 ----------------- COMP 310/ECSE 427 Winter 2018 -----------------
 Dimitri Gallos
 Assignment 2 skeleton

 -----------------------------------------------------------------
 I declare that the awesomeness below is a genuine piece of work
 and falls under the McGill code of conduct, to the best of my knowledge.
 -----------------------------------------------------------------
 */

//Please enter your name and McGill ID below
//Name: Cheng Li
//McGill ID: 260706615



#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <semaphore.h>


int BUFFER_SIZE = 100; //size of queue
int idCount = 0;//a counter to obtain the passengerID

pthread_mutex_t mutex;
sem_t empty;

// A structure to represent a queue
struct Queue
{
    int front, rear, size;
    unsigned capacity;
    int* array;
};

// function to create a queue of given capacity.
// It initializes size of queue as 0
struct Queue* createQueue(unsigned capacity)
{
    struct Queue* queue = (struct Queue*) malloc(sizeof(struct Queue));
    queue->capacity = capacity;
    queue->front = queue->size = 0;
    queue->rear = capacity - 1;  // This is important, see the enqueue
    queue->array = (int*) malloc(queue->capacity * sizeof(int));
    return queue;
}

// Queue is full when size becomes equal to the capacity
int isFull(struct Queue* queue)
{
    return ((queue->size ) >= queue->capacity);
}

// Queue is empty when size is 0
int isEmpty(struct Queue* queue)
{
    return (queue->size == 0);
}

// Function to add an item to the queue.
// It changes rear and size
void enqueue(struct Queue* queue, int item)
{
    if (isFull(queue))
        return;
    queue->rear = (queue->rear + 1)%queue->capacity;
    queue->array[queue->rear] = item;
    queue->size = queue->size + 1;
}

// Function to remove an item from queue.
// It changes front and size
int dequeue(struct Queue* queue)
{
    if (isEmpty(queue))
        return INT_MIN;
    int item = queue->array[queue->front];
    queue->front = (queue->front + 1)%queue->capacity;
    queue->size = queue->size - 1;
    return item;
}

// Function to get front of queue
int front(struct Queue* queue)
{
    if (isEmpty(queue))
        return INT_MIN;
    return queue->array[queue->front];
}

// Function to get rear of queue
int rear(struct Queue* queue)
{
    if (isEmpty(queue))
        return INT_MIN;
    return queue->array[queue->rear];
}

void print(struct Queue* queue){
    if (queue->size == 0){
        return;
    }

    for (int i = queue->front; i < queue->front +queue->size; i++){

        printf(" Element at position %d is %d \n ", i % (queue->capacity ), queue->array[i % (queue->capacity)]);
    }

}

struct Queue* queue;

/*Producer Function: Simulates an Airplane arriving and dumping 5-10 passengers to the taxi platform */
void *FnAirplane(void* cl_id)
{
    int planeID = *(int *) cl_id;
    int numOfPassengers = rand()%6 + 5;
    printf("Airplane %d arrives with %d passengers\n",planeID,numOfPassengers);

    for(int i=0;i < numOfPassengers;i++){
        //ID assign
        int passengerID = 1000000 + idCount*1000 + i;
        if(isFull(queue)){
            printf("Platform is full: Rest of passengers of plane %d take the bus\n",planeID);
            break;
        }
        printf("Passenger %d of airplane %d arrives to platform\n",passengerID,planeID);
        enqueue(queue,passengerID);
        sem_post(&empty);

    }
    idCount++;
}

/* Consumer Function: simulates a taxi that takes n time to take a passenger home and come back to the airport */
void *FnTaxi(void* pr_id)
{
    int taxiID = *(int *) pr_id;
    int passengerID;
    while(1){
        printf("Taxi driver %d arrives\n",taxiID);
        //If it is empty, print a massage and wait
        if(isEmpty(queue)){
            printf("Taxi driver %d waits for passengers to enter the platform\n",taxiID);
        }
        sem_wait(&empty);
        passengerID = dequeue(queue);
        printf("Taxi driver %d picked up client %d from the platform\n",taxiID,passengerID);
        //10 ~ 30 minutes to deliver customer
        usleep((167 + rand()%334)*1000);
    }


}

int main(int argc, char *argv[])
{

  int num_airplanes;
  int num_taxis;

  num_airplanes=atoi(argv[1]);
  num_taxis=atoi(argv[2]);

  printf("You entered: %d airplanes per hour\n",num_airplanes);
  printf("You entered: %d taxis\n", num_taxis);


  //initialize queue
  queue = createQueue(BUFFER_SIZE);

  //declare arrays of threads and initialize semaphore(s)
  pthread_t threads[1000];
  sem_init(&empty,0,0);

  //create arrays of integer pointers to ids for taxi / airplane threads
  int *taxi_ids[num_taxis];
  int *airplane_ids[num_airplanes];

  //create threads for taxis
  for(int j=0;j<num_taxis;j++){
    taxi_ids[j] = malloc(sizeof(int));
    *taxi_ids[j]=j;
    pthread_create(&threads[j], NULL, FnTaxi, taxi_ids[j]);
  }
  //create threads for airplanes
  while(1){
    for(int i=0;i<num_airplanes;i++){
        airplane_ids[i] = malloc(sizeof(int));
        *airplane_ids[i] = i;
        printf("Creating airplane thread %d\n",i);
        pthread_create(&threads[i],NULL,FnAirplane,airplane_ids[i]);
    }
    sleep(1);
    idCount=0;
  }
  pthread_exit(NULL);
}
