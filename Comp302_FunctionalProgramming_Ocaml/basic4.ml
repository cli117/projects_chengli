exception NotImplemented

(* Question 1 : Let's have cake! *)

type price = float
type weight = float
type calories = int
type ingredient = Nuts | Gluten | Soy | Dairy

type cupcake = Cupcake of price * weight * calories * ingredient list

let c1 = Cupcake (2.5, 80.3, 250, [Dairy; Nuts])
let c2 = Cupcake (2.75, 90.5, 275, [Dairy; Soy])
let c3 = Cupcake (3.05, 100.4, 303, [Dairy; Gluten; Nuts])
let c4 = Cupcake (3.25, 120.4, 330, [Dairy; Gluten ])

let cupcakes = [c1 ; c2 ; c3 ; c4]

(* Question 2 : Generic Tree Traversals *)

type 'a tree = Node of 'a * 'a tree * 'a tree | Empty
(* -------------------------------------------------------------*)
(* QUESTION 1 : Let's have cake!                                *)
(* -------------------------------------------------------------*)

(* allergy_free : ingredient list -> cupcake list -> cupcake list *)
let allergy_free (allergens: ingredient list) (cupcakes: cupcake list): cupcake list =match allergens, cupcakes with
  | [], _ -> cupcakes
  | _, [] -> []
  | h::t, cupcakes -> List.filter((fun cupcake -> match cupcake with
      | Cupcake(p, w, c, []) -> true 
      | Cupcake(p, w, c, ingredient) -> List.for_all(fun have -> not (List.exists (fun cant -> cant == have) allergens)) ingredient)) cupcakes 

(* -------------------------------------------------------------*)
(* QUESTION 2 : Generic Tree Traversals                         *)
(* -------------------------------------------------------------*)

(* map_tree : ('a -> 'b) -> 'a tree -> 'b tree *)
let rec map_tree (f: 'a -> 'b) (t: 'a tree) = match t with
  | Empty -> Empty
  | Node(n, l, r) -> Node(f n, map_tree f l, map_tree f r)

(* delete_data : ('a * 'b) tree -> 'a tree *)
let delete_data (t: 'a tree) = match t with
  | Empty -> Empty
  | Node(n, l, r) -> map_tree (fun (a,b) -> a) t

(* fold_tree : ('a * 'b ' * 'b -> 'b) -> 'b -> 'a tree -> 'b *)
let rec fold_tree f e t = match t with
  | Empty -> e
  | Node(n, l ,r) -> f(n, (fold_tree f e l), (fold_tree f e r))
  

(* size : 'a tree -> int *)
let size tr = match tr with
  | Empty -> 0
  | Node(n, l, r) -> fold_tree (fun (n, a, b) -> a + b + 1) 0 tr

(* reflect : 'a tree -> 'a tree *)
let reflect tr = match tr with
  | Empty -> Empty
  | Node(n, l, r) -> fold_tree(fun (n, a, b) -> Node(n, b, a)) Empty tr
  

(* inorder : 'a tree -> 'a list *)
let inorder tr = match tr with
  | Empty -> []
  | Node(n, l, r) -> fold_tree(fun (n, a, b) -> a @ [n] @ b) [] tr
