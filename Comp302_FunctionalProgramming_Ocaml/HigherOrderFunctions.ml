exception NotImplemented

(** The unfold function. *)
let rec unfold (f : 'seed -> 'a * 'seed) (stop : 'seed -> bool) (b : 'seed) : 'a list =
  if stop b then
    []
  else
    let x, b' = f b in
    x :: unfold f stop b'

(** Example function using `unfold`.
 * Generates a list of natural numbers less than `max`.
 * *)
let nats max = unfold (fun b -> (b, b+1)) (fun b -> max <= b) 0

(* Question 1: let's compose something! *)

(* 1.1 Composition *)

let compose (fs : ('a -> 'a) list) : 'a -> 'a = 
  (*let rec compose' fs acc = match fs with
      | [] -> acc
      | h::t -> fun x -> h (compose' t acc x)
   in 
   compose' fs (fun a -> a)*)
                       
  List.fold_right (fun fun1 fun2 x -> fun1 (fun2 x)) fs (fun a -> a)
  
(* 1.2 Replication *)

let replicate (n : int) : 'a -> 'a list =
  let rec replicate' n acc = match n with
    | 0 -> acc
    | n -> fun x -> x :: (replicate' (n - 1) acc x)
  in replicate' n (fun a -> [])

(* 1.3 Repeating *)

let repeat (n : int) (f : 'a -> 'a) : 'a -> 'a =
  compose (replicate n f)

(* Question 2: unfolding is like folding in reverse *)

(* 2.1 Compute the even natural numbers up to an exclusive limit. *)
let evens (max : int) : int list=
  let numSet = unfold (fun b -> (b, b+1)) (fun b -> true) 0 in 
  let rec evens' x acc = 
    if x = max then acc
    else( 
      if x mod 2 = 0 then x :: (evens' (x+1) acc) else (evens' (x+1) acc))
  in evens' 0 []
    

(* 2.2 Compute the fibonacci sequence up to an exclusive limit. *)
let fib (max : int) : int list =
  let numSet = unfold (fun b -> (b, b+1)) (fun b -> max <= b) 0 in
  let rec fibCompute n = 
    if n < 3 then 1
    else fibCompute (n - 1) + fibCompute (n - 2)
  in
  let rec fib' max n acc =
    if ((fibCompute n) < max) then (fibCompute n) :: (fib' max (n + 1) acc)
    else acc
  in
  fib' max 1 []

(* 2.3 Compute Pascal's triangle up to a maximum row length. *)
let pascal (max : int) : int list list =
  let rec get_row r = 
    1 :: unfold(function [] -> raise (Failure "This is not possible!")
                       | h :: [] -> h, []
                       | h1::h2::t -> h1 + h2, h2 :: t) (fun l -> l = []) r
  in unfold (fun r -> (r, get_row r)) (fun row -> (List.length row) > max) [1]

(* 2.4 Implement the zip, which joins two lists into a list of tuples.
 * e.g. zip [1;2] ['a', 'c'] = [(1, 'a'); (2, 'c')]
 * Note that if one list is shorter than the other, then the resulting
 * list should have the length of the smaller list. *)
let zip (l1 : 'a list) (l2 : 'b list) : ('a * 'b) list =
  let numSet = unfold (fun b -> (b, b+1)) (fun b -> true) 0 in
  let rec zip' (l1 : 'a list) (l2 : 'b list) =
    match l1, l2 with
    | [], _ -> []
    | _, [] -> []
    | x::xs, y::ys -> (x, y) :: zip' xs ys
  in
  zip' l1 l2
