exception NotImplemented

(* -------------------------------------------------------------*)
(* QUESTION:  Tries                                             *)
(* -------------------------------------------------------------*)

(* A trie is an n-ary tree *)

type 'a trie = Node of 'a * ('a trie) list | Empty

(* -------------------------------------------------------------*)
(* Example trie list                                            *)
(* -------------------------------------------------------------*)

let t =
 [Node
     ('b',
      [Node ('r' , [Node ('e' , [Node ('e' , [Empty])])]) ;
       Node ('e' , [Empty ;
		    Node ('e' , [Empty ; Node ('f', [Empty; Node ('y', [Empty])]) ;
				 Node ('r',[Empty])]) ;
		    Node ('a' , [Node ('r', [Empty; Node ('d' , [Empty])])])])])]

(* -------------------------------------------------------------*)
(* Implementation of tabulate                                   *)
(* tabulate f n returns [f 0; f 1; ...; f (n - 1)]              *)
(* -------------------------------------------------------------*)

let rec tabulate f n =
  let rec tab n acc =
    if n < 0 then acc
    else tab (n - 1) ((f n) :: acc)
  in
  tab (n - 1) []

(* -------------------------------------------------------------*)
(* TRIE HELPER FUNCTIONS                                        *)
(* -------------------------------------------------------------*)

(* Creates a tree containing only l *)
(* unroll : 'a list -> 'a trie list *)
let rec unroll l = match l with
  | []     -> [Empty]
  | x :: t -> [Node(x, unroll t)]

(* Returns true if l contains an Empty node, false otherwise *)
(* contains_empty : 'a trie list -> bool *)
let rec contains_empty l = match l with
  | x :: xs -> x = Empty || contains_empty xs
  | []      -> false

(* Examples of functions that could be used for q5 predicates *)
type labeled_pred = string * (int -> bool)

let is_even : labeled_pred = ("Is even", fun n -> (n mod 2) = 0)
let mult_3 : labeled_pred = ("Is a multiple of 3", fun n -> (n mod 3) = 0)
let is_odd : labeled_pred = ("Is odd", fun n -> (n mod 2) <> 0)
let more_than_4 : labeled_pred = ("Is larger than 4", fun n -> n > 4)
let is_pow_of_2 : labeled_pred =
  let rec f = function
  | 0 -> false
  | 1 -> true
  | n -> if n mod 2 <> 0 then false else f (n / 2)
  in
  ("Is a power of 2", f)



(* HOMEWORK 3 : COMP 302

   GOAL: Learn tree traversal and
         use to your advantage library functions such as
         List.map where appropriate

*)

(* -------------------------------------------------------------*)
(* QUESTION 1 : String manipulation  [20 points]                *)
(* -------------------------------------------------------------*)

(* string_explode : string -> char list *)
let string_explode s =
  tabulate (fun n -> String.get s n) (String.length s)

(* string_implode : char list -> string *)
let string_implode l =
  List.fold_right (fun c s -> Char.escaped c ^ s) l ""


(* -------------------------------------------------------------*)
(* QUESTION 2 : Insert a string into a dictionary  [20 points]  *)
(* -------------------------------------------------------------*)

(* Insert a word into a dictionary. Duplicate inserts are allowed *)

let  insert s t =
  (* ins : char list -> char trie list -> char trie list *)
  let rec ins l t = match l with
    | [] -> Empty :: t
    | c :: cs -> match t with
      | [] -> unroll l
      | Empty :: ts -> Empty :: ins l ts
      | Node (char, children) as node :: ts ->
          if char = c then
            Node (char, ins cs children) :: ts
          else
            node :: ins l ts
  in
  ins (string_explode s) t


(* -------------------------------------------------------------*)
(* QUESTION 3 : Look up a string in a dictionary   [20 points]  *)
(* -------------------------------------------------------------*)

(* Look up a word in a dictionary *)

let lookup s t =
  (* lkp : char list -> char trie list -> bool *)
  let rec lkp l t = match l with
    | [] -> contains_empty t
    | c :: cs -> match t with
      | [] -> false
      | Empty :: ts -> lkp l ts
      | Node (char, children) :: ts ->
          if char = c then
            lkp cs children
          else
            lkp l ts
  in
  lkp (string_explode s) t

(* -------------------------------------------------------------*)
(* QUESTION 4 : Find all strings in a dictionary   [OPTIONAL]   *)
(* -------------------------------------------------------------*)

(* Returns a list of the words contained in trie_list *)
(* to_words : 'a trie list -> 'a list list *)
let rec to_words trie_list = match trie_list with
  | []              -> []
  | Empty :: t_list -> [] :: to_words t_list
  | Node(a, children) :: t_list ->
      (List.map (fun l -> a::l) (to_words children)) @ to_words t_list

(* Find all strings which share the prefix p *)

let find_all prefix t =
  (* find_all' : char list -> char trie list -> char list list *)
  let rec find_all' l t = match l with
    | [] -> to_words t
    | c :: cs -> match t with
      | [] -> []
      | Empty :: ts -> find_all' l ts
      | Node (char, children) :: ts ->
          if char = c then
            find_all' cs children
          else
            find_all' l ts
  in
  let suffixes = find_all' (string_explode prefix) t in
  let suffix_strings = List.map string_implode suffixes in
  List.map (fun s -> prefix ^ s) suffix_strings

(* -------------------------------------------------------------*)
(* QUESTION 5 :  Logic Functions   [OPTIONAL]                   *)
(* -------------------------------------------------------------*)

(* eval: labeled_pred -> labeled_pred -> int -> int -> bool *)
let eval (_, (p : int -> bool)) (_, (q : int -> bool)) (n : int) =
  fun x ->
    List.for_all (fun y -> (p y) || (q (x + y))) (tabulate (fun i -> i) n)

