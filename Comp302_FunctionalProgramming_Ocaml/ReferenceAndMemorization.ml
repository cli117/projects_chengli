exception NotImplemented

exception Msg of string

type passwd = string
type bank_account = {update_passwd  : passwd -> passwd -> unit ;
                     retrieve       : passwd -> int -> unit;
                     deposit        : passwd -> int -> unit;
                     print_balance  : passwd -> int }

(* Bank account errors *)
let wrong_pass = Msg "Wrong Password"
let too_many_attempts = Msg "Change your password"
let no_money = Msg "Insufficient funds"
(* ------------------------------------------------------------------------*)
(* Q 1 : Money in the bank (25 points)                                     *)
(* ------------------------------------------------------------------------*)

let new_account p = 
  let pwd = ref p in
  let balance = ref 0 in
  let attempts = ref 0 in
  {update_passwd = (fun password n -> if (!pwd <> password) then (attempts := !attempts + 1; raise wrong_pass)
                     else (attempts := 0; pwd := n));
   retrieve = (fun password r-> if (!attempts = 3) then (raise too_many_attempts)
                else if (!pwd <> password) then (attempts := !attempts + 1; raise wrong_pass)
                else if (!balance >= r) then(balance:= !balance - r; attempts := 0) 
                else (raise no_money));
   deposit = (fun password b-> if (!attempts = 3) then (raise too_many_attempts)
               else if (!pwd <> password) then (attempts := !attempts + 1; raise wrong_pass) 
               else (balance:= !balance + b; attempts := 0));
   print_balance = (fun password -> if (!attempts = 3) then (raise too_many_attempts)
                     else if (!pwd <> password) then (attempts := !attempts + 1; raise wrong_pass) 
                     else (attempts := 0; !balance))}
;;




(* ------------------------------------------------------------------------*)
(* Q 2 : Memoization (75 points)                                           *)
(* ------------------------------------------------------------------------*)

(* Q 2.1 : Counting how many function calls are made *)

let rec catalan_I n =
  let num = ref 1 in 
  {num_rec = if (n <> 0 && n <> 1) then (num := 2 * (sum (fun x -> (catalan_I x).num_rec) (n - 1)) + 1; !num)
     else !num;
   result = catalan n}

;;


(* Q 2.2 : Memoization with a global store *)

let rec catalan_memo n =
  let rec catalan n =
    match Hashtbl.find_opt store n with 
    | Some a -> a
    | None -> (let outcome = reccat catalan n in
               Hashtbl.add store n outcome; outcome) 

  in
  catalan n
;;


(* Q 2.3 : General memoization function *)

let memo f stats =
  let h = ref (Hashtbl.create 1000) in
  let rec g = (fun a -> match Hashtbl.find_opt !h a with
      | None -> (let result = f g a in
                 Hashtbl.add !h a result; 
                 stats.entries := !(stats.entries) +1; result;)
      | Some a -> (stats.lkp := !(stats.lkp) + 1; a))
  in g

;;


(* Q 2.4 : Using memo to efficiently compute the Hofstadter Sequence Q *)

let hofstadter_Q =
  let s = ref {entries = ref 0; lkp = ref 0} in
  let f g n = match n with
    | 1 | 2 -> 1
    | n -> g (n - g (n - 1)) + g (n - g (n - 2)) in
  let h = memo f !s in
  fun n -> (h n, !s) 

;;
