(* The code here will be added to the top of your code automatically.
   You do NOT need to copy it into your code.
*)

exception Domain
exception NotImplemented

type suit = Clubs | Spades | Hearts | Diamonds

type rank =  Six | Seven | Eight | Nine | Ten |
             Jack | Queen | King | Ace

type card = rank * suit

type hand = Empty | Hand of card * hand

(* dom_suit : suit -> suit -> bool

   dom_suit s1 s2 = true iff suit s1 beats or is equal to suit s2
                    relative to the ordering S > H > D > C
   Invariants: none
   Effects: none
*)

let dom_suit s1 s2 = match s1, s2 with
  | Spades, _        -> true
  | Hearts, Diamonds -> true
  | Hearts, Clubs    -> true
  | Diamonds, Clubs  -> true
  | s1, s2           -> s1 = s2

type nat = int list (* increasing list of weights, each a power of two *)
(* --------------------------------------------------------------------*)
(* QUESTION 1: House of Cards                                          *)
(* --------------------------------------------------------------------*)

(* Q1: Comparing cards *)
(* Comparing two ranks *) 
let dom_rank (r1 : rank) (r2 : rank) : bool= match r1,r2 with
  | Ace, _       -> true
  | King, Ace    -> false
  | King, _      -> true
  | Queen, Ace   -> false
  | Queen, King  -> false
  | Queen, _     -> true
  | Jack, Ace    -> false
  | Jack, King   -> false
  | Jack, Queen  -> false
  | Jack, _      -> true
  | Ten, Ace     -> false
  | Ten, King    -> false
  | Ten, Queen   -> false
  | Ten, Jack    -> false
  | Ten, _       -> true
  | Nine, Six    -> true
  | Nine, Seven  -> true
  | Nine, Eight  -> true
  | Nine, Nine   -> true
  | Nine, _      -> false
  | Eight, Six   -> true
  | Eight, Seven -> true
  | Eight, Eight -> true
  | Eight, _     -> false
  | Seven, Six   -> true
  | Seven, Seven -> true
  | Seven, _     -> false
  | Six, Six     -> true
  | Six, _       -> false
  

(* Comparing two cards (r1, s1) and (r2, s2) *)
let dom_card (c1 : card) (c2 : card) : bool= match c1, c2 with
  | (r1, s1),(r2, s2) -> if (s1 == s2) then dom_rank r1 r2
      else dom_suit s1 s2

(* Q2: Insertion Sort – Sorting cards in a hand *)
let rec insert (c : card) (h : hand) : hand = match h with 
  | Empty -> Hand(c, Empty) 
  | Hand(c0, Empty) -> if dom_card c c0 then Hand(c,h) else Hand(c0, Hand(c,Empty))
  | Hand(c1, h1)    -> if dom_card c c1 then Hand(c,Hand(c1,h1)) else Hand(c1, insert c h1) 

let rec sort (h : hand) : hand = match h with
  | Empty            -> Empty
  | Hand(c0, Empty)  -> h 
  | Hand(c1, h1)     -> insert c1 (sort h1)
  

(* Q3: Generating a deck of cards *) 
let generate_deck (suits : suit list) (ranks : rank list) : card list = 
  let rec generate_deck' (newSuits : suit list) (newRanks : rank list) = match newSuits, newRanks with 
    | [], _          -> []
    | _, []          -> [] 
    |x0::l0, x1::l1 -> match l0,l1 with
      | [],[]        -> [(x1, x0)]
      | [], _        -> [(x1, x0)] @ generate_deck' [x0] l1
      | _,[]         -> [(x1, x0)] @ generate_deck' l0 ranks
      | _, _         -> [(x1, x0)] @ generate_deck' (x0::l0) l1 
  in generate_deck' suits ranks

(* Q4: Shuffling a deck of cards *)
let rec split (deck : card list) (n : int) : card * card list = match n, deck with
  | _, [] -> raise Domain
  | 0, x0::l0 -> (x0, l0) 
  | n, x1::l1 -> (fst (split l1 (n-1)), x1 :: snd (split l1 (n-1)))
  

let shuffle (deck : card list) : card list =
  let size = List.length deck in
  let rec select deck n = match deck with
    | [] -> []
    | x::l -> let index = Random.int n in
        [fst(split deck index)] @ select (snd(split deck index)) (n-1)
    
  in
  select deck size

(* --------------------------------------------------------------------*)
(* QUESTION 2: Sparse Representation of Binary Numbers                 *)
(* ------------------------------------------------------------------- *)

    (* Q1: Incrementing a sparse binary number *)
let inc (ws : nat ) : nat = match ws with
  | []     -> [1]
  | x0::l0 -> if x0 <> 1 then [1] @ ws
      else let rec traverse (check : nat) : nat = match check with
          | [] -> []
          | [x1] -> [x1]
          | [x1;x2] -> if (x1 == x2) then [2*x1] else [x1;x2]
          | x1::x2::l1 -> if (x1 == x2) then traverse ((2*x1)::l1) else x1::x2::l1
        in traverse ([1]@[1]@l0)

(* Q2: Decrementing a sparse binary number *) 
                      
let dec (ws : nat) : nat =
  let rec dec' (newWs : nat) (n : int): nat = match n with
    | 1 -> newWs
    | _ -> dec' ((n/2)::newWs) (n/2)
  in 
  match ws with
  | [] -> raise Domain
  | x::l -> dec' l x
            
(* Q3: Adding sparse binary numbers *)
let rec add (m : nat) (n : nat) : nat  = match n with
  | [] -> m 
  | _  -> add (inc m) (dec n)
  

(* Q4: Converting to integer - tail recursively *)
let rec toInt (n : nat) (acc : int) : int = match n with
  | [] -> (acc+0)
  | x0::l0 -> toInt l0 (acc+x0)
  

let rec sbinToInt (n : nat) : int = match n with
  | [] -> 0
  | x0::l0 -> x0 + sbinToInt l0

