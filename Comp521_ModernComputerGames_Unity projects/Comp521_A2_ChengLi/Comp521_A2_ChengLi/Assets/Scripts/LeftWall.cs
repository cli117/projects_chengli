﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Mesh to draw wall
public class LeftWall : MonoBehaviour
{

    Mesh mesh;

    Vector3[] vertices;
    int[] triangles;

    private void Awake()
    {
        mesh = GetComponent<MeshFilter>().mesh;
    }

    void Start()
    {
        MakeMeshData();
        CreateMesh();
    }

    void MakeMeshData()
    {
        vertices = new Vector3[] {
            new Vector3(-10.3f, -5f),
            new Vector3(-10.3f, 7f),
            new Vector3(-10.1f, -5f),
            new Vector3(-10.1f, 7f)
        };

        triangles = new int[] { 0, 1, 2, 1, 3, 2 };
    }

    void CreateMesh()
    {
        mesh.Clear();
        mesh.vertices = vertices;
        mesh.triangles = triangles;
    }
}


