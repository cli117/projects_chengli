﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//Use mesh to draw a cloud
[RequireComponent(typeof(MeshRenderer))]
public class Cloud : MonoBehaviour {

    Mesh mesh;
    float EPSILON = 0.01f;

    Vector3[] vertices;
    int[] triangles;

    private void Awake()
    {
        mesh = GetComponent<MeshFilter>().mesh;
    }

    void Start()
    {
        MakeMeshData();
        CreateMesh();
    }

    void MakeMeshData()
    {
        vertices = new Vector3[] {
            new Vector3(0f, 3.5f),
            new Vector3(0.3f, 4f),
            new Vector3(2.5f, 3.5f),
            new Vector3(2.3f, 4f),
            new Vector3(0.5f, 4f),
            new Vector3(0.7f, 4.3f),
            new Vector3(2f, 4f),
            new Vector3(1.8f, 4.3f)
        };

        triangles = new int[] { 0, 1, 2, 1, 3, 2, 4, 5, 6, 5, 7, 6 };
    }

    void CreateMesh()
    {
        mesh.Clear();
        mesh.vertices = vertices;
        mesh.triangles = triangles;
    }

    //If exceed the edge of the screen, transform to another side of the screen
    void Update () {
        transform.Translate(new Vector3(WindForce.GetRealTimeWindForce()*100f, 0f, 0f));
        if((transform.position.x - (-10f)) < EPSILON){
            transform.position = new Vector3(7.5f, 0f, 0f);
        }
        if ((transform.position.x - 7.8f) > EPSILON)
        {
            transform.position = new Vector3(-10f, 0f, 0f);
        }
    }
}
