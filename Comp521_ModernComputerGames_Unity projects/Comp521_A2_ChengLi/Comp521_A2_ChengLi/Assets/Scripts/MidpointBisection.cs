﻿using UnityEngine;

public class MidpointBisection {
    //Start point and end point in the base
    private Vector2 start;
    private Vector2 end;
    //A platform at the very top
    private Vector2 topStart;
    private Vector2 topEnd;
    private float randomRange;
    private static Vector3[] vertices;

    public MidpointBisection(Vector2 start, Vector2 end){
        this.start = start;
        this.end = end;
        this.topStart = new Vector2(((start.x+end.x)/2f) - Random.Range(0f, 1f), 0f);
        this.topEnd = new Vector2(((start.x+end.x)/2f) + Random.Range(0f, 1f), 0f);
        //In our game, the range need to less than 1.25 so both sides of the mountain would not higher than the peak
        this.randomRange = 1.25f;
    }

    public Vector3[] GetVerticesAsVector3Array(){
        //MidPoint-Bisection algo
        Vector2 mid3 = new Vector2((start.x + topStart.x) / 2, ((start.y + topStart.y) / 2) + Random.Range(0f, randomRange));
        Vector2 mid8 = new Vector2((end.x + topEnd.x) / 2, ((end.y + topEnd.y) / 2) + Random.Range(0f, randomRange));

        randomRange = randomRange / 2f;

        Vector2 mid2 = new Vector2((start.x + mid3.x) / 2, ((start.y + mid3.y) / 2) + Random.Range(0f, randomRange));
        Vector2 mid9 = new Vector2((end.x + mid8.x) / 2, ((end.y + mid8.y) / 2) + Random.Range(0f, randomRange));
        Vector2 mid4 = new Vector2((topStart.x + mid3.x) / 2, ((topStart.y + mid3.y) / 2) + Random.Range(0f, randomRange));
        Vector2 mid7 = new Vector2((topEnd.x + mid8.x) / 2, ((topEnd.y + mid8.y) / 2) + Random.Range(0f, randomRange));

        Vector2 base2 = new Vector2(mid2.x, start.y);
        Vector2 base3 = new Vector2(mid3.x, start.y);
        Vector2 base4 = new Vector2(mid4.x, start.y);
        Vector2 base5 = new Vector2(topStart.x, start.y);
        Vector2 base6 = new Vector2(topEnd.x, start.y);
        Vector2 base7 = new Vector2(mid7.x, start.y);
        Vector2 base8 = new Vector2(mid8.x, start.y);
        Vector2 base9 = new Vector2(mid9.x, start.y);
        vertices = new Vector3[]{start, mid2, base2, mid3, base3, mid4, base4, topStart, base5, topEnd, base6, mid7, base7, mid8, base8, mid9, base9, end};
        return vertices;
    }

    public int[] GetTriangles(){
        //Orders are fixed
        return new int[] { 0, 1, 2, 1, 3, 2, 2, 3, 4, 4, 3, 6, 3, 5, 6, 6, 5, 8, 5, 7, 8, 8, 7, 10, 7, 9, 10, 10, 9, 12, 9, 11, 12, 12, 11, 14, 11, 13, 14, 14, 13, 16, 13, 15, 16, 16, 15, 17 };
    }

    public static Vector2[][] GetMountainBounds(){
        Vector2[] line1 = new Vector2[] { vertices[0], vertices[1] };
        Vector2[] line2 = new Vector2[] { vertices[1], vertices[3] };
        Vector2[] line3 = new Vector2[] { vertices[3], vertices[5] };
        Vector2[] line4 = new Vector2[] { vertices[5], vertices[7] };
        Vector2[] line5 = new Vector2[] { vertices[7], vertices[9] };
        Vector2[] line6 = new Vector2[] { vertices[9], vertices[11] };
        Vector2[] line7 = new Vector2[] { vertices[11], vertices[13] };
        Vector2[] line8 = new Vector2[] { vertices[13], vertices[15] };
        Vector2[] line9 = new Vector2[] { vertices[15], vertices[17] };
        return new Vector2[][]{
            line1, line2, line3, line4, line5, line6, line7, line8, line9
        };
    }

}
