﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonBall : MonoBehaviour
{

    private readonly float EPSILON = 0.0001f;
    private GameObject myCannonBall;
    private Vector3 startingPoint;
    private Vector3 startingDirection;
    private Vector3 currentVelosityAsVector3;
    private Vector2[][] lines;
    private int[] lineMark;
    TurkeyManager turkeys;
    Turkey[] turkeyAsArray;
    private bool isActicated;

    private void Start()
    {
        isActicated = true;
        //Get all turkeys through turkey manager
        turkeys = GameObject.Find("TurkeyManager").GetComponent<TurkeyManager>() as TurkeyManager;
        turkeyAsArray = new Turkey[5];
        lineMark = new int[1];
        //Sphere represents the cannonball
        myCannonBall = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        myCannonBall.transform.localScale = new Vector3(0.2f, 0.2f, 0f);
        startingPoint = Cannon.GetCurrentCannonPosition();
        startingDirection = Cannon.GetCurrentCannonDirection();
        currentVelosityAsVector3 = CalculateVelocityAsVector3(startingDirection, 0.35f);
        myCannonBall.transform.position = new Vector3(startingPoint.x, startingPoint.y, 0f);
    }

    private void Update()
    {
        //If destroyed, do nothing
        if(isActicated){
            GameObject[] turkeysAsObArray = turkeys.GetAllTurkeysAsGameObjectArray();
            for (int i = 0; i < 5; i++){
                turkeyAsArray[i] = turkeysAsObArray[i].GetComponent(typeof(Turkey)) as Turkey;
            }
            Draw();
            CollideWithATurkey();
        }
    }

    private void Draw()
    {
        //Collide with the wall
        if (myCannonBall.transform.position.x <= -10)
        {
            float newX = currentVelosityAsVector3.x * -1f * 0.95f;
            float newY = currentVelosityAsVector3.y * 0.95f;
            if (newX - 0 < EPSILON) newX = 0.16f;
            currentVelosityAsVector3 = new Vector3(newX, newY, 0f);
        }

        //High enough so be affected by the wind
        if (myCannonBall.transform.position.y >= 0f)
        {
            currentVelosityAsVector3.x += WindForce.GetRealTimeWindForce();
        }

        //Or low enough so encounter the ground
        else if (myCannonBall.transform.position.y <= -4.85)
        {
            currentVelosityAsVector3 = Vector3.zero;
            Destroy(myCannonBall, 1f);
            isActicated = false;
        }

        lines = MidpointBisection.GetMountainBounds();
        if (HelperFunctions.IsCollideWithMountain(myCannonBall, lineMark))
        {
            MoveAccordingly();
        }
        //Move the cannonball use calculated velosity
        myCannonBall.transform.Translate(currentVelosityAsVector3);
        currentVelosityAsVector3.y -= 0.006f;
    }

    private Vector3 CalculateVelocityAsVector3(Vector3 direction, float velosity)
    {
        float directionVectorLength = HelperFunctions.CalculateVectorLength(direction);
        return new Vector3(direction.x * velosity / directionVectorLength, direction.y * velosity / directionVectorLength, 0f);
    }


    private void MoveAccordingly()
    {
        float velosityLength = HelperFunctions.CalculateVectorLength(currentVelosityAsVector3);
        float angle = HelperFunctions.CalculateAngleBetweenAVectorAndALine(lines[lineMark[0]], currentVelosityAsVector3);
        float angleBetweenMountainAndGroud = HelperFunctions.CalculateAngleBetweenALineAndTheGroud(lines[lineMark[0]]);
        float currentXComponent1, currentXComponent2, currentYComponent1, currentYComponent2;
        float oldXV = currentVelosityAsVector3.x;
        float oldXY = currentVelosityAsVector3.y;

        //Mountain Top
        if (lineMark[0] == 4)
        {
            currentVelosityAsVector3 = new Vector3(oldXV * 0.95f, -oldXY * 0.95f);
        }

        //Left side of the mountain
        if (lineMark[0] < 4)
        {
            //Calculate components of velosity in X direction and Y direction
            currentXComponent1 = (-1f) * velosityLength * Mathf.Sin(angle) * Mathf.Sin(angleBetweenMountainAndGroud);
            currentXComponent2 = velosityLength * Mathf.Cos(angle) * Mathf.Cos(angleBetweenMountainAndGroud);
            currentYComponent1 = velosityLength * Mathf.Sin(angle) * Mathf.Cos(angleBetweenMountainAndGroud);
            currentYComponent2 = velosityLength * Mathf.Cos(angle) * Mathf.Sin(angleBetweenMountainAndGroud);
            //Add all components up and times by 0.9
            currentVelosityAsVector3 = new Vector3((currentXComponent1 + currentXComponent2) * 0.95f, (currentYComponent1 + currentYComponent2) * 0.95f, 0f);
        }

        //Right side of the mountain
        if (lineMark[0] > 4)
        {
            currentXComponent1 = velosityLength * Mathf.Sin(angle) * Mathf.Sin(angleBetweenMountainAndGroud);
            currentXComponent2 = velosityLength * Mathf.Cos(angle) * Mathf.Cos(angleBetweenMountainAndGroud);
            currentYComponent1 = velosityLength * Mathf.Sin(angle) * Mathf.Cos(angleBetweenMountainAndGroud);
            currentYComponent2 = (-1f) * velosityLength * Mathf.Cos(angle) * Mathf.Sin(angleBetweenMountainAndGroud);
            currentVelosityAsVector3 = new Vector3((currentXComponent1 + currentXComponent2) * 0.95f, (currentYComponent1 + currentYComponent2) * 0.95f, 0f);
        }
    }

    //To detect if the cannon ball is collide with a turkey
    private void CollideWithATurkey()
    {
        Vector2 currentPosition = new Vector2(myCannonBall.transform.position.x, myCannonBall.transform.position.y);
        Vector3[][] allVertices = turkeys.GetAllVertices();
        for (int i = 0; i < 5; i++){
            for (int j = 0; j < 24; j++){
                //If too close, collide
                if(HelperFunctions.CalculateDistanceBetweenTwoPoints(currentPosition, allVertices[i][j]) <= 0.1){
                    //Destroy the ball and pass the force and the point of the turkey as arguments 
                    Destroy(myCannonBall);
                    turkeyAsArray[i].getHit(j, currentVelosityAsVector3);
                    isActicated = false;
                }
            }
        }
    }
}
