﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshRenderer))]
public class MeshMount : MonoBehaviour
{
    Mesh mesh;

    Vector3[] vertices;
    int[] triangles;

    private void Awake()
    {
        mesh = GetComponent<MeshFilter>().mesh;
    }

    private void Start()
    {
        MakeMeshData();
        CreateMesh();
    }

    private void Update()
    {

    }

    void MakeMeshData(){
        //Calculate vertices by using MidpointBisection
        MidpointBisection temp = new MidpointBisection(new Vector2(-5f, -5f), new Vector2(5f, -5f));
        vertices = temp.GetVerticesAsVector3Array();
        //Triangles are fixed
        triangles = temp.GetTriangles();
        }
    void CreateMesh(){
        mesh.Clear();
        mesh.vertices = vertices;
        mesh.triangles = triangles;
    }
}
