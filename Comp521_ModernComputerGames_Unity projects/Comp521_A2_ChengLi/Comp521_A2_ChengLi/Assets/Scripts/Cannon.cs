﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//Use mesh to draw a rectangle as the body of the cannon
[RequireComponent(typeof(MeshRenderer))]
public class Cannon : MonoBehaviour {
    private Mesh mesh;
    private static Vector3[] vertices;
    private static int[] triangles;
    private float sin = Mathf.Sin(Mathf.PI / 90f);
    private float cos = Mathf.Cos(Mathf.PI / 90f);
    private static int rotateCount;

    public GameObject myCannonBall;

    private void Awake()
    {
        mesh = GetComponent<MeshFilter>().mesh;
    }


    private void Start () {
        rotateCount = 0;
        DrawBody();
	}

    private void Update()
    {
        RotateCannon();
        Fire();
    }


    private void DrawBody(){
        vertices = new Vector3[]{
            new Vector3(8.3f, -4.82f, 0f),
            new Vector3(8.3f, -4.55f, 0f),
            new Vector3(9.3f, -4.82f, 0f),
            new Vector3(9.3f, -4.55f, 0f)
        };
        triangles = new int[] { 0, 1, 2, 1, 3, 2 };
        mesh.Clear();
        mesh.vertices = vertices;
        mesh.triangles = triangles;
    }


    //Rotate Cannon 2 degrees each time
    private void RotateCannon(){
        //If player press upArrow key, and current angle is not greater than 90
        if(Input.GetKeyDown(KeyCode.UpArrow)){
            if(rotateCount != 45){
                for (int i = 0; i < vertices.Length; i++)
                {
                    float xBasedOnPoint = (vertices[i].x - vertices[2].x);
                    float yBasedOnPoint = (vertices[i].y - vertices[2].y);
                    vertices[i] = new Vector3((xBasedOnPoint * cos + yBasedOnPoint * sin) + vertices[2].x, (-xBasedOnPoint * sin + yBasedOnPoint * cos) + vertices[2].y, 0);
                }
                mesh.vertices = vertices;
                rotateCount++;
            }
        }

        //If player press downArrow key, and current angle is not less than 0
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            if (rotateCount != 0){
                for (int i = 0; i < vertices.Length; i++)
                {
                    float xBasedOnPoint = (vertices[i].x - vertices[2].x);
                    float yBasedOnPoint = (vertices[i].y - vertices[2].y);
                    vertices[i] = new Vector3((xBasedOnPoint * cos - yBasedOnPoint * sin) + vertices[2].x, (xBasedOnPoint * sin + yBasedOnPoint * cos) + vertices[2].y, 0);
                }
                mesh.vertices = vertices;
                rotateCount--;
            }
        }
    }

    //Instantiate as firing
    private void Fire(){
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Instantiate(myCannonBall);
        }
    }

    //Getters
    public static Vector3 GetCurrentCannonPosition(){
        return new Vector3((vertices[0].x + vertices[1].x) / 2f, (vertices[0].y + vertices[1].y) / 2f, 0f);
    }

    public static Vector3 GetCurrentCannonDirection(){
        return new Vector3(vertices[0].x - vertices[2].x, vertices[0].y - vertices[2].y, 0f);
    }
}
