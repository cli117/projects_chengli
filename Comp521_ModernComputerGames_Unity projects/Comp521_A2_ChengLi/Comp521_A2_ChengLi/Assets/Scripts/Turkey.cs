﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//Line renderer to draw the turkey
[RequireComponent(typeof(LineRenderer))]
public class Turkey : MonoBehaviour {
    private GameObject eye;
    private LineRenderer lineRenderer;
    private float lineWidth = 0.03f;
    private float[] distancesBetweenPoints;
    private Vector2[] defaultPoints;
    private Vector2[] defaultVectors;
    private float[] currentDistances;
    private Vector2[] currentVectors;
    private float[] defaultDegrees;
    private float[] currentDegrees;
    private const float tolerance = 0.01f;

    private void Awake()
    {
        lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.useWorldSpace = false;
    }

    void Start()
    {
        //Initializations
        defaultVectors = new Vector2[24];
        defaultPoints = new Vector2[24];
        distancesBetweenPoints = new float[24];
        currentDistances = new float[24];
        currentVectors = new Vector2[24];
        defaultDegrees = new float[24];
        currentDegrees = new float[24];
        eye = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        //Draw the body of turkey
        DrawTurkey();
        //Log all datas into our arrays
        LogDistancesVectorsAndDegrees(distancesBetweenPoints, defaultVectors, defaultDegrees);
    }
	
	void Update () {
        //The eye need to follow turkey's body.
        eye.transform.localScale = new Vector3(0.08f, 0.08f, 0f);
        Vector3 pos = transform.localPosition;
        pos.Set(pos.x - 0.7f, pos.y + 1.1f, 0f);
        eye.transform.localPosition = pos;
        //recover due to the constrains
        recovery();
    }


    private void DrawTurkey(){
        lineRenderer.widthMultiplier = lineWidth;
        lineRenderer.positionCount = 27;
        lineRenderer.SetPosition(0, new Vector3(0f, 0f, 0f));
        lineRenderer.SetPosition(1, new Vector3(0.2f, 0.2f, 0f));
        lineRenderer.SetPosition(2, new Vector3(0.2f, 0.5f, 0f));
        lineRenderer.SetPosition(3, new Vector3(0.5f, 0.5f, 0f));
        lineRenderer.SetPosition(4, new Vector3(0.7f, 0.6f, 0f));
        lineRenderer.SetPosition(5, new Vector3(0.8f, 0.8f, 0f));
        lineRenderer.SetPosition(6, new Vector3(0.8f, 1f, 0f));
        lineRenderer.SetPosition(7, new Vector3(0.7f, 1.2f, 0f));
        lineRenderer.SetPosition(8, new Vector3(0.5f, 1.3f, 0f));
        lineRenderer.SetPosition(9, new Vector3(0.3f, 1.3f, 0f));
        lineRenderer.SetPosition(10, new Vector3(0f, 1.3f, 0f));
        lineRenderer.SetPosition(11, new Vector3(-0.2f, 1.25f, 0f));
        lineRenderer.SetPosition(12, new Vector3(-0.4f, 0.9f, 0f));
        lineRenderer.SetPosition(13, new Vector3(-0.5f, 1.25f, 0f));
        lineRenderer.SetPosition(14, new Vector3(-0.7f, 1.25f, 0f));
        lineRenderer.SetPosition(15, new Vector3(-0.9f, 1.1f, 0f));
        lineRenderer.SetPosition(16, new Vector3(-0.8f, 1f, 0f));
        lineRenderer.SetPosition(17, new Vector3(-0.8f, 0.6f, 0f));
        lineRenderer.SetPosition(18, new Vector3(-0.7f, 1f, 0f));
        lineRenderer.SetPosition(19, new Vector3(-0.7f, 0.7f, 0f));
        lineRenderer.SetPosition(20, new Vector3(-0.4f, 0.5f, 0f));
        lineRenderer.SetPosition(21, new Vector3(-0.2f, 0.5f, 0f));
        lineRenderer.SetPosition(22, new Vector3(-0.2f, 0.2f, 0f));
        lineRenderer.SetPosition(23, new Vector3(-0.4f, 0f, 0f));
        //finish the body(do not included in constrain)
        lineRenderer.SetPosition(24, new Vector3(-0.2f, 0.2f, 0f));
        lineRenderer.SetPosition(25, new Vector3(-0.2f, 0.5f, 0f));
        lineRenderer.SetPosition(26, new Vector3(0.2f, 0.5f, 0f));
        for (int i = 0; i < 24; i++){
            defaultPoints[i] = lineRenderer.GetPosition(i);
        }
    }

    //Simply log everything
    private void LogDistancesVectorsAndDegrees(float[] distances, Vector2[] vectors, float[] degrees){
        Vector2 firstPoint = lineRenderer.GetPosition(0);
        Vector2 lastPoint = lineRenderer.GetPosition(23);

        for (int i = 0; i < 23; i++){
            Vector2 thisPoint = lineRenderer.GetPosition(i);
            Vector2 nextPoint = lineRenderer.GetPosition(i + 1);
            distances[i] = HelperFunctions.CalculateDistanceBetweenTwoPoints(thisPoint, nextPoint);
            vectors[i] = new Vector2(nextPoint.x - thisPoint.x, nextPoint.y - thisPoint.y);
        }

        distances[23] = HelperFunctions.CalculateDistanceBetweenTwoPoints(firstPoint, lastPoint);
        vectors[23] = new Vector2(firstPoint.x - lastPoint.x, firstPoint.y - lastPoint.y);

        for (int i = 1; i < 24; i++){
            degrees[i] = HelperFunctions.CalculateAngleBetweenTwoVectors(vectors[i-1], vectors[i]);
        }

        degrees[0] = HelperFunctions.CalculateAngleBetweenTwoVectors(vectors[23], vectors[0]);
    }

    //getter
    public Vector3[] GetTurkeyPoints(){
        return new Vector3[] {
            lineRenderer.GetPosition(0), lineRenderer.GetPosition(1), lineRenderer.GetPosition(2), lineRenderer.GetPosition(3),
            lineRenderer.GetPosition(4), lineRenderer.GetPosition(5), lineRenderer.GetPosition(6), lineRenderer.GetPosition(7),
            lineRenderer.GetPosition(8), lineRenderer.GetPosition(9), lineRenderer.GetPosition(10), lineRenderer.GetPosition(11),
            lineRenderer.GetPosition(12), lineRenderer.GetPosition(13), lineRenderer.GetPosition(14), lineRenderer.GetPosition(15),
            lineRenderer.GetPosition(16), lineRenderer.GetPosition(17), lineRenderer.GetPosition(18), lineRenderer.GetPosition(19),
            lineRenderer.GetPosition(20), lineRenderer.GetPosition(21), lineRenderer.GetPosition(22), lineRenderer.GetPosition(23),
        };
    }

    //If was hit by a cannonBall, transform the hit point to a new position
    public void getHit(int pointGetHit, Vector3 direction){
        if(direction.x > 0){
            //Slowly to model the process
            for (int i = 0; i < direction.x * 1000f; i++)
            {
                Vector3 currentPos = lineRenderer.GetPosition(pointGetHit);
                lineRenderer.SetPosition(pointGetHit, new Vector3(currentPos.x + 0.001f, currentPos.y, 0f));
            }
        }

        else{
            for (int i = 0; i < -direction.x * 1000f; i++)
            {
                Vector3 currentPos = lineRenderer.GetPosition(pointGetHit);
                lineRenderer.SetPosition(pointGetHit, new Vector3(currentPos.x - 0.001f, currentPos.y, 0f));
            }
        }

        if(direction.y > 0){
            for (int i = 0; i < direction.y * 1000f; i++)
            {
                Vector3 currentPos = lineRenderer.GetPosition(pointGetHit);
                lineRenderer.SetPosition(pointGetHit, new Vector3(currentPos.x, currentPos.y + 0.001f, 0f));
            }
        }

        else{
            for (int i = 0; i < -direction.y * 1000f; i++)
            {
                Vector3 currentPos = lineRenderer.GetPosition(pointGetHit);
                lineRenderer.SetPosition(pointGetHit, new Vector3(currentPos.x, currentPos.y - 0.001f, 0f));
            }
        }
    }

    private void recovery(){
        //Recover due to constraints
        LogDistancesVectorsAndDegrees(currentDistances, currentVectors, currentDegrees);

        //Angles first, move the vector to the right direction
        for (int i = 0; i < 24; i++){
            if(Mathf.Abs(currentDegrees[i] - defaultDegrees[i]) >= tolerance){
                int next;
                Vector2 previousPoint;
                Vector2 nextPoint;
                bool isBigger = currentDegrees[i] > defaultDegrees[i], isLeft;
                if (i == 0) {
                    previousPoint = lineRenderer.GetPosition(23);
                    nextPoint = lineRenderer.GetPosition(1);
                    next = 1;
                }
                else if(i == 23){
                    previousPoint = lineRenderer.GetPosition(22);
                    nextPoint = lineRenderer.GetPosition(0);
                    next = 0;
                }
                else{
                    previousPoint = lineRenderer.GetPosition(i - 1);
                    nextPoint = lineRenderer.GetPosition(i + 1);
                    next = i + 1;
                }

                isLeft = previousPoint.x > nextPoint.x ? true : false;
                if(isLeft){
                    //recover slowly
                    if (isBigger)
                    {
                        lineRenderer.SetPosition(next, new Vector3(nextPoint.x - 0.001f, nextPoint.y));
                    }
                    else
                    {
                        lineRenderer.SetPosition(next, new Vector3(nextPoint.x + 0.001f, nextPoint.y));
                    }

                }
                else{
                    if (isBigger)
                    {
                        lineRenderer.SetPosition(next, new Vector3(nextPoint.x + 0.001f, nextPoint.y));
                    }
                    else
                    {
                        lineRenderer.SetPosition(next, new Vector3(nextPoint.x - 0.001f, nextPoint.y));
                    }
                }
            }

        }

        //Then adjust the distance, i.e. the length of the direction vector
        for (int i = 0; i < 24; i++){
            if(Mathf.Abs(currentDistances[i] - distancesBetweenPoints[i]) >= tolerance){
                int next = i == 23 ? 0 : i + 1;
                //Vector2 move = HelperFunctions.CalculateVectorSubtraction(defaultVectors[i], currentVectors[i]);
                Vector2 pointToMove = lineRenderer.GetPosition(next);
                Vector2 moveVector = defaultPoints[next];
                lineRenderer.SetPosition(i + 1, new Vector3(pointToMove.x + ((moveVector.x - pointToMove.x) * 0.01f), pointToMove.y + ((moveVector.y - pointToMove.y) * 0.01f), 0f));
            }
        }
    }
}
