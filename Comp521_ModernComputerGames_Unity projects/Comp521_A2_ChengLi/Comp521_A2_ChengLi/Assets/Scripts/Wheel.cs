﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Draw a circle use line renderer
[RequireComponent(typeof(LineRenderer))]
public class Wheel : MonoBehaviour {
    private int vertexCount = 100;
    private float lineWidth = 0.03f;
    private float radius = 0.15f;
    private LineRenderer lineRenderer;

    private void Awake()
    {
        lineRenderer = GetComponent<LineRenderer>();
    }

    void Start () {
        DrawWheel();
	}

    private void DrawWheel()
    {
        lineRenderer.widthMultiplier = lineWidth;

        float deltaTheta = (2f * Mathf.PI) / vertexCount;
        float theta = 0f;

        lineRenderer.positionCount = vertexCount;
        for (int i = 0; i < vertexCount; i++)
        {
            Vector3 pos = new Vector3(radius * Mathf.Cos(theta) + 9f, radius * Mathf.Sin(theta) - 4.85f, 0f);
            lineRenderer.SetPosition(i, pos);
            theta += deltaTheta;
        }
    }
}
