﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class HelperFunctions {
    //Helper functions to do calculation
    public static float CalculateYValueOnALineAccordingToXValue(Vector2[] line, float x)
    {
        float coifficient = (line[0].y - line[1].y) / (line[0].x - line[1].x);
        float parameter = line[0].y - (line[0].x * coifficient);
        return (x * coifficient + parameter);
    }

    //Return the angle between two vectors
    public static float CalculateAngleBetweenAVectorAndALine(Vector2[] line, Vector2 vector)
    {
        Vector2 directionVectorOfLine = new Vector2(line[0].x - line[1].x, line[0].y - line[1].y);
        return Mathf.Acos(((directionVectorOfLine.x * vector.x) + (directionVectorOfLine.y * vector.y)) / (CalculateVectorLength(directionVectorOfLine)) * (CalculateVectorLength(vector)));
    }

    public static float CalculateAngleBetweenTwoVectors(Vector2 a, Vector2 b){
        return Mathf.Acos(((a.x * b.x) + (a.y * b.y)) / (CalculateVectorLength(a)) * (CalculateVectorLength(b)));
        }

    public static float CalculateAngleBetweenALineAndTheGroud(Vector2[] line)
    {
        Vector2 directionVectorOfLine = new Vector2(line[0].x - line[1].x, line[0].y - line[1].y);
        return (Mathf.Abs(directionVectorOfLine.y) / CalculateVectorLength(directionVectorOfLine));
    }

    public static float CalculateVectorLength(Vector2 vector)
    {
        return Mathf.Sqrt(Mathf.Pow(vector.x, 2) + (Mathf.Pow(vector.y, 2)));
    }

    public static bool IsCollideWithMountain(GameObject objectToCheck, int[] lineMark)
    {
        Vector2[][] lines = MidpointBisection.GetMountainBounds();
        //If it is not above the mountain, cant collide with the mountain
        if (objectToCheck.transform.position.x < -5f || objectToCheck.transform.position.x > 5f)
        {
            return false;
        }
        else
        {
            for (int i = 0; i < 9; i++)
            {
                //In the corresponding segment of the mountain
                if (objectToCheck.transform.position.x >= lines[i][0].x && objectToCheck.transform.position.x < lines[i][1].x)
                {
                    //And its position is lower or equal to the mountain bound
                    if (objectToCheck.transform.position.y <= CalculateYValueOnALineAccordingToXValue(lines[i], objectToCheck.transform.position.x))
                    {
                        lineMark[0] = i;
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
        }

        return false;
    }

    public static float CalculateDistanceBetweenTwoPoints(Vector3 a, Vector3 b){
        float xDifference = a.x - b.x;
        float yDifference = a.y - b.y;
        return CalculateVectorLength(new Vector2(xDifference, yDifference));
    }

    public static Vector3 CalculateVectorSubtraction(Vector3 a, Vector3 b){
        return new Vector3(a.x - b.x, a.y - b.y, 0f);
    }
}
