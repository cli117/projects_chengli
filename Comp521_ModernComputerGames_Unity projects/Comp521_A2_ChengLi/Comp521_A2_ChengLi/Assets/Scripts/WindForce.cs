﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindForce :MonoBehaviour{
    private static float currentWindForce;

    //Generate wind force every 0.5 second
    private void Start()
    {
        InvokeRepeating("GenerateWindforce", 0f, 0.5f);
    }

    void GenerateWindforce(){
        currentWindForce = Random.Range(-0.001f, 0.001f);
    }

    //getter
    public static float GetRealTimeWindForce(){
        return currentWindForce;
    }
}
