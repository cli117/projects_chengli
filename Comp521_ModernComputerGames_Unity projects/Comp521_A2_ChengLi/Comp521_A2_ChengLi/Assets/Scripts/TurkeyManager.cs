﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurkeyManager : MonoBehaviour {
    public GameObject Turkey;
    private const float velosityMagnitude = 0.01f;
    private GameObject[] Turkeys;
    float[] horizontalVelositySets;
    float[] verticalVelositySets;
    float[] horizontalVelosityLogBeforeJump;
    int[] lineMark;

	void Start () {
        //Draw 5 turkeys and log its velosity to restroe after jumping
        lineMark = new int[1];
        Turkeys = new GameObject[5];
        horizontalVelositySets = new float[5];
        verticalVelositySets = new float[5];
        horizontalVelosityLogBeforeJump = new float[5];
        for (int i = 0; i < 5; i++){
            float directionRandom = Random.Range(0f, 10f);
            if(directionRandom < 5f) {
                horizontalVelositySets[i] = velosityMagnitude;
            }

            else{
                horizontalVelositySets[i] = -1f * velosityMagnitude;
            }
        }

        for (int i = 0; i < 5; i++){
            horizontalVelosityLogBeforeJump[i] = horizontalVelositySets[i];
        }

        for (int i = 0; i < 5; i++){
            GameObject temp = Instantiate(Turkey, new Vector3( Random.Range(-9.2f, -5.8f), -5f, 0f), Quaternion.identity);
            Turkeys[i] = temp;
        }
	}
	

	void Update () {

        //If the turkey is jumping
        for (int i = 0; i < 5; i++){
            if (Turkeys[i].transform.position.y > -5f){
                //High enough to be affected by wind
                if(Turkeys[i].transform.position.y > 0f){
                    horizontalVelositySets[i] += WindForce.GetRealTimeWindForce();
                }
                verticalVelositySets[i] -= 0.01f;
                PositionConstrains(i);
            }
            //Move normally on groud
            else{
                float currentPos = Turkeys[i].transform.position.x;
                //Fix bugs due to frame changing
                if (Turkeys[i].transform.position.y < -5f)
                {
                    Turkeys[i].transform.position = new Vector3(currentPos, -5f, 0f);
                }
                //Store the current velosity in case jumping
                horizontalVelositySets[i] = horizontalVelosityLogBeforeJump[i];
                verticalVelositySets[i] = 0f;
                //Turn around if on the edge
                TurnCheck(i);
                if(IsJump()){
                    Jump(i);
                    horizontalVelosityLogBeforeJump[i] = horizontalVelositySets[i];
                    horizontalVelositySets[i] = 0;
                }
                //If on the mountain or exceed the bound
                PositionConstrains(i);
            }
        }

        for (int i = 0; i < 5; i++){
            Turkeys[i].transform.Translate(new Vector3(horizontalVelositySets[i], verticalVelositySets[i], 0f));
        }
    }

    //Turn aound if reach the bound
    private void TurnCheck(int index){
        if (Turkeys[index].transform.position.x <= -9.2f)
        {
            horizontalVelositySets[index] = velosityMagnitude;
            horizontalVelosityLogBeforeJump[index] = horizontalVelositySets[index];
        }

        else if (Turkeys[index].transform.position.x >= -5.8f)
        {
            horizontalVelositySets[index] = -1 * velosityMagnitude;
            horizontalVelosityLogBeforeJump[index] = horizontalVelositySets[index];
        }
    }

    //Jump generator
    private bool IsJump(){
        float jumpRandom = Random.Range(0f, 750f);
        return jumpRandom < 1f ? true : false;
    }

    //Modify vertical velosity if jumps
    private void Jump(int index){
        verticalVelositySets[index] = 0.35f;
    }

    private void PositionConstrains(int index){
        float currentXPosition = Turkeys[index].transform.position.x;
        Vector2[][] lines = MidpointBisection.GetMountainBounds();
        float posOfTopLeftOfTheMountain = lines[4][0].x;
        //Turkeys cannot go beyond the left wall.
        if (currentXPosition <= -9.2f)
        {
            Turkeys[index].transform.position = new Vector3(-9.2f, Turkeys[index].transform.position.y, 0f);
        }
        //If turkeys on the left side of the mountain
        else if(currentXPosition > -5.8 && currentXPosition < posOfTopLeftOfTheMountain) {
            if(HelperFunctions.IsCollideWithMountain(Turkeys[index], lineMark)){
                //Move left to leave the mountain
                horizontalVelositySets[index] = -1f * velosityMagnitude;
                //Change the y position according to x position
                Turkeys[index].transform.position = new Vector3(currentXPosition, HelperFunctions.CalculateYValueOnALineAccordingToXValue(lines[lineMark[0]], currentXPosition) + 0.1f, 0f);
            }
        }
        //On the right side of the mountain
        else if(currentXPosition >= posOfTopLeftOfTheMountain && currentXPosition < 5)
        {
            if(HelperFunctions.IsCollideWithMountain(Turkeys[index], lineMark)){
                DestroyAndMakeANewOne(Turkeys[index], index);
            }
        }
        //Right side to the Mountain
        else if (currentXPosition >= 5){
            if(Turkeys[index].transform.position.y <= 5f){
                DestroyAndMakeANewOne(Turkeys[index], index);
            }
        }  
    }

    private void DestroyAndMakeANewOne(GameObject objectToDestroy, int index){
        Destroy(objectToDestroy);
        //Instantiate in a valid position
        GameObject temp = Instantiate(Turkey, new Vector3(Random.Range(-9.2f, -5.8f), -5f, 0f), Quaternion.identity);
        Turkeys[index] = temp;
        //Random direction
        float directionRandom = Random.Range(0f, 10f);
        horizontalVelositySets[index] = directionRandom < 5f ? velosityMagnitude : -1f * velosityMagnitude;
        horizontalVelosityLogBeforeJump[index] = horizontalVelositySets[index];
    }

    public Vector3[][] GetAllVertices(){
        Vector3[][] allVertices = new Vector3[5][];
        for (int i = 0; i < 5;i++){
            allVertices[i] = (Turkeys[i].GetComponent(typeof(Turkey)) as Turkey).GetTurkeyPoints();
        }
        //Convert to wolrd space
        for (int i = 0; i < 5; i++){
            for (int j = 0; j < 24; j++){
                allVertices[i][j] = new Vector3(allVertices[i][j].x + Turkeys[i].transform.position.x, allVertices[i][j].y + Turkeys[i].transform.position.y, 0f);
            }
        }

        return allVertices;
    }

    //Getter
    public GameObject[] GetAllTurkeysAsGameObjectArray(){
        return Turkeys;
    }
}
