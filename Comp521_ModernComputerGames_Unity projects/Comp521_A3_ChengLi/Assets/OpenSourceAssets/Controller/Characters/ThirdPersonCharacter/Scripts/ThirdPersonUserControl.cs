using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;


namespace UnityStandardAssets.Characters.ThirdPerson
{
    [RequireComponent(typeof (ThirdPersonCharacter))]
    public class ThirdPersonUserControl : MonoBehaviour
    {
        public GameObject EliminatedText;
        public GameObject TeleportTrap;
        public GameObject win;
        public GameObject lose;
        public GameObject tie;

        private coins[] coins;
        private AI agent;


        private ThirdPersonCharacter m_Character; // A reference to the ThirdPersonCharacter on the object
        private Transform m_Cam;                  // A reference to the main camera in the scenes transform
        private Vector3 m_CamForward;             // The current forward direction of the camera
        private Vector3 m_Move;
        private bool m_Jump;                      // the world-relative desired move direction, calculated from the camForward and user input.
        private int CoinsCollected;
        private int TrapLeft;
        private int AICoinsCollected;
        private bool IsEliminated;

        private void Start()
        {
            // get the transform of the main camera
            CoinsCollected = 0;
            TrapLeft = 2;
            IsEliminated = false;

            if (Camera.main != null)
            {
                m_Cam = Camera.main.transform;
            }
            else
            {
                Debug.LogWarning(
                    "Warning: no main camera found. Third person character needs a Camera tagged \"MainCamera\", for camera-relative controls.", gameObject);
                // we use self-relative controls in this case, which probably isn't what the user wants, but hey, we warned them!
            }

            // get the third person character ( this should never be null due to require component )
            //CanvasIns = Instantiate(canvas, new Vector3(300f, 161f, 0f), Quaternion.identity);
            //text = CanvasIns.GetComponent<Text>();
            m_Character = GetComponent<ThirdPersonCharacter>();
            agent = GameObject.Find("AIAgent(Clone)").GetComponent<AI>() as AI;
            coins = new coins[10];
            coins[0] = GameObject.Find("Coin").GetComponent<coins>() as coins;
            coins[1] = GameObject.Find("Coin (1)").GetComponent<coins>() as coins;
            coins[2] = GameObject.Find("Coin (2)").GetComponent<coins>() as coins;
            coins[3] = GameObject.Find("Coin (3)").GetComponent<coins>() as coins;
            coins[4] = GameObject.Find("Coin (4)").GetComponent<coins>() as coins;
            coins[5] = GameObject.Find("Coin (5)").GetComponent<coins>() as coins;
            coins[6] = GameObject.Find("Coin (6)").GetComponent<coins>() as coins;
            coins[7] = GameObject.Find("Coin (7)").GetComponent<coins>() as coins;
            coins[8] = GameObject.Find("Coin (8)").GetComponent<coins>() as coins;
            coins[9] = GameObject.Find("Coin (9)").GetComponent<coins>() as coins;

        }


        private void Update()
        {
            Vector3 currentPos = this.transform.position;
            this.transform.position = new Vector3(currentPos.x, 2.5f, currentPos.z);
            if(Input.GetKeyDown(KeyCode.Space) && TrapLeft > 0){
                Vector3 temp = this.transform.position;
                GameObject trap = Instantiate(TeleportTrap, new Vector3(temp.x, temp.y + 5f, temp.z), Quaternion.identity);
                Destroy(trap, 2);
                TrapLeft--;
            }
            if (IfAllCollected() || (!agent.gameObject.activeSelf && IsEliminated))
            {
                GameObject enemy = GameObject.Find("Enemy(Clone)");
                Destroy(enemy.gameObject);
                Time.timeScale = 0f;
                AICoinsCollected = agent.GetCoins();
                if(CoinsCollected > AICoinsCollected){
                    Instantiate(win, new Vector3(-31f, -80f, -10f), Quaternion.Euler(90f, 0f, 0f));
                }
                else if(CoinsCollected < AICoinsCollected){
                    Instantiate(lose, new Vector3(-31f, -80f, -10f), Quaternion.Euler(90f, 0f, 0f));
                }
                else{
                    Instantiate(tie, new Vector3(-31f, -80f, -10f), Quaternion.Euler(90f, 0f, 0f));
                }
            }
        }


        // Fixed update is called in sync with physics
        private void FixedUpdate()
        {
            // read inputs
            float h = CrossPlatformInputManager.GetAxis("Horizontal");
            float v = CrossPlatformInputManager.GetAxis("Vertical");
            bool crouch = Input.GetKey(KeyCode.C);

            // calculate move direction to pass to character
            if (m_Cam != null)
            {
                // calculate camera relative direction to move:
                m_CamForward = Vector3.Scale(m_Cam.forward, new Vector3(1, 0, 1)).normalized;
                m_Move = v*m_CamForward + h*m_Cam.right;
            }
            else
            {
                // we use world-relative directions in the case of no main camera
                m_Move = v*Vector3.forward + h*Vector3.right;
            }
#if !MOBILE_INPUT
			// walk speed multiplier
	        if (Input.GetKey(KeyCode.LeftShift)) m_Move *= 0.5f;
#endif

            // pass all parameters to the character control script
            m_Character.Move(m_Move, crouch, false);
            m_Jump = false;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag("coin"))
            {
                other.gameObject.SetActive(false);
                CoinsCollected++;
            }

            if (other.gameObject.CompareTag("enemy"))
            {
                this.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezePositionZ;
                EliminatedText = Instantiate(EliminatedText, new Vector3(-122f, 0.8f, 0f), Quaternion.Euler(new Vector3(90f, 0f, 0f)));
                IsEliminated = true;
            }
        }

        private bool IfAllCollected()
        {
            for (int i = 0; i < 9; i++)
            {
                if (coins[i].gameObject.activeSelf)
                {
                    return false;
                }
            }
            return true;
        }

    }
}
