﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class HTNEliminate {

    public static void Eliminate(GameObject gameObject){
        gameObject.gameObject.SetActive(false);
    }

}
