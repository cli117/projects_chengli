﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HTNPetrol : MonoBehaviour {

    public static void petrol(GameObject gameObject, Vector3 velosity){
        gameObject.transform.Translate(velosity);
    }

}
