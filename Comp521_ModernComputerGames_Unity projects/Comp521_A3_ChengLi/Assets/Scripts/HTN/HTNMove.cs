﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class HTNMove {
    
    public static void MoveTo(GameObject gameObject, Vector3 destination){
        gameObject.transform.position = destination;
    }

}
