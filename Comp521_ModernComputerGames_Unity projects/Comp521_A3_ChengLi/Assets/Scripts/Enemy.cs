﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {
    public GameObject enemy;
    public float speed = 0.2f;


    private GameObject enemy1, enemy2;
    private bool HeadLeft1, HeadLeft2, IsWatch1, IsWatch2, GoThrough1, GoThrough2;

	void Start () {
        HeadLeft1 = true;
        HeadLeft2 = true;
        IsWatch1 = true;
        IsWatch2 = true;
        GoThrough1 = false;
        GoThrough2 = false;
        enemy1 = Instantiate(enemy, new Vector3(73f, 5f, -46f), Quaternion.identity);
        enemy2 = Instantiate(enemy, new Vector3(73f, 5f, 6f), Quaternion.identity);
    }
	
	void Update () {
        Enemy1Move();
        Enemy2Move();
	}

    void Enemy1Move(){
        enemy1.transform.Translate(new Vector3(-speed, 0f, 0f));
        TurnCheck1();
    }

    void Enemy2Move(){
        enemy2.transform.Translate(new Vector3(-speed, 0f, 0f));
        TurnCheck2();
    }

    void TurnCheck1(){
        enemy1.transform.eulerAngles = HeadLeft1 ? new Vector3(0f, 0f, 0f) : new Vector3(0f, 180f, 0f);
        if (enemy1.transform.position.x <= 11f && enemy1.transform.position.x >= -3.5f){
            IsWatch1 = false;
        }
        else{
            GoThrough1 = false;
            IsWatch1 = true;
        }
        if(enemy1.transform.position.x <= -66f){
            HeadLeft1 = false;
        }
        else if(enemy1.transform.position.x >= 73f){
            HeadLeft1 = true;
        }
        else if(enemy1.transform.position.x <= 11f && enemy1.transform.position.x >= -3.5f && HeadLeft1 && !GoThrough1){
            int RandomNum = RandomNumGenerator();
            if(RandomNum == 0){
                HeadLeft1 = false;
            }
            else if (RandomNum == 1){
                Destroy(enemy1);
                enemy1 = Instantiate(enemy, new Vector3(73f, 5f, -46f), Quaternion.identity);
            }
            else{
                GoThrough1 = true;
            }
        }
        else if (enemy1.transform.position.x <= 11f && enemy1.transform.position.x >= -3.5f && !HeadLeft1 && !GoThrough1)
        {
            int RandomNum = RandomNumGenerator();
            if (RandomNum == 0)
            {
                HeadLeft1 = true;
            }
            else if (RandomNum == 1)
            {
                Destroy(enemy1);
                enemy1 = Instantiate(enemy, new Vector3(73f, 5f, -46f), Quaternion.identity);
            }
            else
            {
                GoThrough1 = true;
            }
        }

    }

    void TurnCheck2(){
        enemy2.transform.eulerAngles = HeadLeft2 ? new Vector3(0f, 0f, 0f) : new Vector3(0f, 180f, 0f);
        if (enemy2.transform.position.x <= 11f && enemy2.transform.position.x >= -3.5f){
            IsWatch2 = false;
        }
        else{
            GoThrough2 = false;
            IsWatch2 = true;
        }
        if (enemy2.transform.position.x <= -66f){
            HeadLeft2 = false;
        }
        else if (enemy2.transform.position.x >= 73f){
            HeadLeft2 = true;
        }
        else if (enemy2.transform.position.x <= 11f && enemy2.transform.position.x >= -3.5f && HeadLeft2 && !GoThrough2)
        {
            int RandomNum = RandomNumGenerator();
            if (RandomNum == 0)
            {
                HeadLeft2 = false;
            }
            else if (RandomNum == 1)
            {
                Destroy(enemy2);
                enemy2 = Instantiate(enemy, new Vector3(73f, 5f, 6f), Quaternion.identity);
            }
            else{
                GoThrough2 = true;
            }
        }
        else if (enemy2.transform.position.x <= 11f && enemy2.transform.position.x >= -3.5f && !HeadLeft2 && !GoThrough2)
        {
            int RandomNum = RandomNumGenerator();
            if (RandomNum == 0)
            {
                HeadLeft2 = true;
            }
            else if (RandomNum == 1)
            {
                Destroy(enemy2);
                enemy2 = Instantiate(enemy, new Vector3(73f, 5f, 6f), Quaternion.identity);
            }
            else
            {
                GoThrough2 = true;
            }
        }
    }

    private int RandomNumGenerator(){
        return Random.Range(0, 3);
    }

    public GameObject GetEnemy1(){
        return enemy1;
    }

    public GameObject GetEnemy2(){
        return enemy2;
    }

    public void SetEnemy1(GameObject enemy1){
        this.enemy1 = enemy1;
    }

    public void SetEnemy2(GameObject enemy2){
        this.enemy2 = enemy2;
    }
}
