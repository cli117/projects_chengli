﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAndAISpawn : MonoBehaviour {
    public GameObject player;
    public GameObject AI;

    private Vector3[] SpawnPoints;
    private Vector3 PlayerSpawnPoint;
    private Vector3 AISpawnPoint;

    private GameObject PlayerInstance;
    private GameObject AIInstance;

	// Use this for initialization
	void Start () {
        SpawnPoints = new Vector3[]{
                new Vector3(-50f, 2.5f, 40f), new Vector3(-20f, 3f, 40f), new Vector3(4f, 3f, 40f), new Vector3(31f, 3f, 40f), new Vector3(60f, 3f, 40f),
                new Vector3(-50f, 2.5f, -76.5f), new Vector3(-20f, 3f, -76.5f), new Vector3(4f, 3f, -76.5f), new Vector3(31f, 3f, -76.5f), new Vector3(60f, 3f, -76.5f)
            };
        int random = Random.Range(0, 9);
        PlayerSpawnPoint = SpawnPoints[random];
        PlayerInstance = Instantiate(player, PlayerSpawnPoint, Quaternion.identity);
        int random1 = Random.Range(0, 9);
        while(random1 == random){
            random1 = Random.Range(0, 9);
        }
        AISpawnPoint = SpawnPoints[random1];
        AIInstance = Instantiate(AI, AISpawnPoint, Quaternion.identity);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public GameObject GetPlayer(){
        return PlayerInstance;
    }

    public GameObject GetAI(){
        return AIInstance;
    }
}
