﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class HelperFunctions {
    //Helper functions to do calculation
    public static float CalculateDistanceBetweenTwoPoints(Vector3 a, Vector3 b){
        return Mathf.Sqrt(Mathf.Pow(a.x - b.x, 2) + Mathf.Pow(a.y - b.y, 2) + Mathf.Pow(a.z - b.z, 2));
    }

    public static Vector3 GetASpawnPosition(){
        Vector3[] SpawnPoints = {
                new Vector3(-50f, 3f, 25f), new Vector3(-22.5f, 3f, 25f), new Vector3(4f, 3f, 25f), new Vector3(31f, 3f, 25f), new Vector3(60f, 3f, 25f),
                new Vector3(-50f, 3f, -58.5f), new Vector3(-22.5f, 3f, -58.5f), new Vector3(4f, 3f, -58.5f), new Vector3(-31f, 3f, -58.5f), new Vector3(60f, 3f, -58.5f)
            };
        int random = Random.Range(0, 9);
        return SpawnPoints[random];
    }
}
