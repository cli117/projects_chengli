﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI : MonoBehaviour {
    public GameObject Trap;
    private UnityEngine.AI.NavMeshAgent agent;
    private GameObject Player;
    private int CoinsCollected;
    private int TrapLeft;
    private coins destination;
    private coins[] coins;
    private float DistancePlayer;
    private float[] DistanceCoins;
    private Vector3 CurrentPos;

    // Use this for initialization
    void Start () {
        Player = GameObject.Find("ThirdPersonController(Clone)");
        coins = new coins[10];
        DistanceCoins = new float[10];
        agent = GameObject.Find("AIAgent(Clone)").GetComponent<UnityEngine.AI.NavMeshAgent>() as UnityEngine.AI.NavMeshAgent;
        coins[0] = GameObject.Find("Coin").GetComponent<coins>() as coins;
        coins[1] = GameObject.Find("Coin (1)").GetComponent<coins>() as coins;
        coins[2] = GameObject.Find("Coin (2)").GetComponent<coins>() as coins;
        coins[3] = GameObject.Find("Coin (3)").GetComponent<coins>() as coins;
        coins[4] = GameObject.Find("Coin (4)").GetComponent<coins>() as coins;
        coins[5] = GameObject.Find("Coin (5)").GetComponent<coins>() as coins;
        coins[6] = GameObject.Find("Coin (6)").GetComponent<coins>() as coins;
        coins[7] = GameObject.Find("Coin (7)").GetComponent<coins>() as coins;
        coins[8] = GameObject.Find("Coin (8)").GetComponent<coins>() as coins;
        coins[9] = GameObject.Find("Coin (9)").GetComponent<coins>() as coins;
        CoinsCollected = 0;
        TrapLeft = 2;
        ResetDistances();
    }
	
	// Update is called once per frame
	void Update () {
        CurrentPos = this.transform.position;
        //DistancePlayer = HelperFunctions.CalculateDistanceBetweenTwoPoints(CurrentPos, Player.transform.position);
        for (int i = 0; i < 10; i++){
            if (coins[i].gameObject.activeSelf)
            {
                DistanceCoins[i] = HelperFunctions.CalculateDistanceBetweenTwoPoints(CurrentPos, coins[i].transform.position);
            }
        }
        float currentDistance = 1000;
        destination = coins[0];
        for (int i = 0; i < 10; i++){
            if(DistanceCoins[i] < currentDistance){
                currentDistance = DistanceCoins[i];
                destination = coins[i];
            }
        }
        int trapCo = Random.Range(0, 2500);
        Vector3 temp = this.transform.position;
        if(trapCo == 1 && TrapLeft > 0){
            GameObject trap = Instantiate(Trap, new Vector3(temp.x, temp.y + 5f, temp.z), Quaternion.identity);
            TrapLeft--;
            Destroy(trap, 2);
        }

        agent.isStopped = false;
        agent.SetDestination(destination.transform.position);
        ResetDistances();
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("coin"))
        {
            other.gameObject.SetActive(false);
            CoinsCollected++;
        }
        if (other.gameObject.CompareTag("enemy"))
        {
            this.gameObject.SetActive(false);
        }
    }

    private void ResetDistances(){
        DistancePlayer = 1000f;
        for (int i = 0; i < 10; i++){
            DistanceCoins[i] = 1000;
        }
    }

    public int GetCoins()
    {
        return CoinsCollected;
    }
}

   
