﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class trap : MonoBehaviour
{
    public GameObject enemy;

    private Vector3[] positions;
    private bool IsPlayer, IsEnemy1, IsEnemy2, IsAI, IsPlayerPlaced;
    private float OwnerDistance;
    private float EPSILON;
    private Vector3[] SpawnPoints;
    private int PlayerIndex, AIIndex;
    private UnityEngine.AI.NavMeshAgent ai;
    private GameObject player;

    // Use this for initialization
    void Start()
    {
        player = GameObject.Find("ThirdPersonController(Clone)");
        ai = GameObject.Find("AIAgent(Clone)").GetComponent<UnityEngine.AI.NavMeshAgent>() as UnityEngine.AI.NavMeshAgent;
        IsPlayerPlaced = true;
        IsAI = false;
        IsPlayerPlaced = false;
        IsPlayer = false;
        IsEnemy1 = false;
        IsEnemy2 = false;
        PlayerIndex = -1;
        AIIndex = -1;
        OwnerDistance = 5f;
        EPSILON = 0.01f;
        SpawnPoints = new Vector3[]{
                new Vector3(-50f, 2.5f, 40f), new Vector3(-20f, 3f, 40f), new Vector3(4f, 3f, 40f), new Vector3(31f, 3f, 40f), new Vector3(60f, 3f, 40f),
                new Vector3(-50f, 2.5f, -76.5f), new Vector3(-20f, 3f, -76.5f), new Vector3(4f, 3f, -76.5f), new Vector3(31f, 3f, -76.5f), new Vector3(60f, 3f, -76.5f)
            };

        UseTrap();
    }


    public void UseTrap()
    {
        Enemy EnemyGenerator = GameObject.Find("EnemiesGenerator").GetComponent<Enemy>() as Enemy;
        GameObject enemy1 = EnemyGenerator.GetEnemy1();
        GameObject enemy2 = EnemyGenerator.GetEnemy2();
        PlayerAndAISpawn pa = GameObject.Find("PlayerAndAISpawn").GetComponent<PlayerAndAISpawn>() as PlayerAndAISpawn;
        GameObject AI = pa.GetAI();
        GameObject Player = pa.GetPlayer();
        GameObject ToTeleport = GOToTeleport(enemy1, enemy2, AI, Player);
        if (IsAI){
            AIIndex = Random.Range(0, 9);
            ai.Warp(SpawnPoints[AIIndex]);
        }
        else if(IsPlayer){
            PlayerIndex = Random.Range(0, 9);
            player.transform.position = SpawnPoints[PlayerIndex];
        }
        else if(IsEnemy1){
            Destroy(ToTeleport);
            enemy1 = Instantiate(enemy, new Vector3(73f, 5f, -46f), Quaternion.identity);
            EnemyGenerator.SetEnemy1(enemy1);
        }
        else if(IsEnemy2){
            Destroy(ToTeleport);
            enemy2 = Instantiate(enemy, new Vector3(73f, 5f, 6f), Quaternion.identity);
            EnemyGenerator.SetEnemy2(enemy2);
        }

    }

    private GameObject GOToTeleport(GameObject enemy1, GameObject enemy2, GameObject AI, GameObject Player)
    {
        positions = new Vector3[]{
            enemy1.transform.position, enemy2.transform.position, AI.transform.position, Player.transform.position
        };
        float distance1 = HelperFunctions.CalculateDistanceBetweenTwoPoints(this.transform.position, positions[0]);
        float distance2 = HelperFunctions.CalculateDistanceBetweenTwoPoints(this.transform.position, positions[1]);
        float distance3 = HelperFunctions.CalculateDistanceBetweenTwoPoints(this.transform.position, positions[2]);
        float distance4 = HelperFunctions.CalculateDistanceBetweenTwoPoints(this.transform.position, positions[3]);
        float[] distances = new float[3];
        distances[0] = distance1;
        distances[1] = distance2;
        if (distance3 - OwnerDistance <= EPSILON)
        {
            distances[2] = distance4;
        }
        else
        {
            distances[2] = distance3;
            IsPlayerPlaced = true;
        }

        if (distances[0] <= distances[1] && distances[0] <= distances[2])
        {
            IsEnemy1 = true;
            return enemy1;
        }
        else if (distances[1] <= distances[0] && distances[1] <= distances[2])
        {
            IsEnemy2 = true;
            return enemy2;
        }
        else if (IsPlayerPlaced)
        {
            IsAI = true;
            return AI;
        }
        else
        {
            IsPlayer = true;
            return Player;
        }
    }
}
