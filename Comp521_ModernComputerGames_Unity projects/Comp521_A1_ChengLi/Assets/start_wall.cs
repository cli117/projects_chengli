﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class start_wall : MonoBehaviour {
    public GameObject floor;
    public GameObject sideWalls;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    //Player at the gap, not falling, instantiate a floor and the sidewall of the maze
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Player")){
            Instantiate(floor, new Vector3(56.0f, 14.5f, 20.0f), Quaternion.identity);
            Instantiate(sideWalls, new Vector3(170.51f, 19.0f, 272.0f), Quaternion.identity);
            Instantiate(sideWalls, new Vector3(90.51f, 19.0f, 272.0f), Quaternion.identity);
        }
    }
}
