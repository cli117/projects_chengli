﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallHit : MonoBehaviour {
    private int hitPoints;
	// Use this for initialization
	void Start () {
        hitPoints = 3;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    //Destroyed if be hit 3 times
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Projectile"))
        {
            Destroy(other.gameObject);
            hitPoints--;
            if (hitPoints <= 0) Destroy(this.gameObject);

        }
    }

}
