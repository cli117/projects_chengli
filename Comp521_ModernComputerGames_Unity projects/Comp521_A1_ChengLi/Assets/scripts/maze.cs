﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class maze : MonoBehaviour {

    public GameObject wall;
    public GameObject projectile;
    public float wallLength = 1.0f;
    public int xSize = 8;
    public int ySize = 8;
    private Vector3 initalPos;


	// Use this for initialization
	void Start () {
        creatWalls();
	}
	
    void creatWalls()
    {
        initalPos = new Vector3((-xSize/2) + (wallLength/2), 0.0f, (-ySize/2) + (wallLength/2));
        Vector3 myPos = initalPos;
        //x-axis
        for(int i = 0; i < ySize; i++)
        {
            for(int j = 0; j <= xSize; j++)
            {
                myPos = new Vector3(initalPos.x + (j * wallLength * 10) - (wallLength/2) + 95.0f, 0.0f, initalPos.z + (i * wallLength * 10) - (wallLength / 2) + 45.0f);
                Instantiate(wall, myPos, Quaternion.identity);
            }
        }
        //y-axis
        for (int i = 0; i <= ySize; i++)
        {
            for (int j = 0; j < xSize; j++)
            {
                myPos = new Vector3(initalPos.x + (j * wallLength * 10) - (wallLength / 2) + 100.0f, 0.0f, initalPos.z + (i * wallLength * 10) - (wallLength / 2) + 40.0f);
                Instantiate(wall, myPos, Quaternion.Euler(0.0f, 90.0f, 0.0f));

            }
        }
        for (int i = 0; i < ySize; i++)
        {
            for (int j = 0; j < xSize; j++)
            {
                myPos = new Vector3(initalPos.x + (j * wallLength * 10) - (wallLength / 2) + 100.0f, 1.0f, initalPos.z + (i * wallLength * 10) - (wallLength / 2) + 45.0f);
                Instantiate(projectile, myPos, Quaternion.identity);
            }
        }
    }

	// Update is called once per frame
	void Update () {
		
	}
}
