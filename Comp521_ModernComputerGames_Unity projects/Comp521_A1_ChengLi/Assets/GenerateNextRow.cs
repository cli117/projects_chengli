﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateNextRow : MonoBehaviour
{
    //GameObjects to instantiate the maze
    public GameObject wall;
    public GameObject firstRowWall;
    public GameObject projectiles;
    private GameObject[] backWalls = new GameObject[8];
    private GameObject[] sideWalls = new GameObject[7];
    //setLog to save the setLog of current row
    private int[] setLog = new int[8];
    //nextSetLog fot the next row
    private int[] nextSetLog = new int[8];
    //first row cannot be destroyed, need a bool to specify
    private bool firstRow = true;
    //log the number of rows to compute the Vector3 value
    private int numOfRows = 0;
    //set need a ID
    private int setID = 8;
    //if more than 2 continous cells are joined, the value need to be backtraced! e.g. 7 8 0/ 1 1 1 1 1
    private int continuousCellNum = 0;
    private int continuousCellStart = 0;
    // Use this for initialization
    void Start()
    {


    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            //If a player collied the wall, generate next row!
            //First roll is different, specification
            if (firstRow)
            {
                this.transform.position = new Vector3(130.51f, 19.0f, (31.8f + 10 * numOfRows));
                firstRow = false;
                //Merge the set randomly
                int[] log = { 0, 1, 2, 3, 4, 5, 6, 7 };
                for (int i = 0; i < 7; i++)
                {
                    int randomNum = Random.Range(0, 2);
                    continuousCellNum++;
                    if (randomNum == 0)
                    {
                        log[i + 1] = log[i];
                    }
                }

                setLog = log;
                //Then build the wall
                for (int i = 0; i < 7; i++)
                {
                    if (log[i] != log[i + 1])
                    {
                        Instantiate(wall, new Vector3((160.51f - 10.0f * i), 19.0f, (26.8f + 10.0f * numOfRows)), Quaternion.identity);
                    }
                }

                numOfRows++;
                //First row done.
                //Projectiles
                for (int i = 0; i < 8; i++)
                {
                    Instantiate(projectiles, new Vector3((165.51f - 10.0f * i), 16.5f, 26.8f), Quaternion.identity);
                }
            }
            else
            {
                //Reset the log array
                int[] log = { -1, -1, -1, -1, -1, -1, -1, -1 };
                //initial the next row's set condition
                nextSetLog = log;
                //number to log the #holes of a set with next row
                int numOfHoles = 0;
                //move the invisible wall to next position, ready to generate next row or terminate
                this.transform.position = new Vector3(130.51f, 19.0f, (31.8f + 10 * numOfRows));
                //instantiate all walls
                for (int i = 0; i < 8; i++)
                {
                    GameObject temp = Instantiate(wall, new Vector3((165.51f - 10.0f * i), 19.0f, (21.8f + 10.0f * numOfRows)), Quaternion.Euler(0.0f, 90.0f, 0.0f));
                    Instantiate(projectiles, new Vector3((165.51f - 10.0f * i), 16.5f, (26.8f + 10.0f * numOfRows)), Quaternion.identity);
                    backWalls[i] = temp;
                }

                for (int i = 0; i < 7; i++)
                {
                    GameObject temp = Instantiate(wall, new Vector3((160.51f - 10.0f * i), 19.0f, (26.8f + 10.0f * numOfRows)), Quaternion.identity);
                    sideWalls[i] = temp;
                }

                //**core part!!! Took me hundreds of hours!!!
                //nextSet is one of the element of the chosen set's position in the array, and nextSetSave is a temp var to do calculation

                int nextSet = 0, nextSetSave = 0;
                //If no hole is digged within a set, redo the process
                while (true)
                {

                    for (int i = 0; i < 8; i++)
                    {
                        //Dig holes randomly
                        if (setLog[i] == setLog[nextSet])
                        {
                            int randomNum = Random.Range(0, 2);
                            if (randomNum == 0)
                            {
                                Destroy(backWalls[i].gameObject);
                                numOfHoles++;
                                nextSetLog[i] = setLog[i];
                            }
                        }
                        else
                        {
                            //If not the same set, and it has not been considered, save it to do the next round of digging
                            if (setLog[i] != -1)
                            {
                                nextSetSave = i;
                            }
                        }
                    }
                    //No hole digged, redo the process
                    if (numOfHoles == 0)
                    {
                        continue;
                    }
                    //All sets done
                    else if (nextSetSave - nextSet == 0) break;
                    else
                    {

                        int copy = setLog[nextSet];
                        for (int i = 0; i < 8; i++)
                        {
                            if (copy == setLog[i]) setLog[i] = -1;
                        }
                        nextSet = nextSetSave;
                        numOfHoles = 0;
                    }
                }
                //Digging done, substitude all -1 with a different setID
                for (int i = 0; i < 8; i++)
                {
                    if (nextSetLog[i] == -1)
                    {
                        nextSetLog[i] = setID;
                        setID++;
                    }
                }
                //Merge the new row

                for (int i = 0; i < 7; i++)
                {
                    //Destroy side walls randomly if they are not in the same set
                    int randomNum = Random.Range(0, 2);
                    if (randomNum == 0 && (nextSetLog[i] != nextSetLog[i + 1]))
                    {
                        Destroy(sideWalls[i].gameObject);
                        continuousCellNum++;
                        //After merge the cells, merge the sets

                        int setIDCopy = nextSetLog[i + 1];
                        for (int j = 0; j < 8; j++)
                        {
                            if (nextSetLog[j] == setIDCopy)
                            {
                                nextSetLog[j] = nextSetLog[i];
                            }
                        }
                    }

                }

                setLog = nextSetLog;

                numOfRows++;
            }
        }
        //If player shoot to the void, complete the maze
        if (other.gameObject.CompareTag("Projectile") && !firstRow)
        {
            Destroy(this.gameObject);
            for (int i = 0; i < 8; i++)
            {
                Instantiate(firstRowWall, new Vector3((165.51f - 10.0f * i), 19.0f, this.transform.position.z + 10f), Quaternion.Euler(0.0f, 90.0f, 0.0f));
            }

            for (int i = 0; i < 7; i++)
            {
                GameObject temp = Instantiate(wall, new Vector3((160.51f - 10.0f * i), 19.0f, (26.8f + 10.0f * numOfRows)), Quaternion.identity);
                sideWalls[i] = temp;
            }

            for (int i = 0; i < 7; i++)
            {
                if (setLog[i] != setLog[i + 1])
                {
                    Destroy(sideWalls[i].gameObject);

                    //merge sets until all belong to the same
                    int setIDCopy = setLog[i + 1];
                    for (int j = 0; j < 8; j++)
                    {
                        if (setLog[j] == setIDCopy)
                        {
                            setLog[j] = setLog[i];
                        }
                    }
                }
            }
        }
    }
}