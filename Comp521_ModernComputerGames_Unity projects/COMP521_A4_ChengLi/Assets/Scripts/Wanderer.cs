﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Wanderer : MonoBehaviour
{

    private TravellerManager TravellerManager;
    private List<Traveller> TravellerAsList;
    private NavMeshAgent agent;
    private bool isInterfere;


    void Start()
    {
        agent = this.GetComponent<NavMeshAgent>();
        TravellerManager = GameObject.Find("TravellerManager").GetComponent<TravellerManager>();
        TravellerAsList = TravellerManager.GetTravellers();
        agent.isStopped = false;
        agent.destination = new Vector3(Random.Range(-12f, 12f), 0f, Random.Range(-6f, 6f));
        isInterfere = false;
    }


    void Update()
    {
        int WanderCo = Random.Range(0, 30);
        if (WanderCo == 1 && !isInterfere)
        {
            agent.isStopped = false;
            agent.destination = new Vector3(Random.Range(-12f, 12f), 0f, Random.Range(-6f, 6f));
        }
        agent.speed = Random.Range(3f, 4f);

        if (TravellerAsList.Exists(v => HelperFunctions.CalculateDistanceBetweenTwoPoints(v.transform.position, transform.position) < 1f))
        {
            Traveller close = TravellerAsList.Find(v => HelperFunctions.CalculateDistanceBetweenTwoPoints(v.transform.position, transform.position) < 1f);
            agent.destination = HelperFunctions.CalculatePosBetweenTwoVectors(close.transform.position, close.GetDestination());
            isInterfere = true;
        }
        else
        {
            isInterfere = false;
        }
    }
}

