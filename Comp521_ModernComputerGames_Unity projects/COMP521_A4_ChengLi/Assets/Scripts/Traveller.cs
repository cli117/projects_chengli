﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Traveller : MonoBehaviour {

    private UnityEngine.AI.NavMeshAgent agent;
    private Vector3[] destinations;
    private int index;
    private Vector3 DestinationAsVector3;
    private float InitialTime;
    private float TimeSinceSpwan;

    void Start () {
        destinations = new Vector3[]
        {
            new Vector3(-12f, 0f, 3.5f),
            new Vector3(-12f, 0f, -3.5f)
        };
        index = Random.Range(0, 2);
        agent = this.GetComponent<UnityEngine.AI.NavMeshAgent>() as UnityEngine.AI.NavMeshAgent;
        agent.SetDestination(destinations[index]);
        DestinationAsVector3 = destinations[index];
        agent.speed = Random.Range(1.5f, 4f);
        InitialTime = Time.time;
    }
	
	void Update () {
        agent.speed = Random.Range(1.5f, 4f);
        agent.isStopped = false;
        agent.SetDestination(destinations[index]);
        TimeSinceSpwan = Time.time - InitialTime;
        //Cant get to the destination, may change mind.
        if (TimeSinceSpwan > 20f)
        {
            InitialTime = Time.time;
            index = Random.Range(0, 2);
        }

        if(this.transform.position.x < -12f)
        {
            float oldz = this.transform.position.z;
            this.transform.position = new Vector3(-11.8f, 0f, oldz);
        }
        if (this.transform.position.z < -6f)
        {
            float oldx = this.transform.position.x;
            this.transform.position = new Vector3(oldx, 0f, -5.8f);
        }
        else if (this.transform.position.z > 6f)
        {
            float oldx = this.transform.position.x;
            this.transform.position = new Vector3(oldx, 0f, 5.8f);
        }
    }

    public Vector3 GetDestination()
    {
        return DestinationAsVector3;
    }

    public void InitialTimeReset()
    {
        this.InitialTime = Time.time;
    }
}
