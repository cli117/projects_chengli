﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleGenerator : MonoBehaviour {
    public GameObject obstacle1;
    public GameObject obstacle2;
    public GameObject obstacle3;
    public GameObject obstacle4;
    public GameObject obstacle5;
    public int NumOfObstacles;

    private GameObject[] obstacles;
    private List<Vector3> pos;
    // Use this for initialization
    void Start () {
        pos = new List<Vector3>();
        obstacles = new GameObject[]
        {
            obstacle1, obstacle2, obstacle3, obstacle4, obstacle5
        };
        int count = 0;

        while(count < NumOfObstacles)
        {
            int index = Random.Range(0, 5);
            float scale = Random.Range(6f, 8f);
            float xCor = Random.Range(-9.5f, 9.5f);
            float zCor = Random.Range(-3.5f, 3.5f);
            obstacles[index].transform.localScale = new Vector3(scale, 3f, scale);
            Vector3 position = new Vector3(xCor, 0f, zCor);
            if (pos.TrueForAll(v => HelperFunctions.CalculateDistanceBetweenTwoPoints(position, v) > 3f))
            {
                pos.Add(position);
            }
            else
            {
                continue;
            }
            Instantiate(obstacles[index], position, Quaternion.identity);
            count++;
        }

    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
