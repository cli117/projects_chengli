﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SocialAgent : MonoBehaviour {
    private NavMeshAgent agent;
    private List<SocialAgent> socialAgents;
    private SocialAgentManager socialAgentManager;
    private float TalkTime;
    private float cooldown;
    private float timer, cdTimer;

    void Start () {
        agent = this.GetComponent<NavMeshAgent>();
        socialAgentManager = GameObject.Find("SocialAgentManager").GetComponent<SocialAgentManager>();
        agent.isStopped = false;
        agent.destination = new Vector3(Random.Range(-12f, 12f), 0f, Random.Range(-6f, 6f));
        TalkTime = Random.Range(0.5f, 2f);
        timer = 0f;
        cooldown = 3f;
        cdTimer = 0f;
    }
	
	// Update is called once per frame
	void Update () {
        socialAgents = socialAgentManager.GetSocialAgents();
        agent.speed = Random.Range(1.5f, 4f);
        int WanderCo = Random.Range(0, 30);
        if (WanderCo == 1)
        {
            agent.isStopped = false;
            agent.destination = new Vector3(Random.Range(-12f, 12f), 0f, Random.Range(-6f, 6f));
        }
        cdTimer += Time.deltaTime;
        if(cdTimer > cooldown)
        {
            if (socialAgents.Exists(v => (HelperFunctions.CalculateDistanceBetweenTwoPoints(v.transform.position, this.transform.position) > 0f) && (HelperFunctions.CalculateDistanceBetweenTwoPoints(v.transform.position, this.transform.position) < 1f)))
            {
                this.agent.enabled = false;
                SocialAgent other = socialAgents.Find(v => (HelperFunctions.CalculateDistanceBetweenTwoPoints(v.transform.position, this.transform.position) > 0f) && (HelperFunctions.CalculateDistanceBetweenTwoPoints(v.transform.position, this.transform.position) < 1f));
                other.agent.enabled = false;
                timer += Time.deltaTime;
            if (timer > TalkTime)
                {
                    TalkTime = Random.Range(0.5f, 2f);
                    this.agent.enabled = true;
                    other.agent.enabled = true;
                    cdTimer = 0f;
                }

            }
        }

    }
}
