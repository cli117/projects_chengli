﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TravellerManager : MonoBehaviour {
    public GameObject Traveller;
    public int NumOfTraveller;

    private List<Traveller> travellers;

	void Start () {
        travellers = new List<Traveller>();
        Vector3 SpawnPos = new Vector3(12f, 0f, 0f);
        for (int i = 0; i < NumOfTraveller; i++)
        {
            travellers.Add(Instantiate(Traveller, SpawnPos, Quaternion.identity).GetComponent<Traveller>());
        }
    }
	
	void Update () {

	}

    public List<Traveller> GetTravellers()
    {
        return travellers;
    }


}
