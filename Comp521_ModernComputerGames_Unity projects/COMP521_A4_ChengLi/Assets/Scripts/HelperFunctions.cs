﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class HelperFunctions
{
    //Helper functions to do calculation
    public static float CalculateDistanceBetweenTwoPoints(Vector3 a, Vector3 b)
    {
        return Mathf.Sqrt(Mathf.Pow(a.x - b.x, 2) + Mathf.Pow(a.y - b.y, 2) + Mathf.Pow(a.z - b.z, 2));
    }

    public static float CalculateVectorLength(Vector2 vector)
    {
        return Mathf.Sqrt(Mathf.Pow(vector.x, 2) + (Mathf.Pow(vector.y, 2)));
    }

    public static Vector3 CalculatePosBetweenTwoVectors(Vector3 a, Vector3 b)
    {
        return new Vector3((a.x + ((a.x + b.x) / 2)) / 2, (a.y + ((a.y + b.y) / 2)) / 2, (a.z + ((a.z + b.z) / 2)) / 2);
    }
}
