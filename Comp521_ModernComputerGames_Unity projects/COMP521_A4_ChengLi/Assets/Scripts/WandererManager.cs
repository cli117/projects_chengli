﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WandererManager : MonoBehaviour {
    public GameObject Wanderer;
    public int NumOfWanderers;
	// Use this for initialization
	void Start () {
        for (int i = 0; i < NumOfWanderers; i++)
        {
            Instantiate(Wanderer, new Vector3(Random.Range(-12f, 12f), 0f, Random.Range(-6f, 6f)), Quaternion.identity);
        }

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
