﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SocialAgentManager : MonoBehaviour {
    public GameObject SocialAgent;
    public int NumOfSocialAgents;
    private List<SocialAgent> socialAgents;

	// Use this for initialization
	void Start () {
        socialAgents = new List<SocialAgent>();
		for (int i = 0; i < NumOfSocialAgents; i++)
        {
            socialAgents.Add(Instantiate(SocialAgent, new Vector3(Random.Range(-12f, 12f), 0f, Random.Range(-6f, 6f)), Quaternion.identity).GetComponent<SocialAgent>());
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public List<SocialAgent> GetSocialAgents()
    {
        return socialAgents;
    }
}
