﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destination : MonoBehaviour {
    public GameObject Traveller; 
    private Vector3 SpawnPos;

	// Use this for initialization
	void Start () {
        SpawnPos = new Vector3(12f, 0f, 0f);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        //Respawn and reset the spawn time.
        if (other.gameObject.CompareTag("Traveller"))
        {
            other.gameObject.transform.position = new Vector3(12f, 0f, 0f);
            other.GetComponent<Traveller>().InitialTimeReset();
        }
    }
}
