# Program to implement the Dijkstra's GCD algorithm

	.data		# variable declarations follow this line
str1: 	.asciiz "Enter the first integer: "
str2: 	.asciiz "Enter the second integer: "                  
														
	.text		# instructions follow this line	
	
main:     		# indicates start of code to test lcm the procedure
       li $v0, 4        # take the first user input and store it in $a1
       la $a0, str1
       syscall
       li $v0, 5
       syscall
       move $a1, $v0
       
       li $v0, 4        # take the second user input and store it in $a2
       la $a0, str2
       syscall
       li $v0, 5
       syscall
       move $a2, $v0
       
       jal gcd          # jump to gcd and take the value of gcd in return
       
       move $a0, $v0    # print the "gcd"
       li $v0, 1
       syscall
       
       li $v0, 10	# exit
       syscall										                    																	        																                    																	                    																	                    																	                    
gcd:	     		# the ��lcm"? procedure
       beq $a1, $a2, done # check if the two calculated inputs are equal
       bgt $a1, $a2, loop # $a1 = $a1 - $a2 if $a1 is greater
       sub $a2, $a2, $a1  # $a2 = $a2 - $a1 if $a2 is greater
       j gcd
loop:
       sub $a1, $a1, $a2
       j gcd
done:
       move $v0, $a1      #return the value of gcd
       jr $ra							
# End of program
