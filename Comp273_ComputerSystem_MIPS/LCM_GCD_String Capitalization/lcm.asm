# Program to calculate the least common multiple of two numbers

	.data		# variable declarations follow this line
first: 	.word 10	# the first integer
second: .word 15    	# the second integer                  
														
	.text		# instructions follow this line	
																	                    
main:     		# indicates start of code to test lcm the procedure
       la $s0, first
       lw $a0, ($s0)
       la $s1, second
       lw $a1, ($s1)
       move $t0, $a0    # store the value of $a0 into $t0
       move $t1, $a1    # store the value of $a1 into $t1
       
       jal lcm          # find the lcm
       
       move $a0, $v0    # print the "lcm"
       li $v0, 1
       syscall
       
       li $v0, 10	# exit
       syscall
lcm:	     		# the "lcm" procedure
       blt $a0, $a1,loop# if $a0 is less than $a1, then add $t0 to $a0
       beq $a0, $a1 done# if $a0 is equal to $a1, we know the lcm
       add $a1, $a1, $t1# if $a0 is greater than $a1, then add $t1 to $a1
       j lcm            # go back to lcm and check if we have found the lcm
loop:
       add $a0, $a0, $t0
       j lcm            # go back to lcm and check if we have found the lcm
done:
       move $v0, $a0
       jr $ra   	# return the value of lcm							
# End of program
