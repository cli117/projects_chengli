# Program to capitalize the first letter of a string

	.data		# variable declarations follow this line
       str: 	 .asciiz "Enter the string to capitalize: "
 userInput:      .space 127                
														
	.text		# instructions follow this line	
																	                    
main:     		# indicates start of code to test "upper" the procedure
       li $v0, 4        # take the user input and store it in $a0
       la $a0, str
       syscall
       li $v0, 8
       la $a0, userInput
       li $a1, 127
       syscall
       la $t0, userInput
       move $s0, $t0
       
       jal upper        # Capitalization
       
       la $a0, ($s0)    # print the "Capitalized String"
       li $v0, 4
       syscall
       
       li $v0, 10	# exit
       syscall
upper:	     		# the "upper"? procedure
       lb $t1, ($t0)         # the first byte of processed string
       beq $t1, 32, nextToSpace # if it is still a space, process the byte behind it
       sle $t2, $t1, 122     # check if it is a lowercase char
       sge $t3, $t1, 97
       add $t4, $t2, $t3
       bne $t4, 2, checkNext # if not, check if the next byte is a space
       subi $t1, $t1, 32     # if so,cap it 
       sb $t1, ($t0)
       j checkNext           # then check if the next byte is a space
checkNext:
       addi $t0, $t0, 1      # move to next byte
       lb $t1, ($t0)
       beq $t1, 0, done      # if there is no more bytes, print
       beq $t1, 32, nextToSpace # if it is a space, then process the byte behind it
       j checkNext
nextToSpace:
       addi $t0, $t0, 1
       j upper
done:
       jr $ra

# End of program
