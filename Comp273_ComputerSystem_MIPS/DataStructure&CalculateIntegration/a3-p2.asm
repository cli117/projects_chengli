# Name:Cheng Li
# Student ID: 260706615

# Problem 2 - Dr. Ackermann or: How I Stopped Worrying and Learned to Love Recursion
###########################################################
.data
error: .asciiz "error: m, n must be non-negative integers\n"
m: .asciiz "Please enter the value of m\n"
n: .asciiz "Please enter the value of n\n"

.text 
###########################################################
main:
# get input from console using syscalls
        li $v0, 4             # read an integer from user for m and save it into $s0
        la $a0, m
        syscall
        li $v0, 5
        syscall
        add $s0, $v0, $0
        
          
        li $v0, 4             # read an integer from user for n and save it into $s1
        la $a0, n
        syscall
        li $v0, 5
        syscall
        add $s1, $v0, $0
        
        add $a0, $s0, $0      
        add $a1, $s1, $0
# compute A on inputs 
	jal A                # jump to subroutine and calculate the result
# print value to console and exit with status 0
        move $a0, $v0
        li $v0, 1
        syscall
        
        li $v0, 17     # exit with status 0
        li $a0, 0
        syscall
        
        
        
###########################################################
# int A(int m, int n)
# computes Ackermann function
A:      
        addi $sp, $sp, -12    # make room in the stack to store 3 registers 
        sw $ra, 0($sp)
        sw $s0, 4($sp)
        sw $s1, 8($sp)
        jal check             # check if it is a valid input
first:
        bne $a0, $0, second   # branch if m is not equal to 0
        addi $v0, $a1, 1      # return n+1 if m == 0
        j return
second:
        bne $a1, $0, third    # branch if n is not equal to 0
        
        addi $a1, $0, 1       # return A(m-1,1)
        addi $a0, $a0, -1
        jal A
        j return
third:
        add $s0, $a0, $0      # save the value of m for the second call
        addi $a1, $a1, -1
        jal A                 # get the value of A(m,(n-1))
        
        addi $a0, $s0, -1
        add $a1,$v0, $0
        jal A                 # get the value of A(m-1,A(m,(n-1)))
        j return   
return:
        lw $ra, 0($sp)        # restore all the values
        lw $s0, 4($sp)
        lw $s1, 8($sp)
        addi $sp, $sp, 12        
	jr $ra
	
###########################################################
# void check(int m, int n)
# checks that n, m are natural numbers
# prints error message and exits with status 1 on fail
check:
        slt $t0, $a0, $0      # check if both m and n are natural numbers
        slt $t1, $a1, $0
        add $t2, $t0, $t1
        bne $t2, $0, exitWithException
	jr $ra

exitWithException:           # if not, printing an appropriate error message and exits with status 1 on fail
        li $v0, 4
        la $a0, error
        syscall
        
        li $v0, 17          # exit with status 1
        li $a0, 1
        syscall
