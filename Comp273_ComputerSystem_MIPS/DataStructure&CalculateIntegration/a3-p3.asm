# Name: Cheng Li
# Student ID: 260706615

# Problem 3
# Numerical Integration with the Floating Point Coprocessor
###########################################################
.data
N: .word 100
a: .float 0
b: .float 1
error: .asciiz "error: must have low < hi\n"
input: .asciiz "Please input the address of the function to be integrated\n"
endpoint1: .asciiz "Please input the left bound of the interval\n"
endpoint2: .asciiz "Please input the right bound of the interval\n"
result: .asciiz "The approximate integration result of the input function is:\n"
newLine: .asciiz "\n"
zeroF: .float 0.0
oneF:.float 1.0
twoF: .float 2.0

.text 
###########################################################
main:
	# set argument registers appropriately

	lwc1 $f5, a        # store the boundary to $f5 and $f6
	lwc1 $f6, b
	
	mov.s $f12, $f5      
        mov.s $f13, $f6
	# call integrate on test function 
	jal integrate     # integration
	# print result and exit with status 0
	
	li $v0, 4          # print the result
	la $a0, result
	syscall
	
	li $v0, 2         # print the result
	mov.s $f12, $f0
	syscall
	
	li $v0, 17        # exit with status 0
	li $a0, 0
	syscall

###########################################################
# float integrate(float (*func)(float x), float low, float hi)
# integrates func over [low, hi]
# $f12 gets low, $f13 gets hi, $a0 gets address (label) of func
# $f0 gets return value
integrate: 
	addi $sp, $sp, -4        # save $ra
	sw $ra, 0($sp)
	
	mov.s $f12, $f5          # $f12 gets low, $f13 gets hi
        mov.s $f13, $f6
        
        la $a0, ident            # $a0 gets address (label) of func
        
        jal check                # check if it is a valid interval
        
	# initialize $f4 to hold N
	lw $t0, N		# store N to $t0
	mtc1 $t0, $f4		# store $t0 to $f4
	cvt.s.w $f4, $f4	# convert from word to float
	# since N is declared as a word, will need to convert 
	
	sub.s $f1, $f13, $f12	# $f1 = b - a
	div.s $f1, $f1, $f4	# $f1 = ��x =(b - a) / N = $f1 / $f4
	
	l.s $f2, oneF           # set a counter to $f2
	l.s $f3, oneF           # counter plus $f3 to increase
	l.s $f7, zeroF          # $f7 to save the current value of area
	
calculate:
        mov.s $f12, $f5        # reset $f12 to the left bound
        mul.s $f8, $f1, $f2    # $f8 = i * ��x
        add.s $f12, $f12, $f8  # $f12 = left bound + i*��x
        l.s $f10, twoF         # initial the divisor
        div.s $f10, $f1, $f10  # get to the midpoint
        sub.s $f12, $f12, $f10
        
        jalr $a0               # get the value of the midpoint
        mov.s $f9, $f0         # store the result into $f9
        mul.s $f9, $f9, $f1    # $f9 = $f9 * ��x
        add.s $f7, $f7, $f9    # add the new box up
        add.s $f2, $f2, $f3    # counter++
        
        c.le.s $f2, $f4        # when counter <= N
        bc1t calculate         # calculate next box
        
        mov.s $f0, $f7         # store the final result into $f0
	lw $ra, 0($sp)         # restore $ra
	addi $sp, $sp, 4
	
	jr $ra

###########################################################
# void check(float low, float hi)
# checks that low < hi
# $f12 gets low, $f13 gets hi
# # prints error message and exits with status 1 on fail
check:
        mov.s $f12, $f5        #check if it is a valid interval (i.e left bound is smaller than right bound)
        mov.s $f13, $f6
        c.lt.s $f13, $f12
        bc1t errorInput
        
	jr $ra

errorInput:
        li $v0, 4
        la $a0, error
        syscall
        
        li $v0, 17
        li $a0, 1
        syscall
        

###########################################################
# float ident(float x) { return x; }
# function to test your integrator
# $f12 gets x, $f0 gets return value
ident:
	mov.s $f0, $f12
	jr $ra
