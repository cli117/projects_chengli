#StudentID:260706615
#Name:Cheng Li

.data
     prompt: .asciiz "Please type in characters ending with '*'"
  oriPrompt: .asciiz "Original linked list\n"
  revPrompt: .asciiz "reversed linked list\n"
    newLine: .asciiz "\n"
       none: .asciiz "Please type in characters!"
.text
#There are no real limit as to what you can use
#to implement the 3 procedures EXCEPT that they
#must take the specified inputs and return at the
#specified outputs as seen on the assignment PDF.
#If convention is not followed, you will be
#deducted marks.

main:
#build a linked list

    li $v0, 4             # print the prompt
    la $a0, prompt
    syscall
    
    
    jal build             # build the linkedlist
    
#print "Original linked list\n"
    li $v0, 4
    la $a0, newLine
    syscall
    
    li $v0, 4
    la $a0, oriPrompt
    syscall
#print the original linked list
    move $a0, $v1
    jal print
    
    
#reverse the linked list
    move $a1, $v1
    jal reverse
#On a new line, print "reversed linked list\n"
    li $v0, 4
    la $a0, newLine
    syscall
    
    li $v0, 4
    la $a0, revPrompt
    syscall
    
#print the reversed linked list
    move $a0, $v1
    jal print
#terminate program
    li $v0, 10
    syscall

build:
#continually ask for user input UNTIL user inputs "*"
    addi $sp, $sp, -4     # save $ra
    sw $ra, 0($sp)
    jal alloSpace         # make space for the first element
    add $v1, $v0, $0
    add $t0, $v0, $0
    
    li $v0, 12           # get the first value from user and branch if it is *
    syscall
    
    beq $v0, 42, end    # if so, end the program and print a message
    sw $v0, 0($t0)      # if not, save the char
    sw $0, 4($t0)       # initialize to next node

nextInput:
    
    jal alloSpace       # make space for next node
    add $t1, $v0, $0    #save the address
    
    li $v0, 12
    syscall
    add $t2, $v0, $0    # read from user and save it into $t2
    
    beq $t2, 42, goBack # if input is '*', stop reading and go back to main
    sw $t1, 4($t0)      # point to next node
    sw $t2, 0($t1)      # save char
    
    move $t0, $t1
    j nextInput
    
end:
    li $v0, 4
    la $a0, newLine
    syscall
    
    li $v0, 4             # print a message and exit
    la $a0, none
    syscall
 
    li $v0, 10
    syscall
    
    
#FOR EACH user inputted character inG, create a new node that hold's inG AND an address for the next node
#at the end of build, return the address of the first node to $v1

print:
#$a0 takes the address of the first node
#prints the contents of each node in order

   addi $sp, $sp, -4	#save $ra
   sw   $ra, 0($sp)
        
   move $t0, $a0        # save the original pointer
       
   lw $a0,0($t0)        # print the first element
   li $v0,11
   syscall
       
   lw $t1,4($t0)    
       
printNext:
    beqz $t1,goBack    # if there is no more element, stop printing
     
    lw $a0,0($t1)      # print next element
    li $v0,11
    syscall
       
    lw $t1,4($t1)     # point to next element
    j printNext
   
reverse:
#$a1 takes the address of the first node of a linked list
#reverses all the pointers in the linked list
#$v1 returns the address

    lw $t1,4($a1)                    # the last node of reversed list into t1 
    sw $zero,4($a1)                  # clear the last node
    move $t0, $a1  
    
reverseNext:
    beqz $t1,doneRev                 # if the last node is empty, the reversation is done
    lw $t2,4($t1)                    # the second last node into t2 
    sw $t0,4($t1)    
    move $t0, $t1
    move $t1, $t2
    j reverseNext
    
doneRev: 
    move $v1,$t0
    jr $ra     

alloSpace:
    li $v0, 9                        # make space for a node
    li $a0, 5
    syscall
    jr $ra
   
goBack:
    lw $ra, 0($sp)	             # go back to the address called
    addi $sp, $sp, 4
    jr $ra
