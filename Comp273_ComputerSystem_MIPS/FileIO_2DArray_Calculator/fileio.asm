# fileio.asm
	
.data

#Must use accurate file path.
#These file paths are EXAMPLES, 
#should not work for you
str1:	.asciiz "test1.txt"
str2:	.asciiz "/Users/McGill/Courses/COMP-273/assignments/2017/Assign4/test2.txt"
str3:	.asciiz "test.pgm"	#used as output
notFound: .asciiz "File is not found, please check the filename!"
notRead: .asciiz "Error occurs when reading the file!"
emptyFile: .asciiz "Can not find any content in the text file!"
notClose: .asciiz "Error occurs when closing the file!(reading)"
notCreate: .asciiz "Error occurs when creating the test.pgm file!"
notWrite: .asciiz "Error occurs when writing the test.pgm file!"
notCloseW: .asciiz "Error occurs when closing the file!(writing)"
readPrompt: .asciiz "The content in the input file is: \n"
pgmTitle: .asciiz "P2\n24 7\n15\n"

buffer:  .space 2048		# buffer for upto 2048 bytes

	.text
	.globl main

main:	la $a0,str1		#readfile takes $a0 as input
	jal readfile

	la $a0, str3		#writefile will take $a0 as file location
	la $a1,buffer		#$a1 takes location of what we wish to write.
	jal writefile

	li $v0,10		# exit
	syscall

readfile:

#Open the file to be read,using $a0
#Conduct error check, to see if file exists
	li $v0, 13
	li $a1, 0
	li $a2, 0
	syscall
	bltz $v0, fileNotFound
	move $s0, $v0
	
# You will want to keep track of the file descriptor*

# read from file
	li $v0, 14
# use correct file descriptor, and point to buffer
	move $a0, $s0
	la $a1 buffer
# hardcode maximum number of chars to read
	li $a2 2048
# read from file
	syscall
	
	beqz $v0, empty
	bltz $v0, readError
	
# address of the ascii string you just read is returned in $v1.
# the text of the string is in buffer
	la $a0, readPrompt
	li $v0, 4
	syscall
	
	move $a0, $a1
	li $v0, 4
	syscall
# close the file (make sure to check for errors)
	li $v0, 16
	move $a0, $s0
	syscall
	bltz $v0, closeError
	
	jr $ra

writefile:
#open file to be written to, using $a0.
	move $t0, $a1
	li $v0, 13
	li $a1, 1
	li $a2, 0
	syscall
	bltz $v0, createError
	move $s1, $v0
#write the specified characters as seen on assignment PDF:
#P2
#24 7
#15
	la $t1, pgmTitle

	li $v0, 15
	move $a0, $s1
	move $a1, $t1
	li $a2, 2048
	syscall
	bltz $v0, writeError
#write the content stored at the address in $a1.	
	li $v0, 15
	move $a0, $s1
	move $a1, $t0
	li $a2, 2048
	syscall
	bltz $v0, writeError
	
#close the file (make sure to check for errors)
	li $v0, 16
	move $a0, $s1
	syscall
	bltz $v0, closeErrorWrite
	
	jr $ra
#if cannot find the file to be read, print a message and exit
fileNotFound:
	li $v0, 4           
	la $a0, notFound
	syscall
	
	li $v0, 10
	syscall

#if error occurs when reading the input file, print a message and exit
readError:
	li $v0, 4
	la $a0, notRead
	syscall
	
	li $v0, 10
	syscall

#if the file read is empty, print a message and exit
empty:
	li $v0, 4
	la $a0, emptyFile
	syscall
	
	li $v0, 10
	syscall

#if error occurs when closing the reading file, print a message and exit
closeError:
	li $v0, 4
	la $a0, notClose
	syscall
	
	li $v0, 10
	syscall

#if error occurs when creating test.pgm, print a message and exit
createError:
	li $v0, 4
	la $a0, notCreate
	syscall
	
	li $v0, 10
	syscall

#if error occurs when writing test.pgm, print a message and exit
writeError:
	li $v0, 4
	la $a0, notWrite
	syscall
	
	li $v0, 10
	syscall

#if error occurs when closing the writing file, print a message and exit
closeErrorWrite:
	li $v0, 4
	la $a0, notCloseW
	syscall
	
	li $v0, 10
	syscall
