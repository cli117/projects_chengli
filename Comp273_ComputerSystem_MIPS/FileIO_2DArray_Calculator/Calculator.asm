#Calculator
#You will recieve marks based 
#on functionality of your calculator.
#REMEMBER: invalid inputs are ignored.
.data
	promptInformation: 	.asciiz "Calculator for MIPS\npress 'c' for clear and 'q' to quit:\n"
	resultPrompt: 		.asciiz "\nResult :"
	expression: 		.space 200
	operator: 		.byte 1
	finalResult: 		.space 100
	reversedResult:		.space 100
	zero: 			.float 0.0
	aHundred: 		.float 100.0

#Have fun!
.text
main:
	la $t2, promptInformation
writePrompt:
	lb $a0, 0($t2)			#get the char
	jal write			#write the char to screen
	addi $t2, $t2, 1		#point to next char
	bne $a0, $0, writePrompt	#if it is not the end, write next
nextCalculation:
	addi $t4, $0, 0			#$t4 as the array pointer
	li $t5, 0
	j clearTheExpression
readLoop:

	addi $t5, $0, 0			#$t5 as the operator checker
	jal read			#read the input
	addi $t3, $v0, 0		#store the current input in $t0
	beq $t3, 'q', exit		#if press q, exit the calculator
	beq $t3, 'c', clear		#if press c, clear the calculator
	beq $t3, '+', saveExpression	#save all expression contents
	beq $t3, '-', saveExpression
	beq $t3, '*', saveExpression
	beq $t3, '/', saveExpression
	beq $t3, ' ', saveExpression
	beq $t3, '\n', doneStoring
	blt $t3, '0', readLoop
	bgt $t3, '9', readLoop
	
saveExpression:
	sb $t3, expression($t4)
	addi $t4, $t4, 1
	addi $a0, $t3, 0
	jal write
	j readLoop

	
doneStoring:
	jal getNum
	mtc1 $s0, $f4
	cvt.s.w $f4, $f4
	mtc1 $s1, $f5
	cvt.s.w $f5, $f5
	
	jal getResult			#$f6 get the result
	
	la $t2, resultPrompt
resultPromptWrite:
	lb $a0, 0($t2)			#get the char
	jal write			#write the char to screen
	addi $t2, $t2, 1		#point to next char
	bne $a0, $0, resultPromptWrite	#if it is not the end, write next
	
	jal printResult
	j nextCalculation
	
getNum:
	addi $t4, $0, 0			#pointer initialization
	lb $t3, expression($t4)		#the first element
	blt $t3, '0', resultNum		#calculation using previous result and input number

numNum:
	li $t5, 0			#$t5 as current number
getFirst:
	lb $t3, expression($t4)		#current element
	addi $t4, $t4, 1
	beq $t3, '-', negativeFirst
	beq $t3, ' ', getSecond		#once space is read, store the first num and ready to read next one
	addi $t3, $t3, -48
	mul $t5, $t5, 10
	add $t5, $t5, $t3
	j getFirst
negativeFirst:
	lb $t3, expression($t4)
	addi $t4, $t4, 1
	beq $t3, ' ', getSecondNegative
	addi $t3, $t3, -48
	mul $t5, $t5, 10
	add $t5, $t5, $t3
	j negativeFirst
getSecondNegative:
	li $t6, -1
	mul $t5, $t5, $t6
	addi $s0, $t5, 0
	lb $t3, expression($t4)
	addi $t4, $t4, 2
	sb $t3, operator
	lb $t3, expression($t4)		#$t3 as the first element of second number
	li $t5, 0
	bge $t3, '0', saveSecond
	addi $t4, $t4, 1
	li $t6, 1			#if it is negative number, set $t6 to 1 to mark
	j saveSecond
getSecond:
	addi $t6, $0, 0
	addi $s0, $t5, 0		#save the first number to $s0
	lb $t3, expression($t4)
	addi $t4, $t4, 2
	sb $t3, operator
	lb $t3, expression($t4)		#$t3 as the first element of second number
	li $t5, 0
	bge $t3, '0', saveSecond
	addi $t4, $t4, 1
	li $t6, 1			#if it is negative number, set $t6 to 1 to mark

saveSecond:
	lb $t3, expression($t4)
	addi $t4, $t4, 1
	beq $t3, 0, numNumDone
	addi $t3, $t3, -48		#convert to int
	mul $t5, $t5, 10
	add $t5, $t5, $t3
	j saveSecond
	
numNumDone:
	beq $t6, 1, negativeDone
	addi $s1, $t5, 0
	jr $ra
negativeDone:
	li $t6, -1
	mul $t5, $t5, $t6
	addi $s1, $t5, 0
	jr $ra
	
resultNum:
	addi $t4, $t4, 1
	lb $t5, expression($t4)
	subi $t4, $t4, 1
	bgt $t5, '0', numNum
	li $s2, 1
	sb $t3, operator
	addi $t4, $t4, 2
	lb $t3, expression($t4)
	li $t6, 0
	bgt $t3, 47, saveResultNum
	addi $t7, $0, 1
	addi $t4, $t4, 1
saveResultNum:
	lb $t3, expression($t4)
	beq $t3, 0, resultNumDone
	subi $t3, $t3, 48
	mul $t6, $t6, 10
	add $t6, $t6, $t3
	addi $t4, $t4, 1
	j saveResultNum
resultNumDone:
	beq $t7, 1, resultNumDoneNegative
	addi $s1, $t6, 0
	jr $ra
resultNumDoneNegative:
	sub $t7, $0, $t7
	addi $s1, $t6, 0
	jr $ra
	
getResult:
	lb $t3, operator		#$t3 be the operator
	bne $s2, 1, addition
	mov.s $f4, $f6
addition:
	bne $t3, '+', subt
	add.s $f6, $f4, $f5
	jr $ra
subt:
	bne $t3, '-', multi
	sub.s $f6, $f4, $f5
	jr $ra
multi:
	bne $t3, '*', divi
	mul.s $f6, $f4, $f5
	jr $ra
divi:
	div.s $f6, $f4, $f5
	jr $ra
	
read:
	lui $t0, 0xffff
waitRead:	
	lw $t1, 0($t0)
	andi $t1, $t1, 0x0001		#control
	beq $t1, $zero, waitRead
	lw $v0, 4($t0)			#data
	jr $ra
	
#driver for putting output to MIPS display
write:
	lui $t0, 0xffff
waitWrite:	
	lw $t1, 8($t0)			
	andi $t1, $t1, 0x0001		#control
	beq $t1, $zero, waitWrite
	sw $a0, 12($t0)			#data
	jr $ra

clear:
	li $a0, 12
	jal write
	addi $t4, $0, 0
	li $t5, 0
clearTheExpression:			#clear the old expression 
	sb $t5, expression($t4)
	addi $t4, $t4, 1
	lb $t3, expression($t4)
	bne $t3, 0, clearTheExpression
	addi $t4, $0, 0
clearFinalResult:			#clear the old final result
	sb $t5, finalResult($t4)
	addi $t4, $t4, 1
	lb $t3, finalResult($t4)
	bne $t3, 0, clearFinalResult
	addi $t4, $0, 0
clearReversedResult:			#clear the old reversed result
	sb $t5, reversedResult($t4)
	addi $t4, $t4, 1
	lb $t3, reversedResult($t4)
	bne $t3, 0, clearReversedResult
	addi $t4, $0, 0
clearCondition:
	li $s2, 0
	j readLoop
printResult:
	addi $sp, $sp, -4
	sw $ra, 0($sp)
	l.s $f8, zero
  	l.s $f9, aHundred
  	c.eq.s $f6, $f8
  	bc1t print0
  	mul.s $f9, $f9, $f6
  	cvt.w.s $f9, $f9
	mfc1 $t5, $f9
	la $t8, reversedResult
	la $t9, finalResult
	li $t6, 0			#set $t6 as a conuter
	blt $t5, 10, checkNegativeLess0.1
	bge $t5, 10, printPositive
checkNegativeLess0.1:
	bgt $t5, 0, positiveLess0.1
negativeResult:
	sub $t5, $0, $t5
	blt $t5, 10, negativeLess0.1
printNegativeResult:
	div $t5, $t5, 10
	mfhi $t7			#get the remainder
	addi $t7, $t7, '0'		#convert to ascii
	sb $t7, 0($t8)
	addi $t6, $t6, 1
	addi $t8, $t8, 1
	bge $t5, 10, printNegativeResult
	addi $t5, $t5, '0'
	sb $t5, 0($t8)
	li $t5, '-'
	sb $t5, 0($t9)
	addi $t9, $t9, 1
	j reverseLoop
	
printPositive:
	div $t5, $t5, 10
	mfhi $t7			#get the remainder
	addi $t7, $t7, '0'		#convert to ascii
	sb $t7, 0($t8)
	addi $t6, $t6, 1
	addi $t8, $t8, 1
	bge $t5, 10, printPositive
	addi $t5, $t5, '0'
	sb $t5, 0($t8)
reverseLoop:				#reverse the reversed expression
	beq $t6, 1, addDecimal
	lb $t5, 0($t8)
	subi $t6, $t6, 1
	subi $t8, $t8, 1
	sb $t5, 0($t9)
	addi $t9, $t9, 1
	j reverseLoop
addDecimal:				#put a decimal in the right place
	li $t5, '.'
	sb $t5, 0($t9)
	addi $t9, $t9, 1
	lb $t5, 0($t8)
	subi $t8, $t8, 1
	sb $t5, 0($t9)
	addi $t9, $t9, 1
	lb $t5, 0($t8)
	sb $t5, 0($t9)
	la $t9, finalResult
printFinalResult:
	lb $a0, 0($t9)
	jal write
	addi $t9, $t9, 1
	lb $a0, 0($t9)
	bne $a0, 0, printFinalResult
	li $a0, '\n'
	jal write
	
	lw $ra, 0($sp)
	addi $sp, $sp, 4
	jr $ra
print0:					#when the result is 0
	la $t3, finalResult
	li $t5, '0'
	sb $t5, 0($t3)
	addi $t3, $t3, 1
	li $t5, '.'
	sb $t5, 0($t3)
	addi $t3, $t3, 1
	li $t5, '0'
	sb $t5, 0($t3)
	addi $t3, $t3, 1
	sb $t5, 0($t3)
	addi $t3, $t3, 1
	li $t5, '\n'
	sb $t5, 0($t3)
	la $t6, finalResult
print0loop:
	lb $a0, 0($t6)
	jal write
	addi $t6, $t6, 1
	lb $a0, 0($t6)
	bne $a0, 0, print0loop
	
	lw $ra, 0($sp)
	addi $sp, $sp, 4
	jr $ra
	
positiveLess0.1:			#when the result is positive and smaller than 0.1
	li $t3, '0'
	la $t9, finalResult
	sb $t3, 0($t9)
	addi $t9, $t9, 1
	li $t3, '.'
	sb $t3, 0($t9)
	addi $t9, $t9, 1
	li $t3, '0'
	sb $t3, 0($t9)
	addi $t9, $t9, 1
	addi $t5, $t5, '0'
	sb $t5, 0($t9)
	la $t9, finalResult
	j printFinalResult
negativeLess0.1:			#when the result is negative and greater than -0.1
	li $t3, '-'
	la $t9, finalResult
	sb $t3, 0($t9)
	addi $t9, $t9, 1
	li $t3, '0'
	sb $t3, 0($t9)
	addi $t9, $t9, 1
	li $t3, '.'
	sb $t3, 0($t9)
	addi $t9, $t9, 1
	li $t3, '0'
	sb $t3, 0($t9)
	addi $t9, $t9, 1
	addi $t5, $t5, '0'
	sb $t5, 0($t9)
	la $t9, finalResult
	j printFinalResult
exit:
	nop
