
.data

#Must use accurate file path.
#These file paths are EXAMPLES, 
#should not work for you
str1:	.asciiz "test1.txt"
str3:	.asciiz "test-blur.pgm"	#used as output
notFound: .asciiz "File is not found, please check the filename!"
notRead: .asciiz "Error occurs when reading the file!"
emptyFile: .asciiz "Can not find any content in the text file!"
notANumber: .asciiz "There exists a char which is neither a number nor a space!"
space: .asciiz " "
pgmTitle: .asciiz "P2\n24 7\n15\n"
notCreate: .asciiz "Error occurs when creating the test.pgm file!"
notWrite: .asciiz "Error occurs when writing the test.pgm file!"
notCloseW: .asciiz "Error occurs when closing the file!(writing)"

myArray: .space 2048
buffer:  .space 2048		# buffer for upto 2048 bytes
newbuff: .space 2048

	.text
	.globl main

main:	la $a0,str1		#readfile takes $a0 as input
	jal readfile


	la $a1,buffer		#$a1 will specify the "2D array" we will be averaging
	la $a2,newbuff		#$a2 will specify the blurred 2D array.
	jal blur


	la $a0, str3		#writefile will take $a0 as file location
	la $a1,newbuff		#$a1 takes location of what we wish to write.
	jal writefile
	
	li $v0,10		# exit
	syscall

readfile:
#done in Q1
	addi $sp, $sp, -4
	sw $ra, 0($sp)
	
	li $v0, 13
	li $a1, 0
	li $a2, 0
	syscall
	bltz $v0, fileNotFound
	move $s0, $v0
# read from file
	li $v0, 14
# use correct file descriptor, and point to buffer
	move $a0, $s0
	la $a1, buffer
# hardcode maximum number of chars to read
	li $a2 2048
# read from file
	syscall
	
	beqz $v0, empty
	bltz $v0, readError
	
	addu $a1, $0, 0
	addi $t6, $0, 0
	addi $t7, $0, 0
	la $t0, buffer
	lbu $t1, 0($t0)
nextCondition:
	blt $t1,48, NAN
	bgt $t1, 57, NAN 
	addi $t0, $t0, 1
	lbu $t2, 0($t0)
	beqz $t2, store
	beq $t2, 32, store
	beq $t2, '\n', store
	slti $t3, $t2, 48
	slti $t4, $t2, 58
	add $t3, $3, $t4
	bne $t3, 1, NAN
moreDigits:
	mulu $a1, $a1, 10
	addi $t5, $t1, -48
	addu $a1, $a1, $t5
	move $t1, $t2
	j nextCondition
store:
	mul $a1, $a1, 10
	addi $t5, $t1, -48
	addu $a1, $a1, $t5
	sb $a1, myArray($t6)
	li $a1, 0
	addi $t6, $t6, 4
	addi $t0, $t0, 1
	lbu $t1, 0($t0)
	addi $t7, $t7, 1
	beq $t7, 168, restoreBack
	jal skipSpaceCheck
	j nextCondition
blur:
#use real values for averaging.
#HINT set of 8 "edge" cases.
#The rest of the averaged pixels will 
#default to the 3x3 averaging method
#we will return the address of our
#blurred 2D array in #v1
	addi $sp, $sp, -4
	sw $ra, 0($sp)
	
	addi $t1, $0, 0      #$t1 as the current number of element in the array to check if it is a edge
	move $t2, $a2        #$t2 as the pointer of new buffer
	addi $t3, $0, 10     #$t3 is 10, to deal with 2 or 3 digits numbers
	addi $t4, $0, ' '    #$t4 is space
	addi $t5, $0, '\n'   #$t5 is CR to make a new line
	addi $t6, $0, 0      #$t6 as the array index
	addi $t7, $0, 24     #$t7 is 24 to check if it is a edge element
edgeCheck:
	beq $t1, 168, restoreBack     #blur is done, go back to main
	blt $t1, 23, edge             #the first line is edge
	bgt $t1, 143, edge	      #the last line is edge
	div $t1, $t7
	mfhi $t8		      #get the remainder
	beq $t8, 23, lastEle	      #if the remainder is 23, then it is the last element of a line
	beq $t8, 0, edge	      #if the remainder is 0, then it is the first element of a line, and it is a edge
	j averageCalculate            #if it is not a edge element, then blur it

#if it is a edge element, directly store this element into new buffer as a asciiz
edge:
	lbu $t0, myArray($t6)	      #take out the current element
	jal store1Digit		      #store it
	sb $t4, 0($t2)		      #store a space after that element
	addi $t6, $t6, 4	      #move to next element
	addi $t1, $t1, 1	      #increasment of index
	addi $t2, $t2, 1	      #point to next space in the new buffer
	j edgeCheck
	
lastEle:
	lbu $t0, myArray($t6)	      #take out the current element
	jal store1Digit		      #store it
	sb $t5, 0($t2)		      #make a new line
	addi $t6, $t6, 4	      #move to next element
	addi $t1, $t1, 1	      #increasment of index
	addi $t2, $t2, 1	      #point to next space in the new buffer
	j edgeCheck
	
averageCalculate:
	lbu $t0, myArray($t6)	      #the central element
	
	addi $t8, $t6, -100	      #the first element in the 3*3 block
	lbu $t9, myArray($t8)
	add $t0, $t0, $t9
	
	addi $t8, $t6, -96	      #the second
	lbu $t9, myArray($t8)
	add $t0, $t0, $t9
	
	addi $t8, $t6, -92	      #the third
	lbu $t9, myArray($t8)
	add $t0, $t0, $t9
	
	addi $t8, $t6, -4	      #the fourth
	lbu $t9, myArray($t8)
	add $t0, $t0, $t9
	
	addi $t8, $t6, 4	    	      #the sixth
	lbu $t9, myArray($t8)
	add $t0, $t0, $t9
	
	addi $t8, $t6, 92	      #the seventh
	lbu $t9, myArray($t8)
	add $t0, $t0, $t9
	
	addi $t8, $t6, 96	      #the eighth
	lbu $t9, myArray($t8)
	add $t0, $t0, $t9
	
	addi $t8, $t6, 100	      #the ninth
	lbu $t9, myArray($t8)
	add $t0, $t0, $t9
	
	div $t0, $t0, 9
	jal store1Digit
	
	addi $t1, $t1, 1
	sb $t4, 0($t2)
	addi $t2, $t2, 1
	addi $t6, $t6, 4
	j edgeCheck
	
store1Digit:
	bgt $t0, 9, store2Digit       #if the number is greater than 9, then jump to store2Digit
	addi $t0, $t0, 48	      #convert to ascii
	sb $t0, 0($t2)		      #store
	addi $t2, $t2, 1	      #point to next space in the new buffer
	jr $ra
store2Digit:
	div $t0, $t3		      #check if it is a 3-digit number
	mflo $t8
	mfhi $t9
	bgt $t8, 9, store3Digit	      #if it is, then jump to store3Digit
	addi $t8, $t8, 48	      #conversion
	addi $t9, $t9, 48
	sb $t9, 1($t2)
	sb $t8, 0($t2)
	addi $t2, $t2, 2
	jr $ra
store3Digit:
	addi $t9, $t9, 48	      #store the least significant bit first
	sb $t9, 2($t2)
	div $t8, $t3		      #get the second significant and most significant bit
	mflo $t8
	mfhi $t9
	addi $t8, $t8, 48	      #conversion
	addi $t9, $t9, 48
	sb $t9, 1($t2)
	sb $t8, 0($t2)
	addi $t2, $t2, 3
	jr $ra
	
writefile:
#done in Q1
#open file to be written to, using $a0.
	move $t0, $a1
	li $v0, 13
	li $a1, 1
	li $a2, 0
	syscall
	bltz $v0, createError
	move $s1, $v0
#write the specified characters as seen on assignment PDF:
#P2
#24 7
#15
	la $t1, pgmTitle

	li $v0, 15
	move $a0, $s1
	move $a1, $t1
	li $a2, 2048
	syscall
	bltz $v0, writeError
#write the content stored at the address in $a1.	
	li $v0, 15
	move $a0, $s1
	move $a1, $t0
	li $a2, 2048
	syscall
	bltz $v0, writeError
	
#close the file (make sure to check for errors)
	li $v0, 16
	move $a0, $s1
	syscall
	bltz $v0, closeErrorWrite
	
	jr $ra
#if cannot find the file to be read, print a message and exit
	jr $ra
#if cannot find the file to be read, print a message and exit
fileNotFound:
	li $v0, 4           
	la $a0, notFound
	syscall
	
	li $v0, 10
	syscall

#if error occurs when reading the input file, print a message and exit
readError:
	li $v0, 4
	la $a0, notRead
	syscall
	
	li $v0, 10
	syscall

#if the file read is empty, print a message and exit
empty:
	li $v0, 4
	la $a0, emptyFile
	syscall
	
	li $v0, 10
	syscall
	
NAN: 
	li $v0, 4
	la $a0, notANumber
	syscall
	
	li $v0, 10
	syscall
	

restoreBack:
	lw $ra, 0($sp)         # restore $ra
	addi $sp, $sp, 4
	jr $ra

back:
	jr $ra

skipSpaceCheck:
	bne $t1, 32, back
	addi $t0, $t0, 1
	lbu $t1, 0($t0)
	j skipSpaceCheck

#if error occurs when creating test.pgm, print a message and exit
createError:
	li $v0, 4
	la $a0, notCreate
	syscall
	
	li $v0, 10
	syscall

#if error occurs when writing test.pgm, print a message and exit
writeError:
	li $v0, 4
	la $a0, notWrite
	syscall
	
	li $v0, 10
	syscall

#if error occurs when closing the writing file, print a message and exit
closeErrorWrite:
	li $v0, 4
	la $a0, notCloseW
	syscall
	
	li $v0, 10
	syscall