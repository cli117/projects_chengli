//File Name:    PinMain.java
//Developers:   Cheng Li
//Purpose:      This file contains the user interface code.
//Inputs:       Old pin, New pin
//Outputs:      Update the new pin
//First version, no modifications
//======================================================================
//2018-1-17, user is asked for the old pin firstly. If it is correct, the user can change the pin to a new valid one.

import java.util.Scanner;
public class PinMain {
    public static void main(String[] args) {
        Pin currentPin = new Pin();
        String inputPin, newPin;
        Scanner getInput = new Scanner(System.in);

        System.out.print("Welcome to pin update!\n");

        while (true) {
            System.out.print("Please input your old pin: ");
            inputPin = getInput.nextLine();
            if (currentPin.isMatch(inputPin)) break;
        }

        while (true) {
            System.out.print("Please input your new pin: ");
            newPin = getInput.nextLine();
            if (currentPin.setNewPin(newPin)) break;
        }

    }
}
