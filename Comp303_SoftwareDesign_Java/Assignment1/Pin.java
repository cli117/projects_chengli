//File Name:    Pin.java
//Developers:   Cheng Li
//Purpose:      This file contains all the pin code.
//Inputs:       None
//Outputs:      None
//First version, no modifications
//======================================================================
//2018-1-17

public class Pin {
    private char firstBit;
    private char secondBit;

    //Name:         Pin
    //Developers:   Cheng Li
    //Purpose:      Construct a new Pin
    //Inputs:       None
    //Outputs:      A new Pin with default 00
    //Side-effect:  None
    //Special notes:None
    public Pin(){
        firstBit = '0';
        secondBit = '0';
    }

    //Name:         getCurrentPin
    //Developers:   Cheng Li
    //Purpose:      Access private attributes of class Pin
    //Inputs:       None
    //Outputs:      Current Pin as a String
    //Side-effect:  None
    //Special notes:Private attributes are two chars, while this getter returns them as a String
    public String getCurrentPin(){
        return "" + firstBit + secondBit;
    }
    //Name:         setNewPin
    //Developers:   Cheng Li
    //Purpose:      update the new pin if it is valid
    //Inputs:       new Pin
    //Outputs:      new Pin updated
    //Side-effect:  None
    //Special notes:New Pin can be invalid, the setter will call isValid to check validity.
    public boolean setNewPin(String newPin){
        if (!isValid(newPin)) {
            System.out.print("Invalid pin.\n");
            return false;
        }
        firstBit = newPin.charAt(0);
        secondBit = newPin.charAt(1);
        System.out.print("New pin confirmed.\n");
        System.out.printf("Your pin has been updated to %s\n", newPin);
        return true;
    }
    //Name:         isMatch
    //Developers:   Cheng Li
    //Purpose:      Check if the input Pin can match the old pin
    //Inputs:       Input Pin
    //Outputs:      True if input pin is same as the old pin
    //Side-effect:  None
    //Special notes:None
    public boolean isMatch(String inputPin){
        if (inputPin.length() != 2){
            System.out.print("That is not your old pin.\n");
            return false;
        }
        if (inputPin.charAt(0) == firstBit && inputPin.charAt(1) == secondBit) {
            System.out.print("Old pin confirmed.\n");
            return true;
        }
        System.out.print("That is not your old pin.\n");
        return false;
    }
    //Name:         isValid
    //Developers:   Cheng Li
    //Purpose:      Check if the new Pin is valid
    //Inputs:       New Pin
    //Outputs:      True if new pin is valid
    //Side-effect:  None
    //Special notes:This function is private, it is called by setter to check the validity
    private boolean isValid(String newPin){
        if (newPin.length() != 2) return false;                                                //1. length is not 2
        if (newPin.charAt(0) < '0' || newPin.charAt(0) > '9') return false;                   //2. nonnumeric char
        if (newPin.charAt(1) < '0' || newPin.charAt(1) > '9') return false;
        if (newPin.charAt(0) == firstBit && newPin.charAt(1) == secondBit) return false;    //3. duplication
        return true;
    }
}
