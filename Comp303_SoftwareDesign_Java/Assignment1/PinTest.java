//File Name:    PinTest.java
//Developers:   Cheng Li
//Purpose:      This file contains the testing code.
//Inputs:       None
//Outputs:      None
//First version, no modifications
//======================================================================
//2018-1-17, this file is for testing pin values.

public class PinTest {
    public static void main(String[] args) {
        Pin testPin = new Pin();
        char testFirstBit = '0', testSecondBit = '0';
        String testPinString;
        //isValid is called by setNewPin, so we can test three methods in one shot
        System.out.print("Test case for method getCurrentPin, setNewPin and isValid:\n");
        //From 00 to 99
        for (int i = 0; i < 10; i++){
            for (int j = 0; j < 10; j++){
                testPinString = "" + testFirstBit + testSecondBit;
                System.out.print("Arguments: testPin.setNewPin(testPinString);\n");
                System.out.print("Result: ");
                testPin.setNewPin(testPinString);
                System.out.print("Current Pin is " + testPin.getCurrentPin() + "\n\n");
                testSecondBit++;
            }
            testSecondBit = '0';
            testFirstBit++;
        }

        //When the length is less than 2
        System.out.print("Arguments: testPin.setNewPin(testPinString);\n");
        System.out.print("Result: ");
        testPinString = "1";
        testPin.setNewPin(testPinString);
        System.out.print("Current Pin is " + testPin.getCurrentPin() + "\n\n");

        //When there are nonnumeric chars
        System.out.print("Arguments: testPin.setNewPin(testPinString);\n");
        System.out.print("Result: ");
        testPinString = "AB";
        testPin.setNewPin(testPinString);
        System.out.print("Current Pin is " + testPin.getCurrentPin() + "\n\n");

        //When the length is larger than 2
        System.out.print("Arguments: testPin.setNewPin(testPinString);\n");
        System.out.print("Result: ");
        testPinString = "999";
        testPin.setNewPin(testPinString);
        System.out.print("Current Pin is " + testPin.getCurrentPin() + "\n\n");

        testFirstBit = '0';
        testSecondBit = '0';
        System.out.print("Test case for method isMatch:\n");
        //From 00 to 99
        for (int i = 0; i < 10; i++){
            for (int j = 0; j < 10; j++){
                testPinString = "" + testFirstBit + testSecondBit;
                System.out.print("Arguments: testPin.isMatch(testPinString);\n");
                System.out.print("Result: ");
                testPin.isMatch(testPinString);
                testSecondBit++;
            }
            testSecondBit = '0';
            testFirstBit++;
        }

        //When the length is less than 2
        System.out.print("Arguments: testPin.isMatch(testPinString);\n");
        System.out.print("Result: ");
        testPinString = "1";
        testPin.isMatch(testPinString);

        //When there are nonnumeric chars
        System.out.print("Arguments: testPin.isMatch(testPinString);\n");
        System.out.print("Result: ");
        testPinString = "AB";
        testPin.isMatch(testPinString);

        //When the length is larger than 2
        System.out.print("Arguments: testPin.isMatch(testPinString);\n");
        System.out.print("Result: ");
        testPinString = "999";
        testPin.isMatch(testPinString);
    }
}
