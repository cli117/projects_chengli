public class Immoveable extends Item{

    public Immoveable(String name, char token, int x, int y, World world){
        super(name, token, x, y, world);
    }

    @Override
    public boolean isMoveable(Moveable hit){
        return false;
    }

    @Override
    public void move(Moveable hit) {
        throw new RuntimeException("Immoveable items cannot move!");
    }
}