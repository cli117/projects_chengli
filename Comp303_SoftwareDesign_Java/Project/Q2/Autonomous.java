import java.util.ArrayList;

public class Autonomous extends Moveable{

    public Autonomous(String name, char token, int x, int y, World world){
        super(name, token, x, y, world);
    }

    @Override
    public void step(){
        ArrayList<int[]> availableVector = new ArrayList<int[]>();
        int[] up = {this.getX(), this.getY()+1};
        int[] down = {this.getX(), this.getY()-1};
        int[] left = {this.getX()-1, this.getY()};
        int[] right = {this.getX()+1, this.getY()};

        if (this.getWorld().isAvailableSlot(up[0], up[1],this)) availableVector.add(up);
        if (this.getWorld().isAvailableSlot(down[0], down[1],this)) availableVector.add(down);
        if (this.getWorld().isAvailableSlot(left[0], left[1],this)) availableVector.add(left);
        if (this.getWorld().isAvailableSlot(right[0], right[1],this)) availableVector.add(right);

        if (availableVector.size() == 0) {
            return;
        }
        //choose a direction randomly
        int randomIndex = (int)(Math.random()*(availableVector.size()));
        this.setPosition(availableVector.get(randomIndex)[0], availableVector.get(randomIndex)[1]);
        if(this.getWorld().getItem(this.getX(), this.getY()) != null && this.getWorld().getItem(this.getX(), this.getY()).isMoveable(this)){
            this.getWorld().getItem(this.getX(), this.getY()).move(this);
        }
        else if (this.getWorld().getItem(this.getX(), this.getY()) == null){
            this.getWorld().remove(this);
            this.getWorld().add(this, this.getX(), this.getY());
        }
        else return;
    }
}