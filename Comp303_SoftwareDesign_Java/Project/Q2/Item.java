public abstract class Item {
    private String name;
    private char token;
    private int x, y;
    private World world;

    public Item(){}

    public Item(String name, char token, int x, int y, World world){
        this.name = name;
        this.token = token;
        this.x = x;
        this.y = y;
        this.world = world;
    }

    public abstract boolean isMoveable(Moveable hit);

    public abstract void move(Moveable hit);

    public String getName() {
        return name;
    }

    public char getToken() {
        return token;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public World getWorld() {
        return world;
    }

    public void setPosition(int x, int y){
        this.x = x;
        this.y = y;
    }

    public boolean equals(Item item){
        Item i = item;
        boolean isEquals = (i.getX() == this.getX()) &&(i.getY() == this.getY()) && (i.getName().equals(this.getName())) && (i.getToken() == this.getToken());
        return isEquals;
    }

    public void step(){}
}
