import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Main{
    public static World buildWorld(int immoveable, int moveable, int autonomous){
        int numOfRow = 10, numOfColumn = 10;
        World myWorld = new World(numOfRow, numOfColumn);
        if ((immoveable + moveable + autonomous) > numOfRow*numOfColumn) {
            System.out.print("Too much items!\n");
        }

        for (int i = 0; i < immoveable; i++){
            int x, y;
            char token = (char) ('A' + (i % 26));
            String name = "immoveable" + ("" + token);
            while (true){
                x = (int)(Math.random() * (numOfColumn));
                y = (int)(Math.random() * (numOfRow));
                if (myWorld.getItem(x, y)==null) break;
            }
            Immoveable immoveable1 = new Immoveable(name, token,x,y,myWorld);
            myWorld.add(immoveable1, x, y);
        }

        for (int i = 0; i < moveable; i++){
            int x, y;
            char token = (char) ('a' + (i % 26));
            String name = "moveable" + ("" + token);
            while (true){
                x = (int)(Math.random() * (numOfRow));
                y = (int)(Math.random() * (numOfColumn));
                if (myWorld.getItem(x, y)==null) break;
            }
            Moveable moveable1 = new Moveable(name, token,x,y,myWorld);
            myWorld.add(moveable1, x, y);
        }

        for (int i = 0; i < autonomous; i++){
            int x, y;
            char token = (char) ('!' + (i % 26));
            String name = "autonomous" + ("" + token);
            while (true){
                x = (int)(Math.random() * (numOfRow));
                y = (int)(Math.random() * (numOfColumn));
                if (myWorld.getItem(x, y)==null) break;
            }
            Autonomous autonomous1 = new Autonomous(name, token,x,y,myWorld);
            myWorld.add(autonomous1, x, y);
        }

        return myWorld;
    }

    public static void main(String[] args) {
        World myWorld = buildWorld(5, 3, 3);
        myWorld.display();
        System.out.print("\n");
        while (true) {
            System.out.print("restart\n");
            for (int i = 0; i < 100; i++){
                try {
                    Thread.sleep(30);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                myWorld.step();
                myWorld.display();
            }
            System.out.print("Would you like to run the simulation again?\n");
            Scanner sc = new Scanner(System.in);
            String answer = sc.next();
            System.out.print("a\n");
            if (answer.equalsIgnoreCase("no")) break;
        }
        System.exit(0);
    }
}