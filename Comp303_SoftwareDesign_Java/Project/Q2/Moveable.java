public class Moveable extends Immoveable{

    public Moveable(String name, char token, int x, int y, World world){
        super(name, token, x, y, world);
    }

    @Override
    public boolean isMoveable(Moveable hit){
        int newX = 2*this.getX() - hit.getX();
        int newY = 2*this.getY() - hit.getY();
        if (newX<0||newY<0||newX>=this.getWorld().getNumOfColumn()||newY>=this.getWorld().getNumOfRow()) return false;
        if ((this.getWorld().getItem(newX, newY) != null) && (this.getWorld().getItem(newX, newY) instanceof Immoveable)) return false;
        else return true;
    }
    @Override
    public void move(Moveable hit){
        int newX = 2*this.getX() - hit.getX();
        int newY = 2*this.getY() - hit.getY();
        if (this.getWorld().getItem(newX, newY) == null){
            this.getWorld().remove(this);
            this.getWorld().remove(hit);
            hit.setPosition(this.getX(), this.getY());
            this.getWorld().add(hit, hit.getX(), hit.getY());
            this.setPosition(newX, newY);
            this.getWorld().add(this, this.getX(), this.getY());
        }
        //Then the item in the new position will be hit by current Moveable Item
        else this.getWorld().getItem(newX,newY).move(this);
    }
}