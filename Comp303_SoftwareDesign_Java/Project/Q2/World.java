import javax.swing.*;
import java.awt.*;
import java.util.HashMap;

public class World extends HashMap {
    private int numOfRow, numOfColumn;
    private Item[][] slots;
    public World(int numOfRow, int numOfColumn){
        this.numOfColumn = numOfColumn;
        this.numOfRow = numOfRow;
        this.slots = new Item[numOfRow][numOfColumn];
    }

    public int getNumOfRow() {
        return numOfRow;
    }

    public int getNumOfColumn() {
        return numOfColumn;
    }

    public void add(Item item, int x, int y){ this.slots[x][y] = item; }

    public void remove(Item item){
        int removed = 0;
        for (int i = 0; i < this.numOfRow; i++){
            for (int j = 0; j < this.numOfColumn; j++){
                if (slots[i][j] == null) continue;
                if (item.equals(slots[i][j])) {
                    slots[i][j] = null;
                    removed ++;
                    break;
                }
            }
            if (removed != 0) break;
        }
    }

    public boolean isAvailableSlot(int x, int y, Moveable hit){
        if (x<0||y<0||x>=numOfColumn||y>=numOfRow) return false;
        if (slots[x][y] != null && !slots[x][y].isMoveable(hit)) return false;
        return true;
    }

    public Item getItem(int x, int y){ return slots[x][y]; }

    public void display(){
        char[][] toDisplay = new char[numOfRow][numOfColumn];
        for (int i = 0; i < numOfRow; i++){
            for (int j = 0; j < numOfColumn; j++){
                toDisplay[i][j] = ' ';
            }
        }

        for (int i = 0; i < numOfRow; i++){
            for (int j = 0; j < numOfColumn; j++){
                if (slots[i][j] != null) toDisplay[i][j] = slots[i][j].getToken();
            }
        }

        JFrame myFrame = new JFrame("Minecraft");
        myFrame.setSize(50 * numOfRow, 50 * numOfColumn);
        myFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        myFrame.setLayout(new GridLayout(numOfColumn,numOfRow, 5, 5));
        JPanel jp = new JPanel();
        JLabel[][] labels = new JLabel[numOfRow][numOfColumn];
        for (int i = 0; i < numOfRow; i++){
            for (int j = 0; j < numOfColumn; j++){
                JLabel current  = new JLabel(""+ toDisplay[i][j]);
                current.setHorizontalAlignment(JLabel.CENTER);
                jp.add(current);
            }
        }
        myFrame.add(jp);
        myFrame.setVisible(true);
    }

    public void step(){
        for (int i = 0; i < numOfRow; i++){
            for (int j = 0; j < numOfColumn; j++){
                if (slots[i][j] != null) slots[i][j].step();
            }
        }
    }
}