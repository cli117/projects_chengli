Item is parent of Immoveable, Moveable and Autonomous.
Moveable extends Immoveable but overrides isMoveable() and move() method.
Autonomous extends Moveable but has a method named step() to make random move.
move() will move items recursively if the slot is not empty.

Design pattern used:
1. The decotator pattern, Immoveable is decorated Item, Moveable is decorated Immoveable, Autonomous is decorated Moveable.
2. The iterator pattern, I iterate the whole World by a iterator.
3. The MVC pattern, data stored in the World Object is the model, GUI is the view, user input is the controller.
4. The strategy pattern, the GUI is GridLayout.