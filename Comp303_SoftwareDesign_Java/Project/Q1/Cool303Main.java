/**
 * </h1>This is main method</h1>
 * JavaCool303 is imported
 * A theme need to be given or a exception will be threw
 *
 * @author Cheng Li
 * @version 1.0
 * @since 2018-4-15
 */

import JavaCool303.*;

import javax.swing.*;

public class Cool303Main {
    public static void main(String[] args) {
        Cool303Theme summer = new Cool303Summer();
        Cool303Root root = new Cool303Root(summer);
        Cool303Container container = new Cool303Container();
        container.setTheme(summer);
        Cool303Button b1 = new Cool303Button(1);
        Cool303Button b2 = new Cool303Button(2);
        Cool303Button b3 = new Cool303Button(3);
        Cool303Button b4 = new Cool303Button(4);
        Cool303Button b5 = new Cool303Button(5);
        Cool303Button b6 = new Cool303Button(6);
        Cool303Button b7 = new Cool303Button(7);
        Cool303Button b8 = new Cool303Button(8);
        Cool303Button b9 = new Cool303Button(9);
        Cool303Button b10 = new Cool303Button(10);
        Cool303Button b11 = new Cool303Button(11);
        Cool303Button b12 = new Cool303Button(12);
        Cool303Button b13 = new Cool303Button(13);
        Cool303Button b14 = new Cool303Button(14);
        Cool303Button b15 = new Cool303Button(15);
        Cool303Button b16 = new Cool303Button(16);
        Cool303Button b17 = new Cool303Button(17);
        Cool303Button b18 = new Cool303Button(18);
        Cool303Button b19 = new Cool303Button(19);
        Cool303Button b20 = new Cool303Button(20);


        container.add(b1);
        container.add(b2);
        container.add(b3);
        container.add(b4);
        container.add(b5);
        container.add(b6);
        container.add(b7);
        container.add(b8);
        container.add(b9);
        container.add(b10);
        container.add(b11);
        container.add(b12);
        container.add(b13);
        container.add(b14);
        container.add(b15);
        container.add(b16);
        container.add(b17);
        container.add(b18);
        container.add(b19);
        container.add(b20);
        root.add(container);
        try{
            root.paintTheme();
        }
        catch (NullPointerException e){
            System.out.print("Please set a theme!");
        }

    }
}
