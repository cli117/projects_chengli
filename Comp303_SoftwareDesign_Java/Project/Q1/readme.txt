I decorated JFrame, JPanel and JButton so they can be painted, and all of the wrappers are themeable so they all implement interface Themeable. Cool303Root, Cool303Container and Cool303Button are children of abstract class Cool303Component, so the code and constructure in Cool303Component can be reused.

Design pattern used:
1.The composite pattern, all components are defined as Cool303Components and painted in one shot.
2.The decorator pattern, Cool303Button is decorated JButton, Cool303Container is decorated JPanel, Cool303Root is decorated JFrame.
3.The strategy pattern, layout of the frame is flow layout.