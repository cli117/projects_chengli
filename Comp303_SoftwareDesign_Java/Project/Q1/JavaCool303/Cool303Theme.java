/**
 * The Cool303Theme is a class stores the colors of background and buttons
 *
 * @author Cheng Li
 * @version 1.0
 * @since 2018-4-15
 */
package JavaCool303;

import java.awt.Color;

public class Cool303Theme {
	private Color background;
	private Color buttonColor;

    /**
     * Default constructor of Cool303Theme
     */
	public Cool303Theme() {}

    /**
     * A setter for the color of background
     * @param background The color that the background will changed to
     */
	public void setBackground(Color background) {
		this.background = background;
	}

    /**
     * A setter for the color of background
     * @param buttonColor The color that buttons will changed to
     */
	public void setButtonColor(Color buttonColor) {
		this.buttonColor = buttonColor;
	}

    /**
     * A getter for the current background's color
     * @return The current background's color
     */
	public Color getBackground() {
		return this.background;
	}

    /**
     * A getter for the current buttons' color
     * @return The current buttons' color
     */
	public Color getButtonColor() {
		return this.buttonColor;
	}
}
