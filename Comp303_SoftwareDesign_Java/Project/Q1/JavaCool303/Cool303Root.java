/**
 * The Cool303Root decorates JFrame as a wrapper
 * So it can add Cool303Containers and Cool303Components
 * It is a child of JFrame and implements Themeable
 *
 * @author Cheng Li
 * @version 1.0
 * @since 2018-4-15
 */

package JavaCool303;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

import javax.swing.JFrame;

public class Cool303Root extends JFrame implements Themeable{
	private ArrayList<Cool303Component> components;
	private Cool303Theme theme;

    /**
     * The default constructor
     * A new resizable flowLayout JFrame is created
     */
	public Cool303Root() {
	    this.components = new ArrayList<Cool303Component>();
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent windowEvent) {
				System.exit(0);
			}
		});
        super.setLayout(new FlowLayout());
        super.setResizable(true);
	}

    /**
     * The constructor with theme
     * A new resizable flowLayout JFrame with the theme given is created
     *
     * @param theme The theme that JFrame will change to
     */
	public Cool303Root(Cool303Theme theme){
        this.components = new ArrayList<Cool303Component>();
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent windowEvent) {
                System.exit(0);
            }
        });
        super.setLayout(new FlowLayout());
        this.theme = theme;
        super.setResizable(true);
        super.getContentPane().setBackground(this.theme.getBackground());
    }

    /**
     * Add method that can add a Cool303Component into the Root
     * @param component The Cool303Component need to be added
     */
	public void add(Cool303Container component){
	    this.components.add(component);
	    super.add(component.getComponent());
        component.setTheme(this.theme);
    }

    /**
     * Set the theme of the root
     * @param theme The theme that root will change to
     */
    public void setTheme(Cool303Theme theme){
	    this.theme = theme;
        super.getContentPane().setBackground(this.theme.getBackground());
        for (Cool303Component component: this.components){
            component.setTheme(this.theme);
        }
    }

    /**
     * A getter method
     * @return the current theme
     */
    public Cool303Theme getTheme(){
        return this.theme;
    }

    /**
     * The paint method
     * every components in the root will be painted
     */
    public void paintTheme() {

        for (Cool303Component component: this.components){
            component.paintTheme();
        }
        super.pack();
        super.setVisible(true);
    }
}
