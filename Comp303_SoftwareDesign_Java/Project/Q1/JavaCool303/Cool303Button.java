/**
 *The Cool303Button decorates JButton, as a wrapper
 *It can be painted depending on the theme
 *And it is a child of Cool303Component and implements interface Themeable
 *
 * @author Cheng Li
 * @version 1.0
 * @since 2018-04-15
 */

package JavaCool303;

import javax.swing.JButton;
import java.awt.event.*;

public class Cool303Button extends Cool303Component implements Themeable{
	private int label;
	private JButton button;

    /**
     * Implementation of abstract method
     * Create a JButton and add a ActionListener within the JButton
     * No input needed
     */
	protected void createComponent() {
		this.button = new JButton(Integer.toString(this.label));
		this.button.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println(label);
			}
		});
	}

    /**
     * Constructor of Cool303Button
     * Label of the JButton is the input int
     * @param label Label on the button
     */
	public Cool303Button(int label) {
		this.label = label;
		createComponent();
		super.setComponent(this.button);
	}

    /**
     * Paint the button according to the theme
     */
	@Override
	public void paintTheme() {
		this.button.setBackground(this.getTheme().getButtonColor());
	}
	
}
