/**
 * A interface for all themeable objects
 *
 * @author Cheng Li
 * @version 1.0
 * @since 2018-4-15
 */

package JavaCool303;

public interface Themeable {
    /**
     * Set the theme so it can be colored
     * @param selectedTheme The theme will be changed to
     */
	public void setTheme(Cool303Theme selectedTheme);

    /**
     * Get the current theme
     * @return The current theme
     */
	public Cool303Theme getTheme();

    /**
     * Paint according to the current theme
     */
	public void paintTheme();
	
}
