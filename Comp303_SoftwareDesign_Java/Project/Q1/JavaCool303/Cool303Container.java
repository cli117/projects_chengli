/**
 * The Cool303Container decorates JPanel
 * It is a container for Cool303Components
 * It is a child of Cool303Component and implements interface Themeable
 *
 * @author Cheng Li
 * @version 1.0
 * @since 2018-4-15
 */

package JavaCool303;

import java.util.ArrayList;

import javax.swing.*;

public class Cool303Container extends Cool303Component implements Themeable{
	private JPanel container;
	private ArrayList<Cool303Component> components;

    /**
     * Instantiate the abstract method
     * Create the JPanel that will be wrapped
     */
	protected void createComponent() {
		this.container = new JPanel();
	}

    /**
     * Constructor of Cool303Container
     * A ArrayList of Cool303Components is created
     */
	public Cool303Container() {
		this.components = new ArrayList<Cool303Component>();
		createComponent();
		super.setComponent(this.container);
	}

    /**
     * Add method to add components into this container
     * @param component The component that will be added to the container
     */
	public void add(Cool303Component component) {
		this.components.add(component);
		this.container.add(component.getComponent());
		component.setTheme(this.getTheme());
	}

    /**
     * Instantiate the abstract method
     * Paint the JPanel area according to the theme
     */
	public void paintTheme() {
		this.getComponent().setBackground(this.getTheme().getBackground());
		for (Cool303Component components: this.components) {
			components.paintTheme();
		}
	}
}
