/**
 * Abstract class Cool303Component, it decorates JComponent
 * It is a child of JComponent, and implements interface Themeable
 * JComponent within this wrapper can be got using a getter
 *
 * @author Cheng Li
 * @version 1.0
 * @since 2018-04-15
 */

package JavaCool303;

import javax.swing.JComponent;

public abstract class Cool303Component extends JComponent implements Themeable{

	private JComponent component;
	private Cool303Theme theme;

    /**
     * Default constructor for abstract class
     */

	public Cool303Component() {}

    /**
     * Abstract method that need to be instantiated
     * It is used to create the JComponent within the warpper
     */
	protected abstract void createComponent();

    /**
     * Abstract method that need to be instantiated
     * It is used for painting every component itself
     */
    public abstract void paintTheme();

    /**
     * A setter for the component within Cool303Component
     * @param component JComponent that user wants to put in the wrapper
     */
	public void setComponent(JComponent component) {
		this.component = component;
	}

    /**
     * A setter for the theme
     * @param selectedTheme The theme that will be applied
     */
	public void setTheme(Cool303Theme selectedTheme) {
		this.theme = selectedTheme;
	}

    /**
     * A getter for the component, so the user can take the JComponent out of the wrapper if needed
     * @return The JComponent within the wrapper
     */
	public JComponent getComponent() {
		return this.component;
	}

    /**
     * A getter for the current theme
     * @return The theme of the component
     */
	public Cool303Theme getTheme() {
		return this.theme;
	}

}
