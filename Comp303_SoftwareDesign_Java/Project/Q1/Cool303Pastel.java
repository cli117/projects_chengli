/**
 * This is a theme named pastel
 * Colors are pastel colors
 *
 * @author Cheng Li
 * @version 1.0
 * @since 2018-4-15
 */

import JavaCool303.Cool303Theme;

        import java.awt.*;

public class Cool303Pastel extends Cool303Theme{
    /**
     * Constructor of this theme, rgb values are assigned
     */
    public Cool303Pastel(){
        this.setBackground(new Color(186,255,201));
        this.setButtonColor(new Color(186,225,255));
    }
}
