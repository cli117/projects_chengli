/**
 * This is a theme named summer
 * Colors are summer colors
 *
 * @author Cheng Li
 * @version 1.0
 * @since 2018-4-15
 */

import JavaCool303.Cool303Theme;

import java.awt.*;

public class Cool303Summer extends Cool303Theme{
    /**
     * Constructor of this theme, rgb values are assigned
     */
    public Cool303Summer(){
        this.setBackground(new Color(255,51,51));
        this.setButtonColor(new Color(255,225,0));
    }
}
