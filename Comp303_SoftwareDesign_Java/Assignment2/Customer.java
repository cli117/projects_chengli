//File Name:        Customer.java
//Developers:       Cheng Li
//Purpose:          To construct a object named Customer, and do some manipulation on it
//Inputs:           name, IDNumber and discountPercentage to construct
//Outputs:          None
//Modifications:    First version, no modifications.
//=============================================================================
//
public class Customer {
    private String name;
    private int IDNumber;
    private double discountPercentage;

    //Constructor
    //All customers' default discount percentage is 5%
    public Customer(String customerName, int customerID){
        this.name = customerName;
        this.IDNumber = customerID;
        this.discountPercentage = 5.0;
    }
    //Name:         getIDNumber
    //Developers:   Cheng Li
    //Purpose:      Get the ID of this customer
    //Inputs:       None
    //Outputs:      ID of this customer
    //Modifications:None
    //=============================================================================
    //
    public int getIDNumber(){
        return this.IDNumber;
    }

    //Name:         getIDNumber
    //Developers:   Cheng Li
    //Purpose:      Get the discount percentage of this customer
    //Inputs:       None
    //Outputs:      The discount percentage of this customer
    //Modifications:None
    //=============================================================================
    //
    public double getDiscountPercentage(){
        return this.discountPercentage;
    }
}
