//File Name:        Account.java
//Developers:       Cheng Li
//Purpose:          To construct a object named Account, and do some manipulation on it
//Inputs:           Type. balance and ownerID to construct
//Outputs:          None
//Modifications:    First version, no modifications.
//=============================================================================
//
public class Account {
    private char type;
    private double balance;
    private int ownerID;

    //Constructor
    public Account(char userDefineType, double initBalance, int accountOwnerID){
        this.type = userDefineType;
        this.balance = initBalance;
        this.ownerID = accountOwnerID;
    }
    //Name:         checkBalance
    //Developers:   Cheng Li
    //Purpose:      Get the current balance
    //Inputs:       None
    //Outputs:      Current balance
    //Modifications:None
    //=============================================================================
    //Balance is double.
    public void checkBalance(){
        System.out.printf("Your current balance is %f.\n", this.balance);
    }

    public int getOwnerID(){
        return this.ownerID;
    }
    //Name:         makeDepositWithAward
    //Developers:   Cheng Li
    //Purpose:      Make deposits
    //Inputs:       The amount user want to deposit, and user's discount percentage
    //Outputs:      Balance changed due to the manipulation, or return -1 if any problem occurred
    //Modifications:None
    //=============================================================================
    //If the account type is savings account, an award will be given according to users discount percentage.
    public int makeDepositWithAward(double amount, double discountPercentage) {
        double award = 0;
        if (amount <= 0){
            System.out.print("The amount must be positive!\n");
            return -1;
        }
        else {
            if (this.type == 's')
                award = 1 + discountPercentage / 100;
            this.balance += amount + award;
            System.out.printf("Deposit completed, your current balance is %f.\n", this.balance);
        }
        return 0;
    }
    //Name:         makeWithdrawalWithCharge
    //Developers:   Cheng Li
    //Purpose:      Make withdrawals
    //Inputs:       The amount user want to withdrawal, and user's discount percentage
    //Outputs:      Balance changed due to the manipulation, or return -1 if any problem occurred
    //Modifications:None
    //=============================================================================
    //If the account type is savings account, withdrawals are not permitted if the amount is less than 1000.
    //If the account type is checking account, withdrawals will charged according to users discount percentage.
    //Balance must not be less than the amount withdrawal.
    public int makeWithdrawalWithCharge(double amount, double discountPercentage){
        if (amount <= 0){
            System.out.print("The amount must be positive!\n");
            return -1;
        }
        else if (this.type == 's' && amount < 1000){
            System.out.print("Savings account only permits withdrawals greater than or equal to $1000!\n");
            return -1;
        }
        else if (amount > this.balance){
            System.out.print("Insufficient account balance to complete this manipulation!");
            return -1;
        }
        else if (this.type == 'c'){
            balance -= amount + 1 - discountPercentage / 100;
            System.out.printf("Withdrawal completed, your current balance is %f.\n", this.balance);
            return 0;
        }
        else{
            balance -= amount;
            System.out.printf("Withdrawal completed, your current balance is %f.\n", this.balance);
            return 0;
        }
    }
    //Name:         sameOwnerMoneyTransfer
    //Developers:   Cheng Li
    //Purpose:      Transfer money between two accounts belongs to the same person
    //Inputs:       Another account
    //Outputs:      Money transferred, or return -1 if any problem occurred
    //Modifications:None
    //=============================================================================
    //Balance must not be less than the amount transferred.
    public int sameOwnerMoneyTransfer(Account anotherAccount, double amount){
        if (amount <= 0){
            System.out.print("The amount must be positive!\n");
            return -1;
        }
        else if (this.ownerID != anotherAccount.ownerID){
            System.out.print("Both accounts must be owned by the same customer!\n");
            return -1;
        }
        else if (amount > this.balance){
            System.out.print("Insufficient account balance to complete this manipulation!\n");
            return -1;
        }
        else {
            this.balance -= amount;
            anotherAccount.balance += amount;
            System.out.printf("Transfer completed, your current balance is %f.\n", this.balance);
            return 0;
        }
    }
}
