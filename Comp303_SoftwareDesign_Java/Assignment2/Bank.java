//File Name:        Bank.java
//Developers:       Cheng Li
//Purpose:          UI, to manipulate customers and accounts
//Inputs:           User input
//Outputs:          Manipulations done
//Modifications:    First version, no modifications.
//=============================================================================
//Note: Account ID and customer ID are assigned by system automatically, which are their position in their corresponding ArrayList
//In this way, finding a customer or an account only runs in O(1).

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Bank {
    public static void main(String[] args) {
        ArrayList<Account> accountList = new ArrayList<Account>();
        ArrayList<Customer> customerList = new ArrayList<Customer>();
        int customerNumberLog = 0, accountNumberLog = 0;    //To log the current id of customers and accounts
        Scanner commandReader = new Scanner(System.in);
        String bufferCleaner;                               //When bufferCleaner appears, it means there is a carriage return stays in buffer and need to be removed.
        System.out.print("------------------------------------------------------------\n");
        System.out.print("|                       Bonjour                            |\n");
        System.out.print("|                       Welcome                            |\n");
        System.out.print("|                         to                               |\n");
        System.out.print("|                       McGill                             |\n");
        System.out.print("|                        Trust                             |\n");
        System.out.print("------------------------------------------------------------\n");
        while(true){
        System.out.print("------------------------------------------------------------\n");
        System.out.print("|       Please select the operation you want:              |\n");
        System.out.print("|           Press 'a' to create a customer                 |\n");
        System.out.print("|           Press 'b' to create a bank account             |\n");
        System.out.print("|           Press 'c' to check your balance                |\n");
        System.out.print("|           Press 'd' to deposit money                     |\n");
        System.out.print("|           Press 'e' to withdrawal money                  |\n");
        System.out.print("|           Press 'f' to make a transfer                   |\n");
        System.out.print("|           Press 'g' to quit                              |\n");
        System.out.print("------------------------------------------------------------\n");
        System.out.print("Give us your decision now: ");
        String input = commandReader.next();
        if (input.length() != 1 || input.charAt(0) > 'g' || input.charAt(0) < 'a')
            System.out.print("Invalid input!\n");

        //Purpose: Quit the program
        else if (input.charAt(0) == 'g'){
            System.out.print("Bye, have a nice day!");
            break;
        }

        //Purpose:  Create a new Customer and assign a ID to it.
        //Notes:    User need to remember the ID to do account manipulations
        else if (input.charAt(0) == 'a') {
            String customerName;
            System.out.print("Now you are creating a customer log, please follow the instruction.\n");
            System.out.print("Enter your name: ");
            bufferCleaner = commandReader.nextLine();            //to deal with the carriage return remained in the buffer
            customerName = commandReader.nextLine();            //read customer's name
            Customer newCustomer = new Customer(customerName, customerNumberLog);
            customerList.add(newCustomer);
            System.out.printf("Thank you for joining us, your customer ID is %d, please remember it!\n", customerNumberLog);
            customerNumberLog++;
            continue;
        }

        //Purpose:  Create a new Account and assign a ID to it.
        //Note:     User need to remember the ID to do account manipulations
        else if (input.charAt(0) == 'b'){
            char accountType;
            String afterCustomerNotFound;
            double accountBalance;
            int customerID;
            System.out.print("Now you are creating a bank account, please follow the instruction.\n");
            while(true){
                System.out.print("Enter your account type, 's' for savings and 'c' for checking: ");
                String type = commandReader.next();
                if (type.length() != 1 || !(type.charAt(0) == 's'|| type.charAt(0) == 'c')){            //If the user input is neither 's' nor 'c'
                    System.out.print("Invalid input, it should be either 's' or 'c'!\n");       //Ask the user to enter again
                    continue;
                }
                else{
                    accountType = type.charAt(0);
                    break;
                }
            }
            while(true){
                System.out.print("Set your initial balance now: ");
                try {
                    accountBalance = commandReader.nextDouble();                                        //If the user input is not a number
                }
                catch (InputMismatchException e){
                    System.out.print("Initial balance should be a number, please retry!\n");  //Clear the buffer and ask user to enter again
                    bufferCleaner = commandReader.nextLine();
                    continue;
                }
                break;
            }
            while (true){
                System.out.print("Enter your customer ID: ");
                try {
                    customerID = commandReader.nextInt();
                }
                catch (InputMismatchException e){
                    System.out.print("Customer ID should be a integer, please retry!\n");
                    bufferCleaner = commandReader.nextLine();
                    continue;
                }
                //=====================================================================================================================================
                //Important: If no customer was created, user will STUCK, the code below is for exiting account creating part and go back to home menu.
                //Similar code appears several times, all for similar purpose: help user to go back to home menu so they are not stucking.
                //=====================================================================================================================================
                if (customerID >= customerList.size()){
                    System.out.print("Customer ID is not registered!\nIf you want to register now, press 'h' to go back to home menu\nOr press 'r' to retry.\n");
                    while(true){
                        afterCustomerNotFound = commandReader.next();
                        if (afterCustomerNotFound.length() != 1 || !(afterCustomerNotFound.charAt(0) == 'h' || afterCustomerNotFound.charAt(0) == 'r')){
                            System.out.print("Invalid input, 'h' to go back to home menu or 'r' to retry.\n");
                            continue;
                        }
                        if (afterCustomerNotFound.charAt(0) == 'r')
                            break;
                        if (afterCustomerNotFound.charAt(0) == 'h')
                            break;
                    }
                    if (afterCustomerNotFound.charAt(0) == 'h')
                        break;
                }
                else {
                    Account newAccount = new Account(accountType, accountBalance, customerID);      //Construct a new Account and add it to Account ArrayList
                    accountList.add(newAccount);
                    System.out.printf("Thank you for choosing McGill Trust, your account has been logged!\nThis account's account ID is %d, please remember it!\n", accountNumberLog);
                    accountNumberLog++;
                    break;
                }
            }
        }

        //Purpose:      Check the current balance of an account.
        //Note:         This account should belong to you or you are not permitted to check.
        else if (input.charAt(0) == 'c'){
            int accountID, customerID;
            String afterNotMatch;
                System.out.print("Now you are finding out your account balance.\n");
                while(true){
                    //Keep asking user for a valid customer ID
                    //Similar construction appears several times, all for same purpose
                    while (true) {
                        System.out.print("Enter your customer ID: ");
                        try {
                            customerID = commandReader.nextInt();
                        } catch (InputMismatchException e) {
                            System.out.print("Customer ID is a integer, please retry!\n");
                            continue;
                        }
                        if (customerID >= customerList.size()) {
                            System.out.print("Invalid customer ID, please retry!\n");
                            continue;
                        }
                        break;
                    }
                    //Keep asking user for a valid account ID
                    //Similar construction appears several times, all for same purpose
                    while (true){
                        System.out.print("Enter your account ID: ");
                        try{
                            accountID = commandReader.nextInt();
                        }
                        catch (InputMismatchException e){
                            System.out.print("Account ID is a integer, please retry!\n");
                            continue;
                        }
                        break;
                    }

                    //If you are not the owner of the account, you cannot check the current balance of it.
                    if (accountList.get(accountID).getOwnerID() != customerID){
                        System.out.print("This is not your account, you cannot do this manipulation.\nPress 'h' to go back to home menu\nOr press 'r' to retry.\n");
                        while (true) {
                            afterNotMatch = commandReader.next();
                            if (afterNotMatch.length() != 1 || !(afterNotMatch.charAt(0) == 'h' || afterNotMatch.charAt(0) == 'r')) {
                                System.out.print("Invalid input, 'h' to go back to home menu or 'r' to retry.\n");
                                continue;
                            }
                            break;
                        }
                            if (afterNotMatch.charAt(0) == 'h')
                                break;
                            if (afterNotMatch.charAt(0) == 'r')
                                continue;
                    }
                    accountList.get(accountID).checkBalance();
                    break;
                }
                continue;

        }

        //Purpose:      Deposit money into an existing account
        //Note:         The account should belong to you or you cannnot do this manipulation
        else if (input.charAt(0) == 'd'){
            int accountID, customerID, errorCheck;
            String afterNotMatch;
            double amount, discountPercentage;
            System.out.print("Now you are depositing money to an account.\n");
            while (true){
                while (true) {
                    System.out.print("Enter your customer ID: ");
                    try {
                        customerID = commandReader.nextInt();
                    } catch (InputMismatchException e) {
                        System.out.print("Customer ID is a integer, please retry!\n");
                        continue;
                    }
                    if (customerID >= customerList.size()) {
                        System.out.print("Invalid customer ID, please retry!\n");
                        continue;
                    }
                    break;
                }

                while (true) {
                    System.out.print("Enter your account ID: ");
                    try {
                        accountID = commandReader.nextInt();
                    } catch (InputMismatchException e) {
                        System.out.print("Account ID is a integer, please retry!\n");
                        continue;
                    }
                    if (accountID >= accountList.size()) {
                        System.out.print("Invalid account ID, please retry!\n");
                        continue;
                    }
                    break;
                }
                //If you are not the owner of the account, you cannot deposit money into it.
                if (accountList.get(accountID).getOwnerID() != customerID) {
                    System.out.print("This is not your account, you cannot do this manipulation.\nPress 'h' to go back to home menu\nOr press 'r' to retry.\n");
                    while (true) {
                        afterNotMatch = commandReader.next();
                        if (afterNotMatch.length() != 1 || !(afterNotMatch.charAt(0) == 'h' || afterNotMatch.charAt(0) == 'r')) {
                            System.out.print("Invalid input, 'h' to go back to home menu or 'r' to retry.\n");
                            continue;
                        }
                        break;
                    }
                    if (afterNotMatch.charAt(0) == 'h')
                        break;
                    if (afterNotMatch.charAt(0) == 'r')
                        continue;
                }


                while (true){
                    System.out.print("Enter the amount you want to deposit: ");
                    try{
                        amount = commandReader.nextDouble();
                    }
                    catch (InputMismatchException e){
                        System.out.print("Amount is a number, please retry!\n");
                        continue;
                    }
                    break;
                }
                errorCheck = accountList.get(accountID).makeDepositWithAward(amount, customerList.get(customerID).getDiscountPercentage());
                if (errorCheck == -1){
                    bufferCleaner = commandReader.nextLine();
                    continue;
                }
                break;
            }
            continue;
        }

        //Purpose:      Withdrawal money into an existing account
        //Note:         The account should belong to you or you cannnot do this manipulation
        else if (input.charAt(0) == 'e'){
            int accountID, customerID, errorCheck;
            String afterNotMatch;
            double amount, discountPercentage;
            System.out.print("Now you are withdrawaling money to an account.\n");
            while (true){
                while (true) {
                    System.out.print("Enter your customer ID: ");
                    try {
                        customerID = commandReader.nextInt();
                    } catch (InputMismatchException e) {
                        System.out.print("Customer ID is a integer, please retry!\n");
                        continue;
                    }
                    if (customerID >= customerList.size()) {
                        System.out.print("Invalid customer ID, please retry!\n");
                        continue;
                    }
                    break;
                }

                while (true) {
                    System.out.print("Enter your account ID: ");
                    try {
                        accountID = commandReader.nextInt();
                    } catch (InputMismatchException e) {
                        System.out.print("Account ID is a integer, please retry!\n");
                        continue;
                    }
                    if (accountID >= accountList.size()) {
                        System.out.print("Invalid account ID, please retry!\n");
                        continue;
                    }
                    break;
                }

                //If you are not the owner of the account, you cannot withdrawal money
                if (accountList.get(accountID).getOwnerID() != customerID) {
                    System.out.print("This is not your account, you cannot do this manipulation.\nPress 'h' to go back to home menu\nOr press 'r' to retry.\n");
                    while (true) {
                        afterNotMatch = commandReader.next();
                        if (afterNotMatch.length() != 1 || !(afterNotMatch.charAt(0) == 'h' || afterNotMatch.charAt(0) == 'r')) {
                            System.out.print("Invalid input, 'h' to go back to home menu or 'r' to retry.\n");
                            continue;
                        }
                        break;
                    }
                    if (afterNotMatch.charAt(0) == 'h')
                        break;
                    if (afterNotMatch.charAt(0) == 'r')
                        continue;
                }


                while (true){
                    System.out.print("Enter the amount you want to withdrawal: ");
                    try{
                        amount = commandReader.nextDouble();
                    }
                    catch (InputMismatchException e){
                        System.out.print("Amount is a number, please retry!\n");
                        continue;
                    }
                    break;
                }
                errorCheck = accountList.get(accountID).makeWithdrawalWithCharge(amount, customerList.get(customerID).getDiscountPercentage());
                if (errorCheck == -1){
                    bufferCleaner = commandReader.nextLine();
                    continue;
                }
                break;
            }
            continue;
        }

        //Purpose:      Transfer money from an existing account into another existing account
        //Note:         Both accounts should belong to you or you cannnot do this manipulation
        else if (input.charAt(0) == 'f'){
            int accountIDTransferor, accountIDReceiver,customerID, errorCheck;
            String afterNotMatch;
            double amount, discountPercentage;
            System.out.print("Now you are trasfering money to another your account.\n");
            while (true){
                while (true) {
                    System.out.print("Enter your customer ID: ");
                    try {
                        customerID = commandReader.nextInt();
                    } catch (InputMismatchException e) {
                        System.out.print("Customer ID is a integer, please retry!\n");
                        continue;
                    }
                    if (customerID >= customerList.size()) {
                        System.out.print("Invalid customer ID, please retry!\n");
                        continue;
                    }
                    break;
                }

                while (true) {
                    System.out.print("Enter your account ID(transferor): ");
                    try {
                        accountIDTransferor = commandReader.nextInt();
                    } catch (InputMismatchException e) {
                        System.out.print("Account ID is a integer, please retry!\n");
                        continue;
                    }
                    if (accountIDTransferor >= accountList.size()) {
                        System.out.print("Invalid account ID, please retry!\n");
                        continue;
                    }
                    break;
                }

                //If you are not the owner of the account, you cannot transfer the money.
                if (accountList.get(accountIDTransferor).getOwnerID() != customerID) {
                    System.out.print("This is not your account, you cannot do this manipulation.\nPress 'h' to go back to home menu\nOr press 'r' to retry.\n");
                    while (true) {
                        afterNotMatch = commandReader.next();
                        if (afterNotMatch.length() != 1 || !(afterNotMatch.charAt(0) == 'h' || afterNotMatch.charAt(0) == 'r')) {
                            System.out.print("Invalid input, 'h' to go back to home menu or 'r' to retry.\n");
                            continue;
                        }
                        break;
                    }
                    if (afterNotMatch.charAt(0) == 'h')
                        break;
                    if (afterNotMatch.charAt(0) == 'r')
                        continue;
                }


                while (true){
                    System.out.print("Enter the amount you want to transfer: ");
                    try{
                        amount = commandReader.nextDouble();
                    }
                    catch (InputMismatchException e){
                        System.out.print("Amount is a number, please retry!\n");
                        continue;
                    }
                    break;
                }

                while (true){
                    System.out.print("Enter your account ID(receiver): ");
                    try {
                        accountIDReceiver = commandReader.nextInt();
                    } catch (InputMismatchException e) {
                        System.out.print("Account ID is a integer, please retry!\n");
                        continue;
                    }
                    if (accountIDReceiver >= accountList.size()) {
                        System.out.print("Invalid account ID, please retry!\n");
                        continue;
                    }
                    break;
                }
                errorCheck = accountList.get(accountIDTransferor).sameOwnerMoneyTransfer(accountList.get(accountIDReceiver),amount);
                if (errorCheck == -1){
                    bufferCleaner = commandReader.nextLine();
                    continue;
                }
                break;
            }
            continue;
        }
        }
    }
}
