/**
 * </h1>Standard test runner from JUnit tutorialspoint</h1>
 *
 * @author Cheng Li
 * @version 1.0
 * @since 2018-02-21
 */

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class TestPinRunner {
    public static void main(String[] args) {
        Result result = JUnitCore.runClasses(TestPin.class);

        for (Failure failure : result.getFailures()) {
            System.out.println(failure.toString());
        }

        System.out.println(result.wasSuccessful());
    }
}