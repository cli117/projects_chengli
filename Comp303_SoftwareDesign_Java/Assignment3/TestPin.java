/**
 * This method is a jUnit test method
 * It is Parameterized
 * 103 cases for each method
 *
 * @author Cheng Li
 * @version 1.0
 * @since 2018-02-21
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.Before;

import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.junit.runner.RunWith;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(Parameterized.class)

public class TestPin {
    private String newPin;
    private Pin testPin;

    /**
     * This is the initializer for going through all the parameters
     */
    @Before
    public void initialize(){
        testPin = new Pin();
    }
    public TestPin(String newPin){
        this.newPin = newPin;
    }

    /**
     * This method collects all the cases
     * @return ArrayList This ArrayList returned contains all the cases as String
     */
    @Parameterized.Parameters
    public static Collection testCases() {
        //From 00 to 99
        char testFirstBit = '0', testSecondBit =  '0';
        ArrayList<String> testCases = new ArrayList<String>();
        for (int i = 0; i < 10; i++){
            for (int j = 0; j < 10; j++){
                testCases.add("" + testFirstBit + testSecondBit);
                testSecondBit++;
            }
            testFirstBit++;
            testSecondBit = '0';
        }
        //When the length is less than 2
        testCases.add("1");
        //When there are nonnumeric chars
        testCases.add("AB");
        //When the length is larger than 2
        testCases.add("999");
        return testCases;
    }

    /**
     * We test setter, getter and isValid in one shot
     * Since isValid is called by setter
     * Set a newPin and assertEquals to see if it is the same as currentPin
     */
    @Test
    public void testSetGetAndIsValid(){
        testPin.setNewPin(newPin);
        assertEquals(newPin, testPin.getCurrentPin());
    }

    /**
     * Tester for isMatch
     * Check if all cases matches current pin
     */
    @Test
    public void testIsMatch(){
        assertTrue(testPin.isMatch(newPin));
    }
}
