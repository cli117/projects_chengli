/**
 * </h1>This is main method</h1>
 * Main method ask user for old pin, and ask for new pin if old pin matches
 * After that, new pin will be updated if it is valid
 * Then the program exit
 *
 * @author Cheng Li
 * @version 1.0
 * @since 2018-02-21
 */

import java.util.Scanner;
public class PinMain {
    /**
     * This is the main method that gives user interface
     * @param args unused
     */
    public static void main(String[] args) {
        Pin currentPin = new Pin();
        String inputPin, newPin;
        Scanner getInput = new Scanner(System.in);

        System.out.print("Welcome to pin update!\n");

        while (true) {
            System.out.print("Please input your old pin: ");
            inputPin = getInput.nextLine();
            if (currentPin.isMatch(inputPin)) break;
        }

        while (true) {
            System.out.print("Please input your new pin: ");
            newPin = getInput.nextLine();
            if (currentPin.setNewPin(newPin)) break;
        }

    }
}
