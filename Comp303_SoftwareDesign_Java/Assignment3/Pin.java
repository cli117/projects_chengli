/**
 * <h1>New object named Pin</h1>
 * The Pin Class makes a new object contains two characters
 * <p>
 *     You can get and set its characters through getter and setter
 *     Setter will automatically check if the input is valid
 *
 * @author Cheng Li
 * @version 1.0
 * @since 2018-02-21
 */

public class Pin {
    private char firstBit;
    private char secondBit;

    /**
     * This is a constructor
     * New Pin object's firstBit and SecondBit will be initialized to '0'
     * No input needed
     */
    public Pin(){
        firstBit = '0';
        secondBit = '0';
    }

    /**
     * This is a getter
     * @return String This returns object's current Pin number as a String
     * i.e. "" + firstBit + secondBit
     */
    public String getCurrentPin(){
        return "" + firstBit + secondBit;
    }

    /**
     * This is a setter, object's characters will be set after called if the new Pin is valid
     * isValid() is called by this method
     * @param newPin This is the new Pin you want to set
     * @return Boolean If the input new Pin is valid, if so, return true, otherwise return false
     */
    public boolean setNewPin(String newPin){
        if (!isValid(newPin)) {
            System.out.print("Invalid pin.\n");
            return false;
        }
        firstBit = newPin.charAt(0);
        secondBit = newPin.charAt(1);
        System.out.print("New pin confirmed.\n");
        System.out.printf("Your pin has been updated to %s\n", newPin);
        return true;
    }

    /**
     * This method checks if the input old Pin matches current Pin
     * @param inputPin User input old pin
     * @return Return true if matches, otherwise return false
     */
    public boolean isMatch(String inputPin){
        if (inputPin.length() != 2){
            System.out.print("That is not your old pin.\n");
            return false;
        }
        if (inputPin.charAt(0) == firstBit && inputPin.charAt(1) == secondBit) {
            System.out.print("Old pin confirmed.\n");
            return true;
        }
        System.out.print("That is not your old pin.\n");
        return false;
    }

    /**
     * This method checks if new pin the user want to set is valid or not
     * It can only called by setter, since it is private
     * @param newPin The new pin user entered
     * @return Return true if it is valid, otherwise return false
     */
    private boolean isValid(String newPin){
        if (newPin.length() != 2) return false;                                                //1. length is not 2
        if (newPin.charAt(0) < '0' || newPin.charAt(0) > '9') return false;                   //2. nonnumeric char
        if (newPin.charAt(1) < '0' || newPin.charAt(1) > '9') return false;
        if (newPin.charAt(0) == firstBit && newPin.charAt(1) == secondBit) return false;    //3. duplication
        return true;
    }
}
