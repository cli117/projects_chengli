import numpy as np
import random
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)

from sklearn.naive_bayes import MultinomialNB
from sklearn import svm
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score, confusion_matrix
from collections import defaultdict
from sklearn.feature_extraction.text import CountVectorizer

import nltk
from nltk.corpus import stopwords
nltk.download('stopwords')
nltk.download('punkt')
nltk.download('wordnet')

from nltk.stem import PorterStemmer
from nltk.stem import WordNetLemmatizer

negativeFile = 'rt-polarity.neg'
positiveFile = 'rt-polarity.pos'

def drop_stopwords(input_list):
    stoplist = stopwords.words('english')
    clean = [[word for word in sentence.split(" ") if word not in stoplist] for sentence in input_list]
    i = 0
    for word_list in clean:
        input_list[i] = " ".join(word_list)
        i = i + 1
    return input_list

porter = PorterStemmer()

def stem_list(input_list):
    input_list = [[porter.stem(word) for word in sentence.split(" ")] for sentence in input_list]
    i = 0
    for word_list in input_list:
        input_list[i] = " ".join(word_list)
        i = i + 1

    return input_list

lemmatizer = WordNetLemmatizer()

def lemmatize_list(input_list):
    input_list = [[lemmatizer.lemmatize(word) for word in sentence.split(" ")] for sentence in input_list]
    i = 0
    for word_list in input_list:
        input_list[i] = " ".join(word_list)
        i = i + 1
    return input_list

def remove_infrequent(input_list, thresholdAsPersentage):
    # Calculate the frequencies and save them in a dictionary
    frequencies = defaultdict(int)
    for words in [sentence.split(" ") for sentence in input_list]:
        for word in words:
            frequencies[word] += 1

    thresholdAsNum = len(frequencies) * thresholdAsPersentage / 100

    result = []
    tempLine = []
    for words in [sentence.split(" ") for sentence in input_list]:
        for word in words:
            if frequencies[word] > thresholdAsNum:
                tempLine.append(word)
        result.append(tempLine)
        tempLine = []

    i = 0
    for word_list in result:
        result[i] = " ".join(word_list)
        i += 1
    return result

def get_data(test_ratio_as_persentage):
    """
    Returns a preprocessed X with the entire corpus and a y with
    neg=0/pos=1 labels.
    """
    with open(negativeFile, mode='r', encoding='cp1252') as f:
        neg_data = f.readlines()

    with open(positiveFile, mode='r', encoding='cp1252') as f:
        pos_data = f.readlines()

    # TODO: Do preprocess here
    # neg_data = remove_infrequent(neg_data, 0.5)
    # pos_data = remove_infrequent(pos_data, 0.5)
    # neg_data = lemmatize_list(neg_data)
    # pos_data = lemmatize_list(pos_data)
    # neg_data = stem_list(neg_data)
    # pos_data = stem_list(pos_data)
    # neg_data = drop_stopwords(neg_data)
    # pos_data = drop_stopwords(pos_data)

    n_neg_train = int(len(neg_data)*(1-test_ratio_as_persentage/100))
    neg_trainX = neg_data[:n_neg_train]
    neg_testX = neg_data[n_neg_train:]

    n_pos_train = int(len(pos_data) * (1 - test_ratio_as_persentage / 100))
    pos_trainX = pos_data[:n_pos_train]
    pos_testX = pos_data[n_pos_train:]


    trainY = np.zeros(len(neg_trainX)+len(pos_trainX))
    trainY[len(neg_trainX):] = 1
    testY = np.zeros(len(neg_testX)+len(pos_testX))
    testY[len(neg_testX):] = 1

    trainXasText = np.concatenate([np.array(neg_trainX), np.array(pos_trainX)])
    testXasText = np.concatenate([np.array(neg_testX), np.array(pos_testX)])

    # Unigram, split words as features
    vector = CountVectorizer(min_df=1, ngram_range=(1, 1))
    trainX = vector.fit(trainXasText).transform(trainXasText)
    testX = vector.transform(testXasText)
    feature_names = vector.get_feature_names()

    return trainX, trainY, testX, testY

def random_baseline(testY):
    rand = []
    for i in range(len(testY)):
        rand.append(random.randint(0, 1))
    print("Random baseline accuracy: ")
    print(accuracy_score(testY, rand))

def FLogisticReg(lrInstance, trainX, trainY, testX, testY):
    lrInstance.fit(trainX, trainY)
    result = lrInstance.predict(testX)
    print("Logistic regression accuracy: ")
    print(confusion_matrix(testY, result))
    print(accuracy_score(testY, result))


def FSVM(svmInstance, trainX, trainY, testX, testY):
    svmInstance.fit(trainX, trainY)
    result = svmInstance.predict(testX)
    print("SVM accuracy: ")
    print(confusion_matrix(testY, result))
    print(accuracy_score(testY, result))

def FNaiveBayes(nbInstance, trainX, trainY, testX, testY):
    nbInstance.fit(trainX, trainY)
    result = nbInstance.predict(testX)
    print("NB accuracy: ")
    print(confusion_matrix(testY, result))
    print(accuracy_score(testY, result))


def main():
    # input of get_data is the percentage of test set
    trainX, trainY, testX, testY = get_data(10)
    random_baseline(testY)

    lrInstance = LogisticRegression()
    FLogisticReg(lrInstance, trainX, trainY, testX, testY)

    svmInstance = svm.SVC(kernel='linear')
    FSVM(svmInstance, trainX, trainY, testX, testY)

    nbInstance = MultinomialNB()
    FNaiveBayes(nbInstance, trainX, trainY, testX, testY)


if __name__ == '__main__':
    main()

