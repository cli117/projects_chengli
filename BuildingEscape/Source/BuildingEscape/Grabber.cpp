// Fill out your copyright notice in the Description page of Project Settings.

#include "Grabber.h"

#define OUT

// Sets default values for this component's properties
UGrabber::UGrabber()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UGrabber::BeginPlay()
{
	Super::BeginPlay();
    Player = GetWorld()->GetFirstPlayerController();
	// Look for attached Physics Handle
    ComponentChecking();

}

void UGrabber::ComponentChecking() {
    PhysicsHandle = GetOwner()->FindComponentByClass<UPhysicsHandleComponent>();
    InputComponent = GetOwner()->FindComponentByClass<UInputComponent>();
    if (PhysicsHandle){
        InputComponent->BindAction("Grab", IE_Pressed, this, &UGrabber::Grab);
        InputComponent->BindAction("Grab", IE_Released, this, &UGrabber::Release);
	} else{
	    FString name = GetOwner()->GetName();
	    UE_LOG(LogTemp, Error, TEXT("%s do not have a Physics Handle Component!\n"), *name);
	}

    if (InputComponent){

	} else{
        FString name = GetOwner()->GetName();
        UE_LOG(LogTemp, Error, TEXT("%s do not have a Input Component!\n"), *name);
	}
}

void UGrabber::Grab(){
    UE_LOG(LogTemp, Warning, TEXT("Grab!"));
    auto ToGrab = GetPhysHit().GetComponent();
    auto ActorHit = GetPhysHit().GetActor();
    FRotator temp(0.f, 0.f, 0.f);
    if (ActorHit){
        PhysicsHandle->GrabComponentAtLocationWithRotation(
                ToGrab,
                NAME_None,
                ToGrab->GetOwner()->GetActorLocation(),
                temp
        );
    }

}

void UGrabber::Release(){
    UE_LOG(LogTemp, Warning, TEXT("Release!"));
    PhysicsHandle->ReleaseComponent();
}

FHitResult UGrabber::GetPhysHit(){
    Player->GetPlayerViewPoint(OUT PlayerViewPointLocation, OUT PlayerViewPointRotation);
//	UE_LOG(LogTemp, Warning, TEXT("I am at %s"), *PlayerViewPointLocation.ToString());
//  UE_LOG(LogTemp, Warning, TEXT("I am looking at %s"), *PlayerViewPointRotation.ToString());

    // Line-trace(AKA ray-cast) out to reach distance
    FCollisionQueryParams TraceParameters(FName(TEXT("")), false, GetOwner());
    GetWorld()->LineTraceSingleByObjectType(
            OUT Hit,
            PlayerViewPointLocation,
            PlayerViewPointLocation + PlayerViewPointRotation.Vector() * 100.f,
            FCollisionObjectQueryParams(ECollisionChannel::ECC_PhysicsBody),
            TraceParameters
    );

    AActor *ActorHit = Hit.GetActor();
    if (ActorHit){
        UE_LOG(LogTemp, Warning, TEXT("Line trace hit: %s"), *(ActorHit->GetName()));
    }
    return Hit;
}

// Called every frame
void UGrabber::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
    if (PhysicsHandle->GrabbedComponent){
        Player->GetPlayerViewPoint(OUT PlayerViewPointLocation, OUT PlayerViewPointRotation);
        FVector LineTracedEnd = PlayerViewPointLocation + PlayerViewPointRotation.Vector() * 100.f;
        PhysicsHandle->SetTargetLocation(LineTracedEnd);
    }
}

