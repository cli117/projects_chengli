// Fill out your copyright notice in the Description page of Project Settings.

#include "PositionReport.h"
#include "Engine.h"
#include "Gameframework/Actor.h"

// Sets default values for this component's properties
UPositionReport::UPositionReport()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

}


// Called when the game starts
void UPositionReport::BeginPlay()
{
	Super::BeginPlay();
    FString name = GetOwner()->GetName();
    FString position = GetOwner()->GetTransform().GetLocation().ToString();

    UE_LOG(LogTemp, Error, TEXT("Position report reporting for duty on %s!\n"), *name);
	UE_LOG(LogTemp, Error, TEXT("%s is at %s\n"), *name, *position);
}


// Called every frame
void UPositionReport::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

}

