﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[System.Serializable]
public struct HomeMadePolygon
{
    public Vector3[] boundary;
    public HomeMadePolygon(Vector3[] boundary)
    {
        this.boundary = boundary;
    }
}

public struct NavmeshEdge
{
    public List<GameObject> NavCubes;
    public Vector3 DeepPts;
    public bool IsDone;
    public bool IsEmpty;

    public NavmeshEdge(List<GameObject> NavCubes, Vector3 DeepPts)
    {
        this.NavCubes = NavCubes;
        this.DeepPts = DeepPts;
        IsDone = false;
        if (NavCubes.Count == 0)
            IsEmpty = true;
        else IsEmpty = false;
    }

    public NavmeshEdge(NavmeshEdge ToSet)
    {
        NavCubes = ToSet.NavCubes;
        DeepPts = ToSet.DeepPts;
        IsDone = true;
        IsEmpty = ToSet.IsEmpty;
    }

}

public struct PointsDecData
{
    public int index;
    public Vector3 NewVertex;

    public PointsDecData(int index, Vector3[] boundary)
    {
        this.index = index;
        int m1 = index - 1, p1 = index + 1, p2 = index + 2;
        if (m1 < 0) m1 = boundary.Length - 1;
        if (p1 > boundary.Length - 1)
        {
            p1 -= boundary.Length - 1;
            p2 -= boundary.Length - 1;
        }
        if (p1 == boundary.Length - 1)
        {
            p2 = 0;
        }
        MapManager.Edge edge1 = new MapManager.Edge(boundary[m1], boundary[index]);
        MapManager.Edge edge2 = new MapManager.Edge(boundary[p1], boundary[p2]);
        NewVertex = edge1.Intersect(edge2);
    }

    public PointsDecData(PointsDecData copy)
    {
        this.index = copy.index;
        this.NewVertex = copy.NewVertex;
    }
}

public class Simulator : MonoBehaviour
{
    [SerializeField]
    GameObject FieldOfView;

    [SerializeField]
    GameObject TracingCube;

    [SerializeField]
    float diameter;

    [Header("Non-Convex")]
    [SerializeField]
    bool IsNonConvex;
    [SerializeField]
    List<HomeMadePolygon> Convex;

    float radius;
    float GridSize;

    int lines;
    int ZigZagCurrentLine;
    int ZigZagCurrentPos;
    int ZigZagFirstLine;
    int ConvexIndex;
    int TracingIndex;
    int DeepsCounter;
    int TracingCircleNum;

    bool TracingInBound;
    bool TracingPointsDecrement;
    bool TracingHaveObstacle;
    bool TracingPolygonCalculated;

    MapManager mapManager;
    GameObject CharacterInstance;
    List<MapManager.Edge> Edges;
    List<GameObject> FOWs;
    List<PointsDecData> decDatas;
    List<Polygon> TracingPolygons;
    float minDistance;
    NavMeshAgent agent;
    bool RunGreedy;
    bool RunZigZag;
    bool Tracing;

    bool OneTime;

    bool ZigZagFirstRow;
    bool ZigZagReverse;

    bool TracingFirstTime;
    bool FirstTrace;
    Vector3[] boundary;
    List<List<Vector3>> TracingDes;
    List<Vector3> Deeps;
    List<NavmeshEdge> TracingCubes;
    bool TracingDone;
    bool IsTracing;
    // Start is called before the first frame update
    void Start()
    {
        //diameter = FieldOfView.transform.localScale.z;
        radius = diameter / 2;
        mapManager = GameObject.Find("MapManager").GetComponent<MapManager>();
        CharacterInstance = GameObject.Find("Character");
        minDistance = Mathf.Infinity;
        agent = CharacterInstance.GetComponent<NavMeshAgent>();
        ZigZagFirstRow = true;
        InitializeVariable();
        ConvexIndex = 0;
        TracingDone = false;
        TracingPolygons = new List<Polygon>();
    }

    void InitializeVariable()
    {
        Edges = new List<MapManager.Edge>();
        TracingFirstTime = true;
        OneTime = true;
        FirstTrace = true;
        Deeps = new List<Vector3>();
        DeepsCounter = 0;
        TracingDes = new List<List<Vector3>>();
        IsTracing = false;
        TracingIndex = 0;
        TracingCubes = new List<NavmeshEdge>();
        decDatas = new List<PointsDecData>();
        TracingPointsDecrement = false;
        TracingCircleNum = 0;
    }

    public void LaunchGreedy()
    {
        RunGreedy = true;
    }

    public void LaunchZigZag()
    {
        RunZigZag = true;
    }

    public void LaunchTracing()
    {
        Tracing = true;
    }

    public void PauseAll()
    {
        RunGreedy = false;
        RunZigZag = false;
        Tracing = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (mapManager.GetIsDone()) return;
        if (mapManager.ColumnLog.Count == 0) return;
        GridSize = mapManager.GetGridSize();
        lines = mapManager.ColumnLog.Count;
        TracingHaveObstacle = (mapManager.GetMapType() == MapManager.MapType.convexWithHole || mapManager.GetMapType() == MapManager.MapType.nonConvexWithHole);
        if (RunGreedy)
        {
            FOWs = mapManager.GetFogOfWars();
            minDistance = Mathf.Infinity;
            GameObject nearest = null;
            foreach (GameObject fow in FOWs)
            {
                float distance = Mathf.Infinity;
                if (fow != null)
                {
                    distance = HelperFunctions.GetDistanceBetweenTwoVector3(fow.transform.position, CharacterInstance.transform.position);
                }

                if (minDistance > distance)
                {
                    nearest = fow;
                    minDistance = distance;
                }
            }
            if (nearest != null)
            {
                agent.isStopped = false;
                agent.SetDestination(nearest.transform.position);
            }
        }

        else if (RunZigZag)
        {
            for (int i = 0; i < mapManager.ColumnLog.Count; i++)
            {
                if (mapManager.ColumnLog[i] != 0)
                {
                    ZigZagFirstLine = i;
                    break;
                }
            }

            print("First Row: " + ZigZagFirstLine);

            int previous = 0;
            FOWs = mapManager.GetFogOfWars();
            if (ZigZagFirstRow)
            {
                ZigZagCurrentLine = ((int)(radius / GridSize)) + ZigZagFirstLine;
                print("Line: " + ZigZagCurrentLine);
                for (int i = 0; i < ZigZagCurrentLine; i++)
                {
                    ZigZagCurrentPos += mapManager.ColumnLog[i];
                }
                print("Pos: " + ZigZagCurrentPos);
                previous = ZigZagCurrentPos;
                ZigZagFirstRow = false;
            }

            for (; ZigZagCurrentPos < FOWs.Count; ZigZagCurrentPos++)
            {
                if (FOWs[ZigZagCurrentPos] == null) continue;
                print("CurrentLineCount: " + mapManager.ColumnLog[ZigZagCurrentLine]);
                if (ZigZagCurrentPos - previous < mapManager.ColumnLog[ZigZagCurrentLine])
                {
                    agent.isStopped = false;
                    agent.SetDestination(FOWs[ZigZagCurrentPos].transform.position);
                }
            }
        }

        else if (Tracing && !IsNonConvex && !TracingHaveObstacle)
        {
            TracingConvex(mapManager.GetBoundaryPoints(), false);
        }

        else if (Tracing && IsNonConvex && !TracingHaveObstacle)
        {
            if (Convex.Count == 0)
            {
                print("Please Enter All Convex!");
                return;
            }

            if (ConvexIndex >= Convex.Count) return;

            TracingConvex(Convex[ConvexIndex].boundary, true);

        }

        else if (Tracing && !IsNonConvex && TracingHaveObstacle)
        {
            #region Cookdata
            if (!TracingPolygonCalculated)
            {
                Vector3[] outerBoundary = mapManager.GetBoundaryPoints();
                Polygon outerPolygon = new Polygon(outerBoundary, true);
                outerPolygon = HertelMehlhorn.ReverseIfNotCounterClockWise(outerPolygon);
                List<Polygon> obstaclesPolygon = new List<Polygon>();
                MapData.Obstacle[] obstacles = MapData.GetObstacles(mapManager.GetMapType());
                for (int i = 0; i < obstacles.Length; i++)
                {
                    List<Vector3> PointsVectors = new List<Vector3>();
                    LinkedListNode<Vector3> currentNode = obstacles[i].BoundaryPoints.First;
                    for (int j = 0; j < obstacles[i].BoundaryPoints.Count - 1; j++)
                    {
                        if (currentNode != null)
                        {
                            PointsVectors.Add(currentNode.Value);
                            currentNode = currentNode.Next;
                        }
                    }
                    Polygon tempObstacle = new Polygon(PointsVectors, false);
                    tempObstacle = HertelMehlhorn.ReverseIfNotClockWise(tempObstacle);
                    obstaclesPolygon.Add(tempObstacle);
                }
                List<Triangle> delaunay = Delaunay.DelaunayTriangles(EarClipping.EarClipWithHole(outerPolygon, obstaclesPolygon));
                TracingPolygons = HertelMehlhorn.MergeTriangles(delaunay);
                TracingPolygonCalculated = true;
            }
            #endregion
            if (ConvexIndex >= TracingPolygons.Count) return;
            TracingConvex(TracingPolygons[ConvexIndex].GetVerticesAsVecter3s(), true);
        }

        else if (Tracing && IsNonConvex && TracingHaveObstacle)
        {

        }
    }

    #region TracingAlgoVector3[]AndIsResettingIn
    private void TracingConvex(Vector3[] pointsCopy, bool IsRestting)
    {
        IsTracing = true;

        if (TracingDone)
        {
            agent.isStopped = false;
            if (TracingIndex >= TracingCubes.Count)
            {
                TracingFirstTime = true;
                IsTracing = false;
                TracingDone = !IsRestting;
                ConvexIndex++;
                InitializeVariable();
            }
            if (TracingCubes[TracingIndex].NavCubes.Count == 0)
            {
                TracingCubes[TracingIndex] = new NavmeshEdge(TracingCubes[TracingIndex]);
            }
            if (!TracingCubes[TracingIndex].IsDone)
            {
                for (int j = 0; j < TracingCubes[TracingIndex].NavCubes.Count; j++)
                {
                    if (j < TracingCubes[TracingIndex].NavCubes.Count - 1)
                    {
                        if (TracingCubes[TracingIndex].NavCubes[j] == null) continue;
                    }
                    if (TracingCubes[TracingIndex].NavCubes[j] != null)
                    {
                        agent.SetDestination(TracingCubes[TracingIndex].NavCubes[j].transform.position);
                    }
                    if (j == TracingCubes[TracingIndex].NavCubes.Count - 1 && !TracingCubes[TracingIndex].IsEmpty)
                    {
                        agent.SetDestination(TracingCubes[TracingIndex].DeepPts);
                        if (IsArrived(agent.gameObject.transform.position, TracingCubes[TracingIndex].DeepPts))
                        {
                            TracingCubes[TracingIndex] = new NavmeshEdge(TracingCubes[TracingIndex]);
                        }
                        goto OUT;
                    }
                    goto OUT;
                }
            }

            else
            {
                TracingIndex++;
            }

        OUT:
            return;
        }
        bool IsEnd = false;
        bool IsDrawn = mapManager.GetMapDrawn();
        if (!IsDrawn) return;
        List<Vector3>[] temp;
        if (TracingFirstTime)
        {
            //pointsCopy = mapManager.GetBoundaryPoints();
            boundary = new Vector3[pointsCopy.Length];
            for (int i = 0; i < boundary.Length; i++)
            {
                boundary[i] = new Vector3(pointsCopy[i].x, pointsCopy[i].y, pointsCopy[i].z);
            }
            TracingFirstTime = false;
            for (int i = 0; i < pointsCopy.Length; i++)
            {
                if (i < pointsCopy.Length - 1)
                {
                    float newX = pointsCopy[i + 1].x - pointsCopy[i].x;
                    float newZ = pointsCopy[i + 1].z - pointsCopy[i].z;
                    MapManager.Edge NewEdge = new MapManager.Edge(pointsCopy[i], pointsCopy[i + 1]);
                    Edges.Add(NewEdge);
                }
                else
                {
                    float newX = pointsCopy[0].x - pointsCopy[i].x;
                    float newZ = pointsCopy[0].z - pointsCopy[i].z;
                    MapManager.Edge NewEdge = new MapManager.Edge(pointsCopy[i], pointsCopy[0]);
                    Edges.Add(NewEdge);
                }
            }
            temp = new List<Vector3>[Edges.Count];
            for (int i = 0; i < temp.Length; i++)
            {
                temp[i] = new List<Vector3>();
                temp[i].AddRange(HelperFunctions.AvailablePoints(boundary, Edges[i], radius, 0.1f, out IsEnd));
            }

            //foreach (MapManager.Edge _edge in Edges)
            //{
            //    temp.AddRange(HelperFunctions.AvailablePoints(boundary, _edge, radius, 0.1f, out IsEnd));
            //}

            List<HelperFunctions.BoundaryPoint> boundaryPointsNext = new List<HelperFunctions.BoundaryPoint>();
            bool first = true;
            for (int i = 0; i < boundary.Length; i++)
            {
                if (first)
                {
                    first = false;
                    boundaryPointsNext.Add(new HelperFunctions.BoundaryPoint(boundary[i], radius, Edges[Edges.Count - 1].UnitDirectionVector, Edges[0].UnitDirectionVector, boundary));
                }
                else
                {
                    boundaryPointsNext.Add(new HelperFunctions.BoundaryPoint(boundary[i], radius, Edges[i - 1].UnitDirectionVector, Edges[i].UnitDirectionVector, boundary));
                }
            }

            Vector3[] boundaryNext = new Vector3[boundary.Length];
            for (int i = 0; i < boundary.Length; i++)
            {
                boundaryNext[i] = boundaryPointsNext[i].nextPos;
            }

            int index;
            Vector3 lastPoint = new Vector3(0, 0, 0);
            for (int i = 0; i < temp.Length; i++)
            {
                List<GameObject> NavEdgeGO = new List<GameObject>();
                foreach (Vector3 point in temp[i])
                {
                    if (MapManager.IsInBoundary(point, boundaryNext, true) && !(System.Math.Abs(point.x - -6.415565f) < Mathf.Epsilon && System.Math.Abs(point.z - -0.889834f) < Mathf.Epsilon))
                    {
                        NavEdgeGO.Add(Instantiate(TracingCube, point, Quaternion.identity));
                        lastPoint = point;
                    }
                }
                if (i < temp.Length - 1) index = i + 1;
                else index = 0;
                MapManager.Edge helper = new MapManager.Edge(boundary[index], lastPoint);
                Vector3 ToAdd = boundary[index] + (helper.UnitDirectionVector * (radius - 0.1f));
                TracingCubes.Add(new NavmeshEdge(NavEdgeGO, ToAdd));
            }
        }

        else
        {
            while (OneTime)
            {
                List<HelperFunctions.BoundaryPoint> boundaryPoints = new List<HelperFunctions.BoundaryPoint>();
                bool first = true;
                if (FirstTrace)
                {
                    for (int i = 0; i < boundary.Length; i++)
                    {
                        if (first)
                        {
                            first = false;
                            boundaryPoints.Add(new HelperFunctions.BoundaryPoint(boundary[i], radius, Edges[Edges.Count - 1].UnitDirectionVector, Edges[0].UnitDirectionVector, boundary));
                        }
                        else
                        {
                            boundaryPoints.Add(new HelperFunctions.BoundaryPoint(boundary[i], radius, Edges[i - 1].UnitDirectionVector, Edges[i].UnitDirectionVector, boundary));
                        }
                    }
                }

                else
                {
                    for (int i = 0; i < boundary.Length; i++)
                    {
                        if (first)
                        {
                            first = false;
                            boundaryPoints.Add(new HelperFunctions.BoundaryPoint(boundary[i], diameter, Edges[Edges.Count - 1].UnitDirectionVector, Edges[0].UnitDirectionVector, boundary));
                        }
                        else
                        {
                            boundaryPoints.Add(new HelperFunctions.BoundaryPoint(boundary[i], diameter, Edges[i - 1].UnitDirectionVector, Edges[i].UnitDirectionVector, boundary));
                        }
                    }
                }

                if (!TracingPointsDecrement)
                {
                    for (int i = 0; i < boundary.Length; i++)
                    {
                        boundary[i] = boundaryPoints[i].nextPos;
                    }
                }

                else
                {
                    if (decDatas.Count > boundary.Length)
                    {
                        TracingDone = true;
                        break;
                    }
                    boundary = new Vector3[boundary.Length - decDatas.Count];
                    int j = 0;
                    for (int i = 0; i < boundary.Length; i++)
                    {
                        if (decDatas.Count != 0)
                        {
                            if (i < decDatas[0].index)
                            {
                                boundary[i] = boundaryPoints[j].nextPos;
                                j++;
                            }

                            else
                            {
                                boundary[i] = decDatas[0].NewVertex;
                                j += 2;
                                decDatas.Remove(decDatas[0]);
                            }
                        }
                        else
                        {
                            boundary[i] = boundaryPoints[j].nextPos;
                            j++;
                        }
                    }
                    decDatas = new List<PointsDecData>();
                    TracingPointsDecrement = false;
                }

                Edges = new List<MapManager.Edge>();
                for (int i = 0; i < boundary.Length; i++)
                {
                    Edges.Add(new MapManager.Edge());
                }

                for (int i = 0; i < boundary.Length; i++)
                {
                    if (i < boundary.Length - 1)
                    {
                        Edges[i] = new MapManager.Edge(boundary[i], boundary[i + 1]);
                    }

                    else
                    {
                        Edges[i] = new MapManager.Edge(boundary[i], boundary[0]);
                    }
                }

                temp = new List<Vector3>[Edges.Count];
                for (int i = 0; i < temp.Length; i++)
                {
                    temp[i] = new List<Vector3>();
                    temp[i].AddRange(HelperFunctions.AvailablePoints(boundary, Edges[i], diameter, 0.1f, out IsEnd));
                }

                List<HelperFunctions.BoundaryPoint> boundaryPointsNext = new List<HelperFunctions.BoundaryPoint>();
                bool firstNext = true;

                for (int i = 0; i < boundary.Length; i++)
                {
                    if (firstNext)
                    {
                        firstNext = false;
                        boundaryPointsNext.Add(new HelperFunctions.BoundaryPoint(boundary[i], diameter, Edges[Edges.Count - 1].UnitDirectionVector, Edges[0].UnitDirectionVector, boundary));
                    }
                    else
                    {
                        boundaryPointsNext.Add(new HelperFunctions.BoundaryPoint(boundary[i], diameter, Edges[i - 1].UnitDirectionVector, Edges[i].UnitDirectionVector, boundary));
                    }
                }

                Vector3[] nextBoundary = new Vector3[boundary.Length];

                for (int i = 0; i < boundary.Length; i++)
                {
                    nextBoundary[i] = boundaryPointsNext[i].nextPos;
                }

                for (int i = 0; i < nextBoundary.Length; i++)
                {
                    if (i < nextBoundary.Length - 1)
                    {
                        MapManager.Edge ToCheck = new MapManager.Edge(nextBoundary[i], nextBoundary[i + 1]);
                        if (System.Math.Abs(ToCheck.length) < Mathf.Epsilon)
                        {
                            TracingDone = true;
                            break;
                        }
                        if (ToCheck.direction != Edges[i].direction)
                        {
                            TracingPointsDecrement = true;
                            decDatas.Add(new PointsDecData(i, nextBoundary));
                        }
                    }
                    else
                    {
                        MapManager.Edge ToCheck = new MapManager.Edge(nextBoundary[i], nextBoundary[0]);
                        if (System.Math.Abs(ToCheck.length) < Mathf.Epsilon)
                        {
                            TracingDone = true;
                            break;
                        }
                        if (new MapManager.Edge(nextBoundary[i], nextBoundary[0]).direction != Edges[i].direction)
                        {
                            TracingPointsDecrement = true;
                            decDatas.Add(new PointsDecData(i, nextBoundary));
                        }
                    }
                }

                if (TracingPointsDecrement)
                {
                    List<PointsDecData> copy = new List<PointsDecData>();
                    foreach (PointsDecData d in decDatas)
                    {
                        copy.Add(new PointsDecData(d));
                    }
                    Vector3[] tempNext = new Vector3[nextBoundary.Length];
                    for (int i = 0; i < tempNext.Length; i++)
                    {
                        tempNext[i] = new Vector3(nextBoundary[i].x, 0f, nextBoundary[i].z);
                    }
                    nextBoundary = new Vector3[tempNext.Length - decDatas.Count];
                    int j = 0;
                    for (int i = 0; i < nextBoundary.Length; i++)
                    {
                        if (decDatas.Count != 0)
                        {
                            if (i < decDatas[0].index)
                            {
                                nextBoundary[i] = tempNext[j];
                                j++;
                            }

                            else
                            {
                                nextBoundary[i] = decDatas[0].NewVertex;
                                j += 2;
                                decDatas.Remove(decDatas[0]);
                            }
                        }
                        else
                        {
                            nextBoundary[i] = tempNext[j];
                            j++;
                        }
                    }
                    foreach (PointsDecData d in copy)
                    {
                        decDatas.Add(new PointsDecData(d));
                    }
                }




                //foreach (Vector3 point in temp)
                //{
                //    if (MapManager.IsInBoundary(point, nextBoundary, true))
                //    {
                //        TracingDes.Add(point);
                //    }
                //}
                int index;
                Vector3 lastPoint = new Vector3(0, 0, 0);
                for (int i = 0; i < temp.Length; i++)
                {
                    List<GameObject> NavEdgeGO = new List<GameObject>();
                    foreach (Vector3 point in temp[i])
                    {
                        if (MapManager.IsInBoundary(point, nextBoundary, true))
                        {
                            NavEdgeGO.Add(Instantiate(TracingCube, point, Quaternion.identity));
                            lastPoint = point;
                        }
                    }
                    if (i < temp.Length - 1) index = i + 1;
                    else index = 0;
                    MapManager.Edge helper = new MapManager.Edge(boundary[index], lastPoint);
                    Vector3 ToAdd = boundary[index] +(helper.UnitDirectionVector * radius);
                    TracingCubes.Add(new NavmeshEdge(NavEdgeGO, ToAdd));
                }

                //times++;
                //bool done = true;
                //foreach(HelperFunctions.BoundaryPoint boundaryPoint in boundaryPoints)
                //{
                //    if (!boundaryPoint.done) done = false;
                //}
                //if (done)
                //{
                //    OneTime = false;
                //    break;
                //}

                float shortest = Mathf.Infinity;
                foreach (MapManager.Edge edge in Edges)
                {
                    if (edge.length < shortest) shortest = edge.length;
                }
                TracingCircleNum++;
                if (TracingCircleNum >= MapData.GetCircleNums(mapManager.GetMapType())[ConvexIndex])
                {
                    OneTime = false;
                    break;
                }

                FirstTrace = false;
            }
            TracingDone = true;
        }
    }

    private float GetWidth(Vector3[] boundary)
    {
        Vector3 top = mapManager.FindMostTopPoint(boundary);
        Vector3 bottom = mapManager.FindMostBottomPoint(boundary);
        Vector3 left = mapManager.FindMostLeftPoint(boundary);
        Vector3 right = mapManager.FindMostRightPoint(boundary);
        float a = Mathf.Abs(top.z - bottom.z);
        float b = Mathf.Abs(right.x - left.x);
        if (a < b) return a;
        return b;
    }

    private bool IsArrived(Vector3 postion, Vector3 destination)
    {
        if (HelperFunctions.GetDistanceBetweenTwoVector3(postion, destination) < 0.1f)
            return true;
        return false;
    }
}
#endregion