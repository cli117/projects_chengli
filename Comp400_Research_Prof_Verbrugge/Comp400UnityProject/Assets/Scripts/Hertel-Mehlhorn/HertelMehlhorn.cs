﻿using UnityEngine;
using System;
using System.Collections.Generic;

public static class HertelMehlhorn
{
    public static List<Polygon> MergeTriangles(List<Triangle> triangles)
    {
        List<Polygon> TPolygon = new List<Polygon>();
        foreach (Triangle t in triangles)
        {
            Polygon temp = new Polygon(t.GetVertices(), true);
            temp = ReverseIfNotCounterClockWise(temp);
            TPolygon.Add(temp);
        }

        return MergePolygons(TPolygon);
    }

    public static List<Polygon> MergePolygons(List<Polygon> polygons)
    {
        Recursion:
        PolygonPair ToMerge = FindCanMerge(polygons);
        if (ToMerge != null)
        {
            Polygon ToAdd = ToMerge.Merge();
            polygons.Remove(ToMerge.p1);
            polygons.Remove(ToMerge.p2);
            polygons.Add(ToAdd);
            goto Recursion;
        }

        return polygons;
    }

    public static Polygon ReverseIfNotCounterClockWise(Polygon polygon)
    {
        float indicator = 0;
        foreach (PolygonEdge edge in polygon.GetEdges())
        {
            indicator += (edge.GetQ().x - edge.GetP().x) * (edge.GetQ().z + edge.GetP().z);
        }
        if (indicator < 0) return polygon;

        LinkedList<PolygonVertex> temp = polygon.GetVertices();
        LinkedListNode<PolygonVertex> tempCurrent = temp.First;
        List<Vector3> cons = new List<Vector3>();
        while (tempCurrent.Next != null)
        {
            cons.Add(tempCurrent.Value.GetPosition());
            tempCurrent = tempCurrent.Next;
        }
        cons.Add(tempCurrent.Value.GetPosition());
        cons.Reverse();
        return new Polygon(cons, true);
    }

    public static Polygon ReverseIfNotClockWise(Polygon polygon)
    {
        float indicator = 0;
        foreach (PolygonEdge edge in polygon.GetEdges())
        {
            indicator += (edge.GetQ().x - edge.GetP().x) * (edge.GetQ().z + edge.GetP().z);
        }
        if (indicator >= 0) return polygon;

        LinkedList<PolygonVertex> temp = polygon.GetVertices();
        LinkedListNode<PolygonVertex> tempCurrent = temp.First;
        List<Vector3> cons = new List<Vector3>();
        while (tempCurrent.Next != null)
        {
            cons.Add(tempCurrent.Value.GetPosition());
            tempCurrent = tempCurrent.Next;
        }
        cons.Add(tempCurrent.Value.GetPosition());
        cons.Reverse();
        return new Polygon(cons, true);
    }

    public static PolygonPair FindCanMerge(List<Polygon> polygons)
    {
        foreach (Polygon p1 in polygons)
        {
            foreach (Polygon p2 in polygons)
            {
                if (!p1.IsSamePolygon(p2))
                {
                    PolygonPair ToTest = new PolygonPair(p1, p2);
                    if (ToTest.CanMerge)
                    {
                        return ToTest;
                    } 
                }
            }
        }
        return null;
    }
}