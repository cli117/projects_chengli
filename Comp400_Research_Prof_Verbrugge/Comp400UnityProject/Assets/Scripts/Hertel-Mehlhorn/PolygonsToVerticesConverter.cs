﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;

public struct PointWithAngle
{
    public Vector3 position;
    public float angle;

    public PointWithAngle(Vector3 position, float angle)
    {
        this.position = position;
        this.angle = angle;
    }
}

public static class PolygonsToVerticesConverter
{
    private static List<PolygonEdge> MergeEdges(Polygon p1, Polygon p2, PolygonEdge CommonEdge)
    {
        List<PolygonEdge> ToReturn = new List<PolygonEdge>();
        foreach (PolygonEdge e in p1.GetEdges())
        {
            if (!e.IsSameEdge(CommonEdge))
            {
                ToReturn.Add(e);
            }
        }
        foreach (PolygonEdge e in p2.GetEdges())
        {
            if (!e.IsSameEdge(CommonEdge))
            {
                ToReturn.Add(e);
            }
        }
        return ToReturn;
    }

    private static List<Vector3> GetPoints(List<PolygonEdge> edges)
    {
        List<Vector3> ToReturn = new List<Vector3>();
        foreach (PolygonEdge edge in edges)
        {
            if (!ToReturn.Contains(edge.GetP())) ToReturn.Add(edge.GetP());
            if (!ToReturn.Contains(edge.GetQ())) ToReturn.Add(edge.GetQ());
        }
        return ToReturn;
    }

    private static Vector3 GetCentroid(List<Vector3> boundaryWOOrder)
    {
        float xSum = 0, zSum = 0;
        foreach (Vector3 v in boundaryWOOrder)
        {
            xSum += v.x;
            zSum += v.z;
        }
        return new Vector3(xSum / boundaryWOOrder.Count, 0f, zSum / boundaryWOOrder.Count);
    }

    private static List<Vector3> SortPoints(List<Vector3> boundaryWOOder)
    {
        List<Vector3> ToReturn = new List<Vector3>();
        Vector3 centroid = GetCentroid(boundaryWOOder);
        Vector3 standard = new Vector3(0, 0, 1);
        Vector3[] VectorsLog = new Vector3[boundaryWOOder.Count];
        for (int i = 0; i < VectorsLog.Length; i++)
        {
            VectorsLog[i] = new Vector3(boundaryWOOder[i].x - centroid.x, 0f, boundaryWOOder[i].z - centroid.z);
        }
        List<PointWithAngle> pas = new List<PointWithAngle>();
        for (int i = 0; i < VectorsLog.Length; i++)
        {
            float angle = GetVectorBetweenTwoVector0To360(standard, VectorsLog[i]);
            pas.Add(new PointWithAngle(boundaryWOOder[i], angle));
        }
        pas.Sort((x, y) => x.angle.CompareTo(y.angle));
        for (int i = 0; i < pas.Count; i++)
        {
            ToReturn.Add(pas[i].position);
        }
        return ToReturn;
    }

    private static float GetVectorBetweenTwoVector0To360(Vector3 a, Vector3 b)
    {
        float angle = Mathf.Atan2(a.z, a.x) - Mathf.Atan2(b.z, b.x);
        angle *= Mathf.Rad2Deg;
        if (angle < 0)
        {
            angle += 360;
        }
        return angle;
    }

    public static List<Vector3> ObtainNewBoundary(Polygon p1, Polygon p2, PolygonEdge CommonEdge)
    {
        List<PolygonEdge> edges = MergeEdges(p1, p2, CommonEdge);
        List<Vector3> verticeswoorder = GetPoints(edges);
        return SortPoints(verticeswoorder);
    }
}