﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class PolygonPair
{
    public Polygon p1;
    public Polygon p2;
    public bool IsPaired;
    public PolygonEdge CommonEdge;
    public bool CanMerge;


    public PolygonPair(Polygon p1, Polygon p2)
    {
        this.p1 = p1;
        this.p2 = p2;
        IsPaired = false;
        foreach (PolygonEdge e1 in p1.GetEdges())
        {
            foreach (PolygonEdge e2 in p2.GetEdges())
            {
                if (e1.IsSameEdge(e2))
                {
                    IsPaired = true;
                    CommonEdge = e1;
                }
            }
        }
        if (CommonEdge == null)
        {
            CanMerge = false;
        }
        else if (Merge().IsConvex)
        {
            CanMerge = true;
        }
        else
        {
            CanMerge = false;
        }
    }

    public Polygon Merge()
    {
        List<Vector3> NewBoundary = PolygonsToVerticesConverter.ObtainNewBoundary(p1, p2, CommonEdge);
        Polygon newPolygon = new Polygon(NewBoundary, true);
        newPolygon = HertelMehlhorn.ReverseIfNotCounterClockWise(newPolygon);
        return newPolygon;
    }
}
