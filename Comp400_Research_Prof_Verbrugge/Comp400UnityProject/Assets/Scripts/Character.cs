﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    [SerializeField] GameObject FieldOfView;
    new Rigidbody rigidbody;
    // Start is called before the first frame update
    void Awake()
    {
        rigidbody = GetComponent<Rigidbody>();
        GameObject FOV = Instantiate(FieldOfView, transform.position, Quaternion.identity);
        FOV.transform.parent = transform;
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.A))
        {
            rigidbody.AddForce(Vector3.left * 5);
        }
        if (Input.GetKey(KeyCode.D))
        {
            rigidbody.AddForce(Vector3.right * 5);
        }
        if (Input.GetKey(KeyCode.W))
        {
            rigidbody.AddForce(Vector3.forward * 5);
        }
        if (Input.GetKey(KeyCode.S))
        {
            rigidbody.AddForce(Vector3.back * 5);
        }
    }
}
