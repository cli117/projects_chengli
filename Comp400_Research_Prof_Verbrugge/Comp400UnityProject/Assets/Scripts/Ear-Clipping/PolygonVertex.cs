﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class PolygonVertex
{
    Vector3 position;
    bool IsReflexAttri;
    public PolygonVertex(Vector3 a, Vector3 b, Vector3 c)
    {
        // b is self
        position = b;
        IsReflexAttri = !((b.x - a.x) * (c.z - b.z) - (c.x - b.x) * (b.z - a.z) > 0);
    }

    public Vector3 GetPosition()
    {
        return position;
    }

    public bool IsReflex()
    {
        return IsReflexAttri;
    }

    public bool IsSameVertex(PolygonVertex other)
    {
        if (position.Equals(other.position))
        {
            return true;
        }
        return false;
    }

    public bool IsSameVertex(PolygonVertex other, ref Vector3 vertexLog)
    {
        if (position.Equals(other.position))
        {
            vertexLog = position;
            return true;
        }
        return false;
    }

    public bool IsOnPosition(Vector3 pos, ref Vector3 posLog)
    {
        if (position.Equals(pos))
        {
            posLog = position;
            return true;
        }
        return false;
    }
}