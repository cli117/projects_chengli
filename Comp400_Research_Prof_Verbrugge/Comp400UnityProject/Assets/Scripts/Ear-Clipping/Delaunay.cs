﻿using UnityEngine;
using System;
using System.Collections.Generic;

public static class Delaunay
{
    public static List<Triangle> DelaunayTriangles(List<Triangle> triangles)
    {
        Recursion:
        List<DelaunayPair> pairs = new List<DelaunayPair>();
        foreach (Triangle t0 in triangles)
        {
            foreach (Triangle t1 in triangles)
            {
                DelaunayPair ToTest = new DelaunayPair(t0, t1);
                if (ToTest.IsPaired)
                {
                    bool valid = true;
                    foreach (DelaunayPair pair in pairs)
                    {
                        valid &= !pair.Same(ToTest);
                    }
                    if (valid && ToTest.IsValid())
                    {
                        pairs.Add(ToTest);
                    }
                }
            }
        }

        //foreach (DelaunayPair pair in pairs)
        //{
        //    Debug.Log(pair.ToString());
        //}

    FailedFlip:
        int index = IsAllDelaunay(pairs);
        if (index != -1)
        {
            List<PolygonEdge> AllEdges = new List<PolygonEdge>();
            foreach (Triangle triangle in triangles)
            {
                foreach (PolygonEdge edge in triangle.GetEdges())
                {
                    AllEdges.Add(edge);
                }
            }

            DelaunayPair current = pairs[index];
            Vector3 ThirdOfOne = DelaunayPair.GetThirdPoint(current.t1, current.CommonEdge);
            Vector3 ThirdOfTwo = DelaunayPair.GetThirdPoint(current.t2, current.CommonEdge);
            PolygonEdge newAdd = new PolygonEdge(ThirdOfOne, ThirdOfTwo);
            foreach (PolygonEdge edge in AllEdges)
            {
                if (edge.Intersects(newAdd) && !edge.IsSameEdge(current.CommonEdge))
                {
                    current.IsDelaunay = true;
                    goto FailedFlip;
                }
            }

            triangles.Remove(current.t1);
            triangles.Remove(current.t2);
            Triangle new1 = new Triangle(current.CommonEdge.GetP(), ThirdOfOne, ThirdOfTwo);
            Triangle new2 = new Triangle(current.CommonEdge.GetQ(), ThirdOfOne, ThirdOfTwo);
            triangles.Add(new1);
            triangles.Add(new2);
            goto Recursion;
        }

        return triangles;
    }

    public static int IsAllDelaunay(List<DelaunayPair> pairs)
    {
        for (int i = 0; i < pairs.Count; i++)
        {
            if (!pairs[i].IsDelaunay)
            {
                return i;
            }
        }
        return -1;
    }
}