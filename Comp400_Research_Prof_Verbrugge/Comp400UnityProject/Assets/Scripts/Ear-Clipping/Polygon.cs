﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class Polygon
{
    LinkedList<PolygonVertex> Vertices;
    List<PolygonVertex> ReflexVertices;
    List<PolygonEdge> edges; 
    bool IsOuter;
    public bool IsConvex;
    public Vector3 centroid;

    // User do not need to care about the clock-wise and counter-clockwise thing
    // All Vector3[] should have the same order
    public Polygon(Vector3[] VerticesArray, bool IsOuter)
    {
        Vertices = new LinkedList<PolygonVertex>();
        edges = new List<PolygonEdge>();
        ReflexVertices = new List<PolygonVertex>();

        this.IsOuter = IsOuter;
        if (IsOuter)
        {
            for (int i = 0; i < VerticesArray.Length; i++)
            {
                int prev = i - 1, next = i + 1;
                if (i == 0) prev = VerticesArray.Length - 1;
                else if (i == VerticesArray.Length - 1) next = 0;

                Vertices.AddLast(new PolygonVertex(VerticesArray[prev], VerticesArray[i], VerticesArray[next]));
            }
        }
        else
        {
            for (int i = VerticesArray.Length - 1; i >= 0; i--)
            {
                int prev = i + 1, next = i - 1;
                if (i == 0) next = VerticesArray.Length - 1;
                else if (i == VerticesArray.Length - 1) prev = 0;

                Vertices.AddLast(new PolygonVertex(VerticesArray[prev], VerticesArray[i], VerticesArray[next]));
            }
        }

        LinkedListNode<PolygonVertex> current = Vertices.First;
        for (int i = 0; i < Vertices.Count; i++)
        {
            if (i < Vertices.Count - 1)
            {
                edges.Add(new PolygonEdge(current.Value.GetPosition(), current.Next.Value.GetPosition()));
                current = current.Next;
            }
            else
            {
                edges.Add(new PolygonEdge(current.Value.GetPosition(), Vertices.First.Value.GetPosition()));
            }
        }

        float SumX = 0, SumZ = 0;
        foreach(PolygonVertex polygonVertex in Vertices)
        {
            SumX += polygonVertex.GetPosition().x;
            SumZ += polygonVertex.GetPosition().z;

            if (polygonVertex.IsReflex() && IsOuter)
            {
                ReflexVertices.Add(polygonVertex);
            }

            else if (!polygonVertex.IsReflex() && !IsOuter)
            {
                ReflexVertices.Add(polygonVertex);
            }
        }

        IsConvex = ReflexVertices.Count == 0;
        centroid = new Vector3(SumX / Vertices.Count, 0, SumZ / Vertices.Count);
    }

    public Polygon(List<Vector3> VerticesList, bool IsOuter)
    {
        Vertices = new LinkedList<PolygonVertex>();
        edges = new List<PolygonEdge>();
        ReflexVertices = new List<PolygonVertex>();

        this.IsOuter = IsOuter;
        if (IsOuter)
        {
            for (int i = 0; i < VerticesList.Count; i++)
            {
                int prev = i - 1, next = i + 1;
                if (i == 0) prev = VerticesList.Count - 1;
                else if (i == VerticesList.Count - 1) next = 0;

                Vertices.AddLast(new PolygonVertex(VerticesList[prev], VerticesList[i], VerticesList[next]));
            }
        }
        else
        {
            for (int i = VerticesList.Count - 1; i >= 0; i--)
            {
                int prev = i + 1, next = i - 1;
                if (i == 0) next = VerticesList.Count - 1;
                else if (i == VerticesList.Count - 1) prev = 0;

                Vertices.AddLast(new PolygonVertex(VerticesList[prev], VerticesList[i], VerticesList[next]));
            }
        }

        LinkedListNode<PolygonVertex> current = Vertices.First;
        for (int i = 0; i < Vertices.Count; i++)
        {
            if (i < Vertices.Count - 1)
            {
                edges.Add(new PolygonEdge(current.Value.GetPosition(), current.Next.Value.GetPosition()));
                current = current.Next;
            }
            else
            {
                edges.Add(new PolygonEdge(current.Value.GetPosition(), Vertices.First.Value.GetPosition()));
            }
        }

        float SumX = 0, SumZ = 0;
        foreach (PolygonVertex polygonVertex in Vertices)
        {
            SumX += polygonVertex.GetPosition().x;
            SumZ += polygonVertex.GetPosition().z;

            if (polygonVertex.IsReflex() && IsOuter)
            {
                ReflexVertices.Add(polygonVertex);
            }

            else if (!polygonVertex.IsReflex() && !IsOuter)
            {
                ReflexVertices.Add(polygonVertex);
            }
        }

        IsConvex = ReflexVertices.Count == 0;
        centroid = new Vector3(SumX / Vertices.Count, 0, SumZ / Vertices.Count);
    }

    public Polygon(LinkedList<PolygonVertex> polygonVertices, bool IsOuter)
    {
        Vector3[] VerticesArray = new Vector3[polygonVertices.Count];
        LinkedListNode<PolygonVertex> pv = polygonVertices.First;
        for (int i = 0; i < VerticesArray.Length; i++)
        {
            VerticesArray[i] = pv.Value.GetPosition();
            pv = pv.Next;
        }
        Vertices = new LinkedList<PolygonVertex>();
        edges = new List<PolygonEdge>();
        ReflexVertices = new List<PolygonVertex>();

        this.IsOuter = IsOuter;
        if (IsOuter)
        {
            for (int i = 0; i < VerticesArray.Length; i++)
            {
                int prev = i - 1, next = i + 1;
                if (i == 0) prev = VerticesArray.Length - 1;
                else if (i == VerticesArray.Length - 1) next = 0;

                Vertices.AddLast(new PolygonVertex(VerticesArray[prev], VerticesArray[i], VerticesArray[next]));
            }
        }
        else
        {
            for (int i = VerticesArray.Length - 1; i >= 0; i--)
            {
                int prev = i + 1, next = i - 1;
                if (i == 0) next = VerticesArray.Length - 1;
                else if (i == VerticesArray.Length - 1) prev = 0;

                Vertices.AddLast(new PolygonVertex(VerticesArray[prev], VerticesArray[i], VerticesArray[next]));
            }
        }

        LinkedListNode<PolygonVertex> current = Vertices.First;
        for (int i = 0; i < Vertices.Count; i++)
        {
            if (i < Vertices.Count - 1)
            {
                edges.Add(new PolygonEdge(current.Value.GetPosition(), current.Next.Value.GetPosition()));
                current = current.Next;
            }
            else
            {
                edges.Add(new PolygonEdge(current.Value.GetPosition(), Vertices.First.Value.GetPosition()));
            }
        }

        float SumX = 0, SumZ = 0;
        foreach (PolygonVertex polygonVertex in Vertices)
        {
            SumX += polygonVertex.GetPosition().x;
            SumZ += polygonVertex.GetPosition().z;

            if (polygonVertex.IsReflex() && IsOuter)
            {
                ReflexVertices.Add(polygonVertex);
            }

            else if (!polygonVertex.IsReflex() && !IsOuter)
            {
                ReflexVertices.Add(polygonVertex);
            }
        }

        IsConvex = ReflexVertices.Count == 0;
        centroid = new Vector3(SumX / Vertices.Count, 0, SumZ / Vertices.Count);
    }

    public LinkedList<PolygonVertex> GetVertices()
    {
        return Vertices;
    }

    public List<PolygonVertex> GetReflexVertices()
    {
        return ReflexVertices;
    }

    public bool IsOuterPolygon()
    {
        return IsOuter;
    }

    public Vector3 PointMaxX()
    {
        float MaxXValue = Mathf.NegativeInfinity;
        // 3 place holder
        PolygonVertex MaxX = new PolygonVertex(new Vector3(), new Vector3(), new Vector3());
        foreach (PolygonVertex point in Vertices)
        {
            if (point.GetPosition().x > MaxXValue)
            {
                MaxXValue = point.GetPosition().x;
                MaxX = point;
            }
        }
        return MaxX.GetPosition();
    }

    public List<PolygonEdge> GetEdges()
    {
        return edges;
    }

    public bool IsSamePolygon(Polygon other)
    {
        int Count = 0;
        foreach (PolygonVertex v1 in Vertices)
        {
            foreach (PolygonVertex v2 in other.GetVertices())
            {
                if (v1.IsSameVertex(v2))
                {
                    Count++;
                }
            }
        }
        return (Count == Vertices.Count) && (Count == other.GetVertices().Count);
    }
    public Vector3[] GetVerticesAsVecter3s()
    {
        Vector3[] ToReturn = new Vector3[Vertices.Count];
        LinkedListNode<PolygonVertex> currentNode = Vertices.First;
        for (int i = 0; i < ToReturn.Length; i++)
        {
            ToReturn[i] = currentNode.Value.GetPosition();
            currentNode = currentNode.Next;
        }
        return ToReturn;
    }

    override
    public string ToString()
    {
        string ToReturn = "";
        LinkedListNode<PolygonVertex> currentNode = Vertices.First;
        for (int i = 0; i < ToReturn.Length; i++)
        {
            ToReturn += currentNode.Value.GetPosition().ToString();
            currentNode = currentNode.Next;
        }
        return ToReturn;
    }
}

