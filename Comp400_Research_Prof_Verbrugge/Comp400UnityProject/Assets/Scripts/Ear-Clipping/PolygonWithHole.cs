﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class PolygonWithHole
{
    Polygon OuterPolygon;
    //Holes
    List<Polygon> InnerPolygons;

    public PolygonWithHole(Polygon outer, List<Polygon> Inner)
    {
        OuterPolygon = outer;
        InnerPolygons = Inner;
    }

    public Polygon GetOuter()
    {
        return OuterPolygon;
    }

    public List<Polygon> GetInners()
    {
        return InnerPolygons;
    }
}

