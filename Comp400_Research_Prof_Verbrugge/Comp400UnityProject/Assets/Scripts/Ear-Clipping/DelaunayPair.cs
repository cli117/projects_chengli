﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class DelaunayPair
{
    public Triangle t1;
    public Triangle t2;
    public PolygonEdge CommonEdge;
    public bool IsPaired;
    public bool IsDelaunay;

    public DelaunayPair(Triangle a, Triangle b)
    {
        t1 = a;
        t2 = b;

        foreach (PolygonEdge aEdge in a.GetEdges())
        {
            foreach (PolygonEdge bEdge in b.GetEdges())
            {
                if (aEdge.IsSameEdge(bEdge))
                {
                    CommonEdge = aEdge;
                }
            }
        }

        if (CommonEdge != null && !a.Same(b))
        {
            IsPaired = true;
        }
        else
        {
            IsPaired = false;
        }
        IsDelaunay = IsDelaunayMethod();
    }

    public bool IsDelaunayMethod()
    {
        if (!IsPaired)
        {
            return false;
        }

        float tolerace = 0.1f;
        if ((GetDegree(t1, CommonEdge) + GetDegree(t2, CommonEdge)) >= 180f + tolerace)
        {
            return false;
        }
        return true;
    }


    public bool Same(DelaunayPair other)
    {
        if (other.t1.Same(this.t1) && other.t2.Same(this.t2)) return true;
        else if (other.t1.Same(this.t2) && other.t2.Same(this.t1)) return true;
        else return false;
    }

    public static float GetDegree(Triangle t, PolygonEdge CommonEdge)
    {
        List<PolygonEdge> TwoEdges = new List<PolygonEdge>();
        foreach (PolygonEdge e in t.GetEdges())
        {
            if (!e.IsSameEdge(CommonEdge)) TwoEdges.Add(e);
        }

        PolygonEdge first = TwoEdges[0];
        PolygonEdge second = TwoEdges[1];
        float radius = Mathf.Acos((Mathf.Pow(LenthOfAnEdge(first), 2) + Mathf.Pow(LenthOfAnEdge(second), 2) - Mathf.Pow(LenthOfAnEdge(CommonEdge), 2)) / (2 * LenthOfAnEdge(first) * LenthOfAnEdge(second)));
        return radius * Mathf.Rad2Deg;
    }

    public static float LenthOfAnEdge(PolygonEdge a)
    {
        float xDifference = Mathf.Abs(a.GetP().x - a.GetQ().x);
        float yDifference = Mathf.Abs(a.GetP().z - a.GetQ().z);
        return Mathf.Sqrt(Mathf.Pow(xDifference, 2) + Mathf.Pow(yDifference, 2));
    }

    public static Vector3 GetThirdPoint(Triangle triangle, PolygonEdge CommonEdge)
    {
        Vector3 ToReturn = new Vector3();
        foreach (Vector3 p in triangle.GetVertices())
        {
            if (!p.Equals(CommonEdge.GetP()) && !p.Equals(CommonEdge.GetQ()))
            {
                ToReturn = p;
            }
        }
        return ToReturn;
    }

    public bool IsValid()
    {
        Vector3 ThirdOfOne = GetThirdPoint(t1, CommonEdge);
        Vector3 ThirdOfTwo = GetThirdPoint(t2, CommonEdge);
        PolygonEdge newEdge = new PolygonEdge(ThirdOfOne, ThirdOfTwo);
        if (newEdge.Intersects(CommonEdge)) return true;
        return false;
    }

    override
    public string ToString()
    {
        string ToReturn = "t1: " + t1.ToString();
        ToReturn += "t2: " + t2.ToString();
        ToReturn += IsDelaunay.ToString();
        return ToReturn;
    }
}