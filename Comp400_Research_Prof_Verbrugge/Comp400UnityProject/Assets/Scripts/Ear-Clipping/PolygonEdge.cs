﻿using UnityEngine;
using System;
using System.Collections.Generic;


public class PolygonEdge
{
    Vector3 p;
    Vector3 q;
    Vector3 Direction;
    float k;
    float b;
    float length;
    public PolygonEdge(Vector3 p, Vector3 q)
    {
        this.p = p;
        this.q = q;
        float xDiff = Mathf.Abs(p.x - q.x);
        float zDiff = Mathf.Abs(p.z - q.z);
        length = Mathf.Sqrt(Mathf.Pow(xDiff, 2) + Mathf.Pow(zDiff, 2));
        Direction = q - p;
        k = (q.z - p.z) / (q.x - p.x);
        b = p.z - (k * p.x);
    }

    public bool IsIntersectXRay(Vector3 StartPoint, ref Vector3 IntersctionPoint)
    {
        if ((p.z <= StartPoint.z && StartPoint.z <= q.z) || (q.z <= StartPoint.z && StartPoint.z <= p.z))
        {
            float z = StartPoint.z;
            float x;
            if (float.IsInfinity(k))
            {
                x = p.x;
            }
            else if (Math.Abs(k) < Mathf.Epsilon)
            {
                if (p.x < q.x)
                    x = p.x;
                else
                    x = q.x;
            }
            else
            {
                x = (StartPoint.z - b) / k;
            }
            if (x < StartPoint.x)
                return false;
            IntersctionPoint.x = x;
            IntersctionPoint.z = z;
            return true;
        }
        return false;
    }

    override
    public string ToString()
    {
        string ToReturn = "p: " + p.ToString();
        ToReturn += " q: " + q.ToString();
        return ToReturn;
    }

    public bool Contains(Vector3 point)
    {
        if ((Math.Abs(p.x - point.x) < Mathf.Epsilon && Math.Abs(p.z - point.z) < Mathf.Epsilon) || (Math.Abs(q.x - point.x) < Mathf.Epsilon && Math.Abs(q.z - point.z) < Mathf.Epsilon))
        {
            return true;
        }
        return false;
    }

    public bool IsSameEdge(PolygonEdge other)
    {
        if (other.p.Equals(this.p) && other.q.Equals(this.q)) return true;
        if (other.p.Equals(this.q) && other.q.Equals(this.p)) return true;
        return false;
    }

    public bool Intersects(PolygonEdge e)
    {
        int s1 = sat(p.x, p.z, q.x, q.z, e.p.x, e.p.z);
        int s2 = sat(p.x, p.z, q.x, q.z, e.q.x, e.q.z);
        if (s1 == s2 || (s1 == 0 && s2 != 0) || (s2 == 0 && s1 != 0)) return false;
        s1 = sat(e.p.x, e.p.z, e.q.x, e.q.z, p.x, p.z);
        s2 = sat(e.p.x, e.p.z, e.q.x, e.q.z, q.x, q.z);
        if (s1 == s2 || (s1 == 0 && s2 != 0) || (s2 == 0 && s1 != 0)) return false;
        return true;
    }

    public int sat(float p0x, float p0z, float p1x, float p1z, float p2x, float p2z)
    {
        float d = (p1x - p0x) * (p2z - p0z) - (p2x - p0x) * (p1z - p0z);
        if (d < 0) return -1;
        if (d > 0) return 1;
        return 0;
    }

    public Vector3 GetRightPoint()
    {
        if (p.x > q.x)
        {
            return p;
        }
        return q;
    }

    public bool IsBelongTo(PolygonVertex vertex)
    {
        return vertex.GetPosition().Equals(p) || vertex.GetPosition().Equals(q);
    }

    public Vector3 GetP()
    {
        return p;
    }

    public Vector3 GetQ()
    {
        return q;
    }

    public float GetLength()
    {
        return length;
    }

    public Vector3 GetDirection()
    {
        return Direction;
    }
    
}
