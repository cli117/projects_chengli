﻿using UnityEngine;
using System;
using System.Collections.Generic;

public static class EarClipping
{
    public static List<Triangle> EarClipWithoutHole(Polygon polygon)
    {
        List<Triangle> ToReturn = new List<Triangle>();
    Recursion:
        LinkedList<PolygonVertex> vertices = polygon.GetVertices();
        LinkedListNode<PolygonVertex> current = polygon.GetVertices().First;
        if (vertices.Count == 3)
        {
            ToReturn.Add(new Triangle(current.Value.GetPosition(), current.Next.Value.GetPosition(), current.Next.Next.Value.GetPosition()));
            return ToReturn;
        }
        Triangle ear;
        for (int i = 0; i < vertices.Count; i++)
        {
            if (current.Value.IsReflex())
            {
                current = current.Next;
                continue;
            }
            if (i == 0)
            {
                ear = new Triangle(vertices.Last.Value.GetPosition(), current.Value.GetPosition(), current.Next.Value.GetPosition());
            }
            else if (i == polygon.GetVertices().Count - 1)
            {
                ear = new Triangle(current.Previous.Value.GetPosition(), current.Value.GetPosition(), vertices.First.Value.GetPosition());
            }
            else
            {
                ear = new Triangle(current.Previous.Value.GetPosition(), current.Value.GetPosition(), current.Next.Value.GetPosition());
            }

            bool IsEar = true;
            foreach (PolygonVertex vertex in vertices)
            {
                if (!ear.IsBelong(vertex.GetPosition()) && ear.IsWithin(vertex.GetPosition()))
                {
                    IsEar = false;
                    break;
                }
            }
            if (IsEar)
            {
                vertices.Remove(current.Value);
                ToReturn.Add(ear);
                polygon = new Polygon(vertices, true);
                goto Recursion;
            }
            current = current.Next;
        }

        return ToReturn;
    }

    public static List<Triangle> EarClipWithHole(Polygon outer, List<Polygon> inners)
    {
        Polygon WOHole = GetNewOuter(outer, inners);
        foreach (PolygonVertex pv in WOHole.GetVertices())
        {
//            Debug.Log(pv.GetPosition());
        }
        return EarClipWithoutHole(WOHole);
    }

    private static Polygon GetNewOuter(Polygon outer, List<Polygon> inners)
    {
        NextRound:
        List<Vector3> NewVertices = new List<Vector3>();
        if (inners.Count == 1)
        {
            Polygon obstacle = inners[0];
            NewVertices = CombineInner(outer, obstacle);
        }

        else
        {
            Polygon obstacle = FindMostRight(inners);
            inners.Remove(obstacle);
            NewVertices = CombineInner(outer, obstacle);
            outer = new Polygon(NewVertices, true);
            goto NextRound;
        }

        return new Polygon(NewVertices, true);
    }

    private static float AngleBetweenTwoEdgesDeg(PolygonEdge a, PolygonEdge b)
    {
        Vector3 aDirection = a.GetDirection(), bDirection = b.GetDirection();
        float DotProduct = (aDirection.x * bDirection.x) + (aDirection.z * bDirection.z);
        return Mathf.Rad2Deg * Mathf.Acos(DotProduct / (a.GetLength() * b.GetLength()));
    }

    private static Vector3 NearestVisiblePoint(List<PolygonEdge> SmallestAngleEdges)
    {
        if (SmallestAngleEdges.Count == 1)
        {
            return SmallestAngleEdges[0].GetQ();
        }
        else
        {
            float smallest = Mathf.Infinity;
            int index = -1;
            for (int i = 0; i < SmallestAngleEdges.Count; i++)
            {
                if (SmallestAngleEdges[i].GetLength() < smallest)
                {
                    smallest = SmallestAngleEdges[i].GetLength();
                    index = i;
                }
            }
            return SmallestAngleEdges[index].GetQ();
        }
    }

    private static List<Vector3> CombineInner(Polygon outer, Polygon obstacle)
    {
        bool Onetime = true;
        List<Vector3> NewVertices = new List<Vector3>();
        Vector3 MaxX = obstacle.PointMaxX();
        Vector3 temp = new Vector3();
        Vector3 PointI = new Vector3(Mathf.Infinity, 0, 0);
        PolygonEdge IEdge = null;

        foreach (PolygonEdge edge in outer.GetEdges())
        {
            if (edge.IsIntersectXRay(MaxX, ref temp))
            {
                if (temp.x < PointI.x)
                {
                    PointI.x = temp.x;
                    PointI.z = temp.z;
                    IEdge = edge;
                }
            }
        }

        if (IEdge == null) return null;
        Vector3 VisiblePoint = new Vector3();
        if (!IEdge.Contains(PointI))
        {
            VisiblePoint = IEdge.GetRightPoint();
        }

        Triangle check = new Triangle(MaxX, PointI, VisiblePoint);
        List<PolygonVertex> ToCheckWithin = new List<PolygonVertex>();
        foreach (PolygonVertex vertex in outer.GetReflexVertices())
        {
            if (check.IsWithin(vertex.GetPosition()) && !check.IsBelong(vertex.GetPosition()))
            {
                ToCheckWithin.Add(vertex);
            }
        }

        bool valid = ToCheckWithin.Count == 0;

        if (!valid)
        {
            PolygonEdge MI = new PolygonEdge(MaxX, PointI);
            PolygonEdge[] candidates = new PolygonEdge[ToCheckWithin.Count];
            float smallestAngle = Mathf.Infinity;
            for (int i = 0; i < candidates.Length; i++)
            {
                candidates[i] = new PolygonEdge(MaxX, ToCheckWithin[i].GetPosition());
                if (AngleBetweenTwoEdgesDeg(MI, candidates[i]) < smallestAngle)
                {
                    smallestAngle = AngleBetweenTwoEdgesDeg(MI, candidates[i]);
                }
            }
            List<PolygonEdge> SmallestAngleEdges = new List<PolygonEdge>();
            for (int i = 0; i < candidates.Length; i++)
            {
                if (Math.Abs(AngleBetweenTwoEdgesDeg(MI, candidates[i]) - smallestAngle) < Mathf.Epsilon)
                {
                    SmallestAngleEdges.Add(candidates[i]);
                }
            }

            VisiblePoint = NearestVisiblePoint(SmallestAngleEdges);
        }

        Vector3 VisibleDup = new Vector3(VisiblePoint.x, 0f, VisiblePoint.z);
        Vector3 MaxXDup = new Vector3(MaxX.x, 0f, MaxX.z);
        LinkedListNode<PolygonVertex> outerCurrent = outer.GetVertices().First;
        LinkedListNode<PolygonVertex> innerCurrent = obstacle.GetVertices().First;
        while (!innerCurrent.Value.GetPosition().Equals(MaxX))
        {
            innerCurrent = innerCurrent.Next;
        }
        for (int i = 0; i < outer.GetVertices().Count; i++)
        {
            if (!outerCurrent.Value.GetPosition().Equals(VisiblePoint))
            {
                NewVertices.Add(outerCurrent.Value.GetPosition());
                outerCurrent = outerCurrent.Next;
            }
            else
            {
                if (!Onetime)
                {
                    NewVertices.Add(outerCurrent.Value.GetPosition());
                    outerCurrent = outerCurrent.Next;
                    continue;
                }
                NewVertices.Add(outerCurrent.Value.GetPosition());
                for (int j = 0; j < obstacle.GetVertices().Count; j++)
                {
                    NewVertices.Add(innerCurrent.Value.GetPosition());
                    if (innerCurrent.Next != null)
                    {
                        innerCurrent = innerCurrent.Next;
                    }
                    else
                    {
                        innerCurrent = obstacle.GetVertices().First;
                    }
                }
                NewVertices.Add(MaxXDup);
                NewVertices.Add(VisibleDup);
                outerCurrent = outerCurrent.Next;
                Onetime = false;
            }
        }
        return NewVertices;
    }

    private static Polygon FindMostRight(List<Polygon> polygons)
    {
        Polygon ToReturn = new Polygon(new List<Vector3>(), true);
        float MaxXValue = Mathf.NegativeInfinity;
        foreach (Polygon polygon in polygons)
        {
            if (polygon.PointMaxX().x > MaxXValue)
            {
                ToReturn = polygon;
                MaxXValue = polygon.PointMaxX().x;
            }
        }
        return ToReturn;
    }
}
