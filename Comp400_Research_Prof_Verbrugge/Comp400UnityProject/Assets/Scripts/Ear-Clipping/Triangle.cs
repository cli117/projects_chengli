﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class Triangle
{
    List<Vector3> vertices;
    List<PolygonEdge> edges;
    float area;
    static float EPSILON = 0.0001f;

    public Triangle(List<Vector3> vertices)
    {
        this.vertices = vertices;
        edges = new List<PolygonEdge>
        {
            new PolygonEdge(vertices[0], vertices[1]),
            new PolygonEdge(vertices[1], vertices[2]),
            new PolygonEdge(vertices[2], vertices[0])
        };
        area = Mathf.Abs((vertices[0].x * (vertices[1].z - vertices[2].z)) + (vertices[1].x * (vertices[2].z - vertices[0].z)) + (vertices[2].x * (vertices[0].z - vertices[1].z))) / 2;
    }

    public Triangle(Vector3 v0, Vector3 v1, Vector3 v2)
    {
        vertices = new List<Vector3>
        {
            v0,
            v1,
            v2
        };
        edges = new List<PolygonEdge>
        {
            new PolygonEdge(vertices[0], vertices[1]),
            new PolygonEdge(vertices[1], vertices[2]),
            new PolygonEdge(vertices[2], vertices[0])
        };
        area = Mathf.Abs((v0.x * (v1.z - v2.z)) + (v1.x * (v2.z - v0.z)) + (v2.x * (v0.z - v1.z))) / 2;
    }

    public bool IsWithin(Vector3 point)
    {
        Triangle a = new Triangle(point, vertices[0], vertices[1]);
        Triangle b = new Triangle(point, vertices[1], vertices[2]);
        Triangle c = new Triangle(point, vertices[0], vertices[2]);
        return Math.Abs(a.GetArea() + b.GetArea() + c.GetArea() - this.GetArea()) < EPSILON;
    }

    public bool IsBelong(Vector3 point)
    {
        return (Math.Abs(vertices[0].x - point.x) < EPSILON && Math.Abs(vertices[0].z - point.z) < EPSILON) || (Math.Abs(vertices[1].x - point.x) < EPSILON && Math.Abs(vertices[1].z - point.z) < EPSILON) || (Math.Abs(vertices[2].x - point.x) < EPSILON && Math.Abs(vertices[2].z - point.z) < EPSILON);
    }

    public List<Vector3> GetVertices()
    {
        return vertices;
    }

    public float GetArea()
    {
        return area;
    }

    public List<PolygonEdge> GetEdges()
    {
        return edges;
    }

    override
    public string ToString()
    {
        string ToReturn = "v0: " + vertices[0].ToString("F4");
        ToReturn += " v1: " + vertices[1].ToString("F4");
        ToReturn += " v2: " + vertices[2].ToString("F4");
        return ToReturn;
    }

    public bool Same(Triangle other)
    {
        int count = 0;
        foreach (Vector3 thisV in this.vertices)
        {
            foreach (Vector3 otherV in other.vertices)
            {
                if (thisV.Equals(otherV))
                {
                    count++;
                }
            }
        }
        if (count == 3) return true;
        return false;
    }
}
