﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnCostText : MonoBehaviour
{
    [SerializeField] GameObject scoreText;
    // Start is called before the first frame update
    void Start()
    {
        Instantiate(scoreText, new Vector3(371.5f, 336.5f, 0f), Quaternion.identity).transform.parent = transform;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
