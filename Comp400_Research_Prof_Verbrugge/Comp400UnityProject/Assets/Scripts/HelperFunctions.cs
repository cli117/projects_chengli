﻿using System;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public static class HelperFunctions
{
    public static float GetDistanceBetweenTwoVector3(Vector3 a, Vector3 b)
    {
        float xDifference = Mathf.Abs(a.x - b.x);
        float zDifference = Mathf.Abs(a.z - b.z);
        return Mathf.Sqrt(Mathf.Pow(xDifference, 2) + Mathf.Pow(zDifference, 2));
    }

    public static void InstantiateBoundary(Vector3 a, Vector3 b)
    {
        float Length = GetDistanceBetweenTwoVector3(a, b);
        GameObject boundary = GameObject.CreatePrimitive(PrimitiveType.Cube);
        boundary.transform.position = new Vector3((a.x + b.x) / 2, 0f, (a.z + b.z) / 2);
        boundary.transform.localScale = new Vector3(Length, 0.1f, 0.1f);
        boundary.name = "Boundary";
        boundary.GetComponent<MeshRenderer>().material.color = Color.black;
        boundary.gameObject.tag = "ObstacleBoundary";
        Vector3 directionVector = b - a;
        float angle = Mathf.Atan(directionVector.z / directionVector.x);
        float rotation = -Mathf.Rad2Deg * angle;
        if (double.IsNaN(rotation))
        {
            rotation = 0;
        }
        boundary.transform.Rotate(0f, rotation, 0f);
        boundary.AddComponent<NavMeshObstacle>().carving = true;
    }

    public struct Line
    {
        public Vector3 p;
        public Vector3 q;
        public float k;
        public float b;
        public float length;
        public bool IsUpLeft;

        public Line(Vector3 p, Vector3 q)
        {
            this.p = p;
            this.q = q;
            k = (q.z - p.z) / (q.x - p.x);
            b = p.z - (k * p.x);
            Vector3 vector = q - p;
            float xDiff = Mathf.Abs(p.x - q.x);
            float zDiff = Mathf.Abs(p.z - q.z);
            length = Mathf.Sqrt(Mathf.Pow(xDiff, 2) + Mathf.Pow(zDiff, 2));
            if (k > 0) IsUpLeft = true;
            else IsUpLeft = false; 
        }

        public Line(float k, float b)
        {
            this.k = k;
            this.b = b;
            this.p = new Vector3(10000f, 0f, 10000f * k + b);
            this.q = new Vector3(-10000f, 0f, -10000 * k + b);
            length = Mathf.Infinity;
            if (k > 0) IsUpLeft = true;
            else IsUpLeft = false;
        }

        public bool IsIntersect(Line other)
        {
            if (System.Math.Abs(this.k - other.k) < Mathf.Epsilon)
            {
                return false;
            }
            if (this.Straddle(other) && other.Straddle(this))
            {
                return true;
            }
            return false;
        }

        private bool Straddle(Line other)
        {
            Vector3 v1 = other.p - this.p;
            Vector3 v2 = other.q - this.p;
            Vector3 vm = this.q - this.p;
            if (VectorCrossProduct(v1, vm) * VectorCrossProduct(v2, vm) <= 0)
            {
                return true;
            }
            return false;
        }
    }

    public struct BoundaryPoint
    {
        public Vector3 position;
        public Vector3 nextPos;
        public bool done;

        public BoundaryPoint(Vector3 position, float FOVDiameter, Vector3 v1, Vector3 v2, Vector3[] boundary)
        {
            this.position = position;
            float dotProduct = -v1.x * v2.x + -v1.z * v2.z;
            float SineTheta = Mathf.Sin(Mathf.Acos(dotProduct));
            float vectorLength = FOVDiameter / SineTheta;
            nextPos = position + vectorLength * (-v1 + v2);

            if (!MapManager.IsInBoundary(nextPos, boundary, true))
            {
                nextPos = position - vectorLength * (-v1 + v2);
            }

            int samePoint = 0;
            done = false;
            for (int i = 0; i < boundary.Length; i++)
            {
                float d = GetDistanceBetweenTwoVector3(position, boundary[i]);
//                Debug.Log(d);
                if (d < 3f && d > 0f)
                {
                    done = true;
                    break;
                }
                else if (Math.Abs(d) < Mathf.Epsilon) samePoint++;
            }
            if (samePoint > 1) done = true;

            //if (done) nextPos = position;

        }
    }

    public static bool IsInBoundaryBound(Vector3 point, MapManager.Edge edge)
    {
        Line ToCheck = new Line(edge.p, edge.q);
        if (System.Math.Abs(ToCheck.k) < Mathf.Epsilon)
        {
            return (point.x >= edge.p.x && point.x <= edge.q.x) || (point.x >= edge.q.x && point.x <= edge.p.x);
        }
        else if (ToCheck.k == Mathf.Infinity)
        {
            return (point.z >= edge.p.z && point.z <= edge.q.z) || (point.z >= edge.q.z && point.z <= edge.p.z);
        }
        float normalK = -1f / ToCheck.k;
        float normalB = point.z - point.x * normalK;
        Line Normal = new Line(normalK, normalB);
        if (ToCheck.IsIntersect(Normal)) return true;
        return false;
    }

    public static float GetDistanceBetweenPointAndEdge(Vector3 point, MapManager.Edge edge)
    {
        Line ToCheck = new Line(edge.p, edge.q);
        if (System.Math.Abs(ToCheck.k) < Mathf.Epsilon)
        {
            return Mathf.Abs(point.z - edge.p.z);
        }
        else if (ToCheck.k == Mathf.Infinity)
        {
            return Mathf.Abs(point.x - edge.p.x);
        }
        float Distance = -1;
        Distance = Mathf.Abs((ToCheck.k * point.x - point.z + ToCheck.b) / Mathf.Sqrt(Mathf.Pow(ToCheck.k, 2) + 1));
        return Distance;
    }

    public static List<Vector3> AvailablePoints(Vector3[] Boundary, MapManager.Edge edge, float distance, float offset, out bool IsEnd)
    {
        List<Vector3> points = new List<Vector3>();
        Line ToCheck = new Line(edge.p, edge.q);
        if (System.Math.Abs(ToCheck.k) < Mathf.Epsilon)
        {
            Vector3 offsetVec = edge.UnitDirectionVector * offset;
            Vector3 checkPoint = new Vector3((ToCheck.p.x + ToCheck.q.x) / 2, 0, (ToCheck.p.z + ToCheck.q.z) / 2);
            Vector3 pointUp = new Vector3(checkPoint.x, 0, checkPoint.z + distance);
            Vector3 pointDown = new Vector3(checkPoint.x, 0, checkPoint.z - distance);
            bool up = MapManager.IsInBoundary(pointUp, Boundary, true);
            if (up)
            {
                points.Add(pointUp);
                Vector3 save = new Vector3(pointUp.x, 0f, pointUp.z);
                while (true)
                {
                    pointUp -= offsetVec;
                    if (!IsInBoundaryBound(pointUp, edge)) break;
                    if (MapManager.IsInBoundary(pointUp, Boundary, true)) points.Add(pointUp);
                }

                pointUp = save;
                points.Reverse();

                while (true)
                {
                    pointUp += offsetVec;
                    if (!IsInBoundaryBound(pointUp, edge)) break;
                    if (MapManager.IsInBoundary(pointUp, Boundary, true)) points.Add(pointUp);
                }
            }

            else
            {
                points.Add(pointDown);
                Vector3 save = new Vector3(pointDown.x, 0f, pointDown.z);
                while (true)
                {
                    pointDown -= offsetVec;
                    if (!IsInBoundaryBound(pointDown, edge)) break;
                    if (MapManager.IsInBoundary(pointDown, Boundary, true)) points.Add(pointDown);
                }

                pointDown = save;
                points.Reverse();

                while (true)
                {
                    pointDown += offsetVec;
                    if (!IsInBoundaryBound(pointDown, edge)) break;
                    if (MapManager.IsInBoundary(pointDown, Boundary, true)) points.Add(pointDown);
                }
            }

            bool IsEndLocal = false;
            IsEnd = IsEndLocal;
            return points;
        }

        if (ToCheck.k == Mathf.Infinity || ToCheck.k == -Mathf.Infinity)
        {
            Vector3 offsetVec = edge.UnitDirectionVector * offset;
            Vector3 checkPoint = new Vector3((ToCheck.p.x + ToCheck.q.x) / 2, 0, (ToCheck.p.z + ToCheck.q.z) / 2);
            Vector3 pointRight = new Vector3(checkPoint.x + distance, 0, checkPoint.z);
            Vector3 pointLeft = new Vector3(checkPoint.x - distance, 0, checkPoint.z);
            bool right = MapManager.IsInBoundary(pointRight, Boundary, true);
            if (right)
            {
                points.Add(pointRight);
                Vector3 save = new Vector3(pointRight.x, 0f, pointRight.z);
                while (true)
                {
                    pointRight -= offsetVec;
                    if (!IsInBoundaryBound(pointRight, edge)) break;
                    if (MapManager.IsInBoundary(pointRight, Boundary, true)) points.Add(pointRight);
                }

                pointRight = save;
                points.Reverse();

                while (true)
                {
                    pointRight += offsetVec;
                    if (!IsInBoundaryBound(pointRight, edge)) break;
                    if (MapManager.IsInBoundary(pointRight, Boundary, true)) points.Add(pointRight);
                }

            }

            else
            {
                points.Add(pointLeft);
                Vector3 save = new Vector3(pointLeft.x, 0f, pointLeft.z);
                while (true)
                {
                    pointLeft -= offsetVec;
                    if (!IsInBoundaryBound(pointLeft, edge)) break;
                    if (MapManager.IsInBoundary(pointLeft, Boundary, true)) points.Add(pointLeft);
                }

                pointLeft = save;
                points.Reverse();

                while (true)
                {
                    pointLeft += offsetVec;
                    if (!IsInBoundaryBound(pointLeft, edge)) break;
                    if (MapManager.IsInBoundary(pointLeft, Boundary, true)) points.Add(pointLeft);
                }
            }

            bool IsEndLocal = false;
            IsEnd = IsEndLocal;
            return points;
        }

        Vector3 offsetVec_ = edge.UnitDirectionVector * offset;
        float deltaX = Mathf.Abs(distance * ToCheck.k * Mathf.Sqrt(1 / (1 + Mathf.Pow(ToCheck.k, 2))));
        float deltaZ = Mathf.Abs(deltaX / ToCheck.k);
        Vector3 checkPoint_ = new Vector3((ToCheck.p.x + ToCheck.q.x) / 2, 0, (ToCheck.p.z + ToCheck.q.z) / 2);
        Vector3 pointUp_;
        if (ToCheck.IsUpLeft)
        {
            pointUp_ = new Vector3(checkPoint_.x - deltaX, 0, checkPoint_.z + deltaZ);
        }
        else
        {
            pointUp_ = new Vector3(checkPoint_.x + deltaX, 0, checkPoint_.z + deltaZ);
        }
        bool up_ = MapManager.IsInBoundary(pointUp_, Boundary, true);
        if (up_)
        {
            points.Add(pointUp_);
            Vector3 save = new Vector3(pointUp_.x, pointUp_.y, pointUp_.z);
            while (true)
            {
                pointUp_ -= offsetVec_;
                if (!IsInBoundaryBound(pointUp_, edge)) break;
                if (MapManager.IsInBoundary(pointUp_, Boundary, true)) points.Add(pointUp_);
            }
            points.Reverse();
            pointUp_ = save;
            while (true)
            {
                pointUp_ += offsetVec_;
                if (!IsInBoundaryBound(pointUp_, edge)) break;
                if (MapManager.IsInBoundary(pointUp_, Boundary, true)) points.Add(pointUp_);
            }
        }

        else
        {
            Vector3 pointDown_;
            if (ToCheck.IsUpLeft)
            {
                pointDown_ = new Vector3(checkPoint_.x + deltaX, 0, checkPoint_.z - deltaZ);
            }
            else
            {
                pointDown_ = new Vector3(checkPoint_.x - deltaX, 0, checkPoint_.z - deltaZ);
            }

            points.Add(pointDown_);
            Vector3 save = new Vector3(pointDown_.x, pointDown_.y, pointDown_.z);
            while (true)
            {
                pointDown_ -= offsetVec_;
                if (!IsInBoundaryBound(pointDown_, edge)) break;
                if (MapManager.IsInBoundary(pointDown_, Boundary, true)) points.Add(pointDown_);
            }

            points.Reverse();
            pointDown_ = save;

            while (true)
            {
                pointDown_ += offsetVec_;
                if (!IsInBoundaryBound(pointDown_, edge)) break;
                if (MapManager.IsInBoundary(pointDown_, Boundary, true)) points.Add(pointDown_);
            }
        }

        bool IsEndLocal_ = false;
        //foreach (Vector3 point in points)
        //{
        //    IsEndLocal_ |= (Double.IsNaN(point.x) || Double.IsNaN(point.z));
        //    if (IsEndLocal_) break;
        //    GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
        //    cube.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
        //    cube.transform.position = point;
        //    cube.GetComponent<Renderer>().material.color = Color.black;
        //    Debug.Log(point);
        //}
        IsEnd = IsEndLocal_;
        return points;
    }

    public static float VectorCrossProduct(Vector3 a, Vector3 b)
    {
        return a.x * b.z - b.x * a.z;
    }
}
