﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class MapSelection : MonoBehaviour
{
    Dropdown dropdown;
    MapManager mapManager;
    // Start is called before the first frame update
    void Start()
    {
        dropdown = GetComponent<Dropdown>();
        mapManager = GameObject.Find("MapManager").GetComponent<MapManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (dropdown.value == 0)
        {
            return;
        }

        if (dropdown.value == 1)
        {
            mapManager.SetMapType(MapManager.MapType.simpleConvex);
        }

        if (dropdown.value == 2)
        {
            mapManager.SetMapType(MapManager.MapType.simpleNonConvex);
        }

        if (dropdown.value == 3)
        {
            mapManager.SetMapType(MapManager.MapType.convexWithHole);
        }

        if (dropdown.value == 4)
        {
            mapManager.SetMapType(MapManager.MapType.nonConvexWithHole);
        }

        if (dropdown.value == 5)
        {
            mapManager.SetMapType(MapManager.MapType.nonConvexWithDifferentType);
        }
    }
}
