﻿using System;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public class MapManager : MonoBehaviour
{

    public enum MapType
    {
        simpleConvex,
        simpleNonConvex,
        convexWithHole,
        nonConvexWithHole,
        nonConvexWithDifferentType
    }

    public enum EdgeDirection
    {
        Up, Down, Left, Right, UpLeft, UpRight, DownLeft, DownRight
    }

    public struct Edge
    {
        public Vector3 p;
        public Vector3 q;
        public float length;
        public Vector3 UnitDirectionVector;
        public EdgeDirection direction;

        private float tolerance;

        public Edge(Vector3 p, Vector3 q)
        {
            tolerance = 0.1f;
            this.p = p;
            this.q = q;
            float xDiff = Mathf.Abs(p.x - q.x);
            float zDiff = Mathf.Abs(p.z - q.z);
            length = Mathf.Sqrt(Mathf.Pow(xDiff, 2) + Mathf.Pow(zDiff, 2));
            UnitDirectionVector = new Vector3((q - p).x/length, 0, (q - p).z/length);
            if (Math.Abs(UnitDirectionVector.x) < tolerance && Math.Abs(UnitDirectionVector.z - 1) < tolerance)
            {
                direction = EdgeDirection.Up;
            }
            else if (UnitDirectionVector.x > tolerance && UnitDirectionVector.z > tolerance)
            {
                direction = EdgeDirection.UpRight;
            }
            else if (Math.Abs(UnitDirectionVector.x - 1) < tolerance && Math.Abs(UnitDirectionVector.z) < tolerance)
            {
                direction = EdgeDirection.Right;
            }
            else if (UnitDirectionVector.x > tolerance && UnitDirectionVector.z < tolerance)
            {
                direction = EdgeDirection.DownRight;
            }
            else if (Math.Abs(UnitDirectionVector.x) < tolerance && Math.Abs(UnitDirectionVector.z + 1) < tolerance)
            {
                direction = EdgeDirection.Down;
            }
            else if (UnitDirectionVector.x < tolerance && UnitDirectionVector.z < tolerance)
            {
                direction = EdgeDirection.DownLeft;
            }
            else if (Math.Abs(UnitDirectionVector.x + 1) < tolerance && Math.Abs(UnitDirectionVector.z) < tolerance)
            {
                direction = EdgeDirection.Left;
            }
            else
            {
                direction = EdgeDirection.UpLeft;
            }
        }

        public Vector3 Intersect(Edge edge)
        {
            HelperFunctions.Line lineA = new HelperFunctions.Line(this.p, this.q);
            HelperFunctions.Line lineB = new HelperFunctions.Line(edge.p, edge.q);
            float x = (lineB.b - lineA.b) / (lineA.k - lineB.k);
            float z = lineA.k * x + lineA.b;
            return new Vector3(x, 0, z);
        }
    }
    [SerializeField] GameObject FogOfWar;
    [Range(0.2f, 2f)]
    [SerializeField] float GridSize = 0.5f;
    [SerializeField] GameObject LineRendererPrefab;
    [SerializeField] MapType mapType = MapType.simpleConvex;

    [SerializeField] Vector3[] BoundaryPointsAsArray = new Vector3[0];

    Vector3[] SpawnPostions;
    Vector3 SpawnPosition;
    Vector3 LastFramePosition;
    Vector3 LastInBoundaryPosition;
    // Clockwise
    List<Edge> Edges;

    LinkedList<Vector3> BoundaryPointsAsLinkedList;
    #region Vars, Getter&Setter
    GameObject CharacterInstance;
    MapType currentMap;
    Vector3[] currentPoints;
    LineRenderer lineRenderer;
    bool[] isFirstTime;
    Vector3[][] mapData;
    LinkedList<MapData.Obstacle> obstacles;
    bool MapNotChanged;
    bool IsFirstTime;
    bool MapDrawn;
    bool IsDone;
    List<GameObject> FogOfWars;
    float currentGridSize;
    float currentCost;
    float currentPercentage;
    int DestroyedFOW;

    public MapType GetMapType()
    {
        return mapType;
    }

    public float GetCurrentCost()
    {
        return currentCost;
    }


    public void SetMapType(MapType mapType)
    {
        this.mapType = mapType;
    }

    public void SetGridSize(float newGridSize)
    {
        GridSize = newGridSize;
    }

    public List<GameObject> GetFogOfWars()
    {
        return FogOfWars;
    }

    public float GetCurrentPercentage()
    {
        return currentPercentage;
    }

    public float GetGridSize()
    {
        return GridSize;
    }

    public void IncreaseDestroyed()
    {
        DestroyedFOW++;
    }

    public List<Edge> GetEdges()
    {
        return Edges;
    }

    public bool GetMapDrawn()
    {
        return MapDrawn;
    }

    public Vector3[] GetBoundaryPoints()
    {
        return BoundaryPointsAsArray;
    }

    public bool GetIsDone()
    {
        return IsDone;
    }

    public LinkedList<MapData.Obstacle> GetObstacles()
    {
        return obstacles;
    }
    #endregion

    struct MaximumPoints
    {
        public Vector3 MostLeft;
        public Vector3 MostRight;
        public Vector3 MostTop;
        public Vector3 MostBottom;
        public MaximumPoints(Vector3 left, Vector3 right, Vector3 top, Vector3 bottom)
        {
            MostLeft = left;
            MostRight = right;
            MostTop = top;
            MostBottom = bottom;
        }
    }
    MaximumPoints maximumPoints;
    // Start is called before the first frame update
    void Awake()
    {
        InitializeVariables();
        SpawnLineGenerator();
    }

    private void Start()
    {
       
    }
    #region Variables
    private void InitializeVariables()
    {
        BoundaryPointsAsLinkedList = new LinkedList<Vector3>();
        currentMap = MapType.nonConvexWithDifferentType;
        currentPoints = new Vector3[999];
        isFirstTime = new bool[5] { true, true, true, true, true };
        mapData = new Vector3[5][]
        {
            MapData.SimpleConvexBoundaryPoints(),
            MapData.SimpleNonConvexBoundaryPoints(),
            MapData.ConvexWithHoleBoundaryPoints(),
            MapData.NonConvexPointsWithHoleBoundaryPoints(),
            MapData.NonConvexPointsWithDifferentTypeBoundaryPoints()
        };
        CharacterInstance = GameObject.Find("Character");
        obstacles = new LinkedList<MapData.Obstacle>();
        MapNotChanged = false;
        FogOfWars = new List<GameObject>();
        currentGridSize = -1f;
        currentCost = 0;
        SpawnPostions = MapData.CharacterSpawnPositions();
        SpawnPosition = new Vector3(0f, 0f, 0f);
        LastFramePosition = new Vector3(0f, 0f, 0f);
        LastInBoundaryPosition = new Vector3(0f, 0f, 0f);
        IsFirstTime = true;
        currentPercentage = 0f;
        DestroyedFOW = 0;
        Edges = new List<Edge>();
        MapDrawn = false;
        IsDone = false;
    }

    private void SpawnLineGenerator()
    {
        GameObject newLineGen = Instantiate(LineRendererPrefab);
        lineRenderer = newLineGen.GetComponent<LineRenderer>();
    }
    #endregion
    // Update is called once per frame
    void Update()
    {
        if (Math.Abs(currentPercentage - 1) < Mathf.Epsilon) IsDone = true; 
        MapNotChanged = currentMap == mapType && BoundaryPointsAsArray.SequenceEqual(currentPoints) && Mathf.Abs(currentGridSize - GridSize) < Mathf.Epsilon;
        // If nothing to change, return
        if (MapNotChanged) 
        {
            UpdateCost();
            LimitPosition();
            UpdatePercentage();
            return;
        }

        Edges.Clear();
        currentCost = 0;
        IsFirstTime = true;
        switch (mapType)
        {
            case MapType.simpleConvex:
                DrawSimpleConvex();
                MapDrawn = true;
                break;
            case MapType.simpleNonConvex:
                DrawSimpleNonConvex();
                MapDrawn = true;
                break;
            case MapType.convexWithHole:
                DrawConvexWithHole();
                MapDrawn = true;
                break;
            case MapType.nonConvexWithHole:
                DrawNonConvexWithHole();
                MapDrawn = true;
                break;
            case MapType.nonConvexWithDifferentType:
                DrawNonConvexWithDifferentType();
                MapDrawn = true;
                break;
        }
    }
    #region DrawAccordingly
    private void DrawSimpleConvex()
    {
        ExtractData(0);
        DrawBoundary();
        SpawnCharacter();
        DrawFogOfWar();
        UpdateData(MapType.simpleConvex);
    }

    private void DrawSimpleNonConvex()
    {
        ExtractData(1);
        DrawBoundary();
        SpawnCharacter();
        DrawFogOfWar();
        UpdateData(MapType.simpleNonConvex);
    }

    private void DrawConvexWithHole()
    {
        ExtractData(2);
        DrawBoundary();
        DrawObstacles();
        DrawFogOfWar();
        SpawnCharacter();
        UpdateData(MapType.convexWithHole);
    }

    private void DrawNonConvexWithHole()
    {
        ExtractData(3);
        DrawBoundary();
        DrawObstacles();
        DrawFogOfWar();
        SpawnCharacter();
        UpdateData(MapType.nonConvexWithHole);
    }

    private void DrawNonConvexWithDifferentType()
    {
        ExtractData(4);
        DrawBoundary();
        //TODO: take obstacles back into consideration
        //DrawObstacles();
        DrawFogOfWar();
        SpawnCharacter();
        UpdateData(MapType.nonConvexWithDifferentType);
    }
    #endregion
    private void ExtractData(int index)
    {
        GameObject[] obstaclesBoundaries = GameObject.FindGameObjectsWithTag("ObstacleBoundary");
        if (obstaclesBoundaries.Length != 0)
        {
            for (int i = 0; i < obstaclesBoundaries.Length; i++)
            {
                Destroy(obstaclesBoundaries[i].gameObject);
            }
        }

        DestroyedFOW = 0;
        // If selected map is not modified yet
        if (isFirstTime[index])
        {
            // Get the default map data
            BoundaryPointsAsArray = mapData[index];
            // W/o this, boundary points can not be modified
            isFirstTime[index] = false;
        }
        if (BoundaryPointsAsArray.Length == 0) { return; }
        // Put the data into linkedlist
        BoundaryPointsAsLinkedList.Clear();
        for (int i = 0; i < BoundaryPointsAsArray.Length; i++)
        {
            // Store the boundary in a linked list
            BoundaryPointsAsLinkedList.AddLast(BoundaryPointsAsArray[i]);
        }
        // Add the starting point to make it circular
        BoundaryPointsAsLinkedList.AddLast(BoundaryPointsAsArray[0]);

        // W/o this, modifications can not be saved
        //if (BoundaryPointsAsArray.SequenceEqual(mapData[index])) { isFirstTime[index] = true; }
    }

    private void DrawBoundary()
    {
        if (BoundaryPointsAsArray.Length == 0) { return; }
        lineRenderer.positionCount = BoundaryPointsAsLinkedList.Count;
        int index = 0;
        foreach (Vector3 point in BoundaryPointsAsLinkedList)
        {
            lineRenderer.SetPosition(index, point);
            index++;
        }

        for (int i = 0; i < BoundaryPointsAsArray.Length; i++)
        {
            if (i < BoundaryPointsAsArray.Length - 1)
            {
                float newX = BoundaryPointsAsArray[i + 1].x - BoundaryPointsAsArray[i].x;
                float newZ = BoundaryPointsAsArray[i + 1].z - BoundaryPointsAsArray[i].z;
                Edge NewEdge = new Edge(BoundaryPointsAsArray[i], BoundaryPointsAsArray[i + 1]);
                Edges.Add(NewEdge);
            }
            else
            {
                float newX = BoundaryPointsAsArray[0].x - BoundaryPointsAsArray[i].x;
                float newZ = BoundaryPointsAsArray[0].z - BoundaryPointsAsArray[i].z;
                Edge NewEdge = new Edge(BoundaryPointsAsArray[i], BoundaryPointsAsArray[0]);
                Edges.Add(NewEdge);
            }
        }
    }

    private void UpdateData(MapType newType)
    {
        // Update selected map to current map
        if (BoundaryPointsAsArray.Length == 0) { return; }
        currentPoints = new Vector3[BoundaryPointsAsArray.Length];
        for (int i = 0; i < currentPoints.Length; i++)
        {
            currentPoints[i] = BoundaryPointsAsArray[i];
        }
        currentMap = newType;
        currentGridSize = GridSize;
    }

    private void DrawObstacles()
    {
        int index = 0;
        // Draw according to points
        for (int i = 0; i < MapData.GetObstacles(mapType).Length; i++)
        {
            foreach (Vector3 point in MapData.GetObstacles(mapType)[i].BoundaryPoints)
            {
                int next = index + 1;
                if (next == MapData.GetObstacles(mapType)[i].BoundaryPoints.Count)
                {
                    next = 0;
                }
                HelperFunctions.InstantiateBoundary(point, MapData.GetObstacles(mapType)[i].BoundaryPoints.ElementAt(next));
                index++;
            }
            index = 0;
        }
    }


    public struct FOWWithPos
    {
        public GameObject FOW;
        public int x;
        public int y;
        public FOWWithPos(GameObject fow, int xPos, int yPos)
        {
            FOW = fow;
            x = xPos;
            y = yPos;
        }
    }



    public List<FOWWithPos> FOWWithPosition = new List<FOWWithPos>();
    public List<int> ColumnLog = new List<int>();

    private void DrawFogOfWar()
    {
        foreach (GameObject fow in FogOfWars)
        {
            Destroy(fow.gameObject);
        }
        FogOfWars = new List<GameObject>();
        maximumPoints = GetCurrentMaximumPoints();
        int numOfColumns = Mathf.RoundToInt((maximumPoints.MostRight.x - maximumPoints.MostLeft.x) / GridSize);
        int numOfRows = Mathf.RoundToInt((maximumPoints.MostTop.z - maximumPoints.MostBottom.z) / GridSize);
        for (int i = 0; i < numOfRows; i++)
        {
            int CurrentX = 0;
            for (int j = 0; j < numOfColumns; j++)
            {
                Vector3 pointToCheck = new Vector3(maximumPoints.MostLeft.x + (j * GridSize), 0f, maximumPoints.MostBottom.z + (i * GridSize));
                bool IsInObstacleBool = false;
                if (mapType != MapType.simpleConvex && mapType != MapType.simpleNonConvex)
                {
                    int numOfObstacles = MapData.GetObstacles(mapType).Length;
                    for (int k = 0; k < numOfObstacles; k++)
                    {
                        IsInObstacleBool |= IsInObstacle(pointToCheck, MapData.GetObstacles(mapType)[k]);
                    }
                }
                if (IsInBoundary(pointToCheck, BoundaryPointsAsArray, false) && !IsInObstacleBool)
                {
                    GameObject temp = Instantiate(FogOfWar, pointToCheck, Quaternion.identity);
                    FogOfWars.Add(temp);
                    FOWWithPos ToAdd = new FOWWithPos(temp, CurrentX++, i);
                    FOWWithPosition.Add(ToAdd);
                    //TODO: do not destroy, only for clearance of tracing 
                   //Destroy(temp);
                }
            }
            ColumnLog.Add(CurrentX);
        }
    }

    #region Boundary
    public static bool IsInBoundary(Vector3 point, Vector3[] BoundaryPoints, bool IsNeedEdge)
    {
        for (int j = 0; j < BoundaryPoints.Length; j++)
        {
            float k, b;
            if (j == 0)
            {
                Vector3 p1 = BoundaryPoints[BoundaryPoints.Length - 1];
                Vector3 p2 = BoundaryPoints[0];
                k = (p2.z - p1.z) / (p2.x - p1.x);
                b = p1.z - (p1.x * k);

                if (Math.Abs(k) < Mathf.Epsilon)
                {
                    if ((Math.Abs(point.z - p1.z) < Mathf.Epsilon) && ((point.x < p1.x && point.x > p2.x) || (point.x > p1.x && point.x < p2.x)))
                    {
                        return IsNeedEdge;
                    }
                }

                else if (float.IsPositiveInfinity(k) || float.IsNegativeInfinity(k))
                {
                    if ((Math.Abs(point.x - p1.x) < Mathf.Epsilon) && ((point.z < p1.z && point.z > p2.z) || (point.z > p1.z && point.z < p2.z)))
                    {
                        return IsNeedEdge;
                    }
                }

                else if (Math.Abs(point.x * k + b - point.z) < 0.1f)
                {
                    if ((point.x < p1.x && point.x > p2.x) || (point.x > p1.x && point.x < p2.x))
                    {
                        return IsNeedEdge;
                    }
                }
            }
            else
            {
                Vector3 p1 = BoundaryPoints[j - 1];
                Vector3 p2 = BoundaryPoints[j];
                k = (p2.z - p1.z) / (p2.x - p1.x);
                b = p1.z - (p1.x * k);

                if (Math.Abs(k) < Mathf.Epsilon)
                {
                    if ((Math.Abs(point.z - p1.z) < Mathf.Epsilon) && ((point.x < p1.x && point.x > p2.x) || (point.x > p1.x && point.x < p2.x)))
                    {
                        return IsNeedEdge;
                    }
                }

                else if (float.IsPositiveInfinity(k) || float.IsNegativeInfinity(k))
                {
                    if ((Math.Abs(point.x - p1.x) < Mathf.Epsilon) && ((point.z < p1.z && point.z > p2.z) || (point.z > p1.z && point.z < p2.z)))
                    {
                        return IsNeedEdge;
                    }
                }

                else if (Math.Abs(point.x * k + b - point.z) < 0.1f)
                {
                    if ((point.x < p1.x && point.x > p2.x) || (point.x > p1.x && point.x < p2.x))
                    {
                        return IsNeedEdge;
                    }
                }
            }
        }
        int BoundaryLength = BoundaryPoints.Length, i = 0;
        bool inside = false;
        float pointX = point.x, pointZ = point.z;
        float startX, startZ, endX, endZ;
        Vector3 endpoint = BoundaryPoints[BoundaryLength - 1];
        endX = endpoint.x;
        endZ = endpoint.z;
        while(i < BoundaryLength)
        {
            startX = endX;
            startZ = endZ;
            endpoint = BoundaryPoints[i++];
            endX = endpoint.x;
            endZ = endpoint.z;
            inside ^= (endZ > pointZ ^ startZ > pointZ) && ((pointX - endX) < (pointZ - endZ) * (startX - endX) / (startZ - endZ));
        }
        return inside;
    }

    public bool IsInObstacle(Vector3 point, MapData.Obstacle obstacle)
    {
        int BoundaryLength = obstacle.BoundaryPoints.Count, i = 0;
        bool inside = false;
        float pointX = point.x, pointZ = point.z;
        float startX, startZ, endX, endZ;
        Vector3 endpoint = obstacle.BoundaryPoints.ElementAt(BoundaryLength - 1);
        endX = endpoint.x;
        endZ = endpoint.z;
        while(i < BoundaryLength)
        {
            startX = endX;
            startZ = endZ;
            endpoint = obstacle.BoundaryPoints.ElementAt(i++);
            endX = endpoint.x;
            endZ = endpoint.z;
            inside ^= (endZ > pointZ ^ startZ > pointZ) && ((pointX - endX) < (pointZ - endZ) * (startX - endX) / (startZ - endZ));
        }
        return inside;
    }

    private MaximumPoints GetCurrentMaximumPoints()
    {
        return new MaximumPoints(FindMostLeftPoint(BoundaryPointsAsArray), FindMostRightPoint(BoundaryPointsAsArray), FindMostTopPoint(BoundaryPointsAsArray), FindMostBottomPoint(BoundaryPointsAsArray));
    }

    public Vector3 FindMostLeftPoint(Vector3[] BoundaryPointsAsArray)
    {
        Vector3[] boundarySortByX = BoundaryPointsAsArray.OrderBy(v => v.x).ToArray<Vector3>();
        return boundarySortByX[0];
    }

    public Vector3 FindMostRightPoint(Vector3[] BoundaryPointsAsArray)
    {
        Vector3[] boundarySortByX = BoundaryPointsAsArray.OrderBy(v => v.x).ToArray<Vector3>();
        return boundarySortByX[boundarySortByX.Length - 1];
    }

    public Vector3 FindMostTopPoint(Vector3[] BoundaryPointsAsArray)
    {
        Vector3[] boundarySortByZ = BoundaryPointsAsArray.OrderBy(v => v.z).ToArray<Vector3>();
        return boundarySortByZ[boundarySortByZ.Length - 1];
    }

    public Vector3 FindMostBottomPoint(Vector3[] BoundaryPointsAsArray)
    {
        Vector3[] boundarySortByZ = BoundaryPointsAsArray.OrderBy(v => v.z).ToArray<Vector3>();
        return boundarySortByZ[0];
    }

    private void LimitPosition()
    {
        if (!IsInBoundary(CharacterInstance.transform.position, BoundaryPointsAsArray, true))
        {
            CharacterInstance.transform.position = LastInBoundaryPosition;
        }
        else
        {
            LastInBoundaryPosition = CharacterInstance.transform.position;
        }
    }
    #endregion

    private void SpawnCharacter()
    {
        switch (mapType)
        {
            case (MapManager.MapType.simpleConvex):
                CharacterInstance.transform.position = SpawnPostions[0];
                SpawnPosition = SpawnPostions[0];
                break;
            case (MapManager.MapType.simpleNonConvex):
                CharacterInstance.transform.position = SpawnPostions[1];
                SpawnPosition = SpawnPostions[1];
                break;
            case (MapManager.MapType.convexWithHole):
                CharacterInstance.transform.position = SpawnPostions[2];
                SpawnPosition = SpawnPostions[2];
                break;
            case (MapManager.MapType.nonConvexWithHole):
                CharacterInstance.transform.position = SpawnPostions[3];
                SpawnPosition = SpawnPostions[3];
                break;
            case (MapManager.MapType.nonConvexWithDifferentType):
                CharacterInstance.transform.position = SpawnPostions[4];
                SpawnPosition = SpawnPostions[4];
                break;
        }
    }
    #region Cost Related
    private void UpdateCost()
    {
        if (IsFirstTime)
        {
            LastFramePosition = SpawnPosition;
            IsFirstTime = false;
        }
        currentCost += HelperFunctions.GetDistanceBetweenTwoVector3(CharacterInstance.transform.position, LastFramePosition);
        LastFramePosition = CharacterInstance.transform.position;
    }

    public void IncreaseCostByTileType(float ExtraCost)
    {
        currentCost += ExtraCost;
    }
    #endregion

    private void UpdatePercentage()
    {
        currentPercentage = (float)DestroyedFOW / (float)FogOfWars.Count;
    }
}
