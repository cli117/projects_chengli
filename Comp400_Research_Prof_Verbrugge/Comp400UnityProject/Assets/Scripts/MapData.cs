﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public static class MapData
{
    public static Vector3[] SimpleConvexBoundaryPoints()
    {
        return new Vector3[6]
        {
            new Vector3(-11f, 0f, -5f),
            new Vector3(-8f, 0f, 4f),
            new Vector3(0f, 0f, 5f),
            new Vector3(9f, 0f, 4f),
            new Vector3(10f, 0f, 0f),
            new Vector3(8f, 0f, -4f)
            };
    }

    public static Vector3[] SimpleNonConvexBoundaryPoints()
    {
        return new Vector3[8]
        {
            new Vector3(-11f, 0f, -5f),
            new Vector3(-11f, 0f, 4.5f),
            new Vector3(3f, 0f, 2f),
            new Vector3(8f, 0f, 2f),
            new Vector3(12f, 0f, 4.5f),
            new Vector3(12f, 0f, -5f),
            new Vector3(8f, 0f, -2f),
            new Vector3(3f, 0f, -2f)
        };
    }

    public static Vector3[] ConvexWithHoleBoundaryPoints()
    {
        return new Vector3[6]
        {
            new Vector3(-11f, 0f, -5f),
            new Vector3(-8f, 0f, 4f),
            new Vector3(0f, 0f, 5f),
            new Vector3(9f, 0f, 4f),
            new Vector3(10f, 0f, 0f),
            new Vector3(8f, 0f, -4f)
            };
    }

    public static Vector3[] NonConvexPointsWithHoleBoundaryPoints()
    {
        return new Vector3[8]
        {
            new Vector3(-11f, 0f, -5f),
            new Vector3(-11f, 0f, 4.5f),
            new Vector3(3f, 0f, 2f),
            new Vector3(8f, 0f, 2f),
            new Vector3(11f, 0f, 4.5f),
            new Vector3(11f, 0f, -5f),
            new Vector3(8f, 0f, -2f),
            new Vector3(3f, 0f, -2f)
        };
    }

    public static Vector3[] NonConvexPointsWithDifferentTypeBoundaryPoints()
    {
        return new Vector3[9]
        {
            new Vector3(-11f, 0f, -5f),
            new Vector3(-8f, 0f, 4f),
            new Vector3(-6f, 0f, 5f),
            new Vector3(-5f, 0f, 0f),
            new Vector3(-3f, 0f, 0f),
            new Vector3(0f, 0f, 4f),
            new Vector3(8f, 0f, 4f),
            new Vector3(8f, 0f, -4f),
            new Vector3(6, 0, -5)
        };
    }

    public static Vector3[] CharacterSpawnPositions()
    {
        return new Vector3[5]
        {
            new Vector3(-10.5f, 0.1f, -4.5f),
            new Vector3(-10.5f, 0.1f, -4.5f),
            new Vector3(-10.5f, 0.1f, -4.5f),
            new Vector3(-10.5f, 0.1f, -4.5f),
            new Vector3(-10.5f, 0.1f, -4.5f)
        };
    }

    public struct Obstacle
    {
        public LinkedList<Vector3> BoundaryPoints;
        public Obstacle(LinkedList<Vector3> boundary)
        {
            BoundaryPoints = boundary;
        }
    }

    public static Obstacle[] GetObstacles(MapManager.MapType mapType)
    {
        if (mapType == MapManager.MapType.simpleConvex || mapType == MapManager.MapType.simpleNonConvex)
        {
            return new Obstacle[0];
        }
        else if (mapType == MapManager.MapType.convexWithHole)
        {
            LinkedList<Vector3> Obstacle1Boundary = new LinkedList<Vector3>();
            Obstacle1Boundary.AddLast(new Vector3(-3f, 0f, -2f));
            Obstacle1Boundary.AddLast(new Vector3(-3f, 0f, -1f));
            Obstacle1Boundary.AddLast(new Vector3(-2f, 0f, -1f));
            Obstacle1Boundary.AddLast(new Vector3(-2f, 0f, -2f));
            Obstacle1Boundary.AddLast(new Vector3(-3f, 0f, -2f));
            Obstacle obstacle1 = new Obstacle(Obstacle1Boundary);

            LinkedList<Vector3> Obstacle2Boundary = new LinkedList<Vector3>();
            Obstacle2Boundary.AddLast(new Vector3(3f, 0f, 3f));
            Obstacle2Boundary.AddLast(new Vector3(3f, 0f, 2f));
            Obstacle2Boundary.AddLast(new Vector3(2f, 0f, 2f));
            Obstacle2Boundary.AddLast(new Vector3(2f, 0f, 3f));
            Obstacle2Boundary.AddLast(new Vector3(3f, 0f, 3f));
            Obstacle obstacle2 = new Obstacle(Obstacle2Boundary);

            LinkedList<Vector3> Obstacle3Boundary = new LinkedList<Vector3>();
            Obstacle3Boundary.AddLast(new Vector3(0f, 0f, 0f));
            Obstacle3Boundary.AddLast(new Vector3(1f, 0f, 0f));
            Obstacle3Boundary.AddLast(new Vector3(1f, 0f, 1f));
            Obstacle3Boundary.AddLast(new Vector3(0f, 0f, 1f));
            Obstacle3Boundary.AddLast(new Vector3(0f, 0f, 0f));
            Obstacle obstacle3 = new Obstacle(Obstacle3Boundary);

            return new Obstacle[] { obstacle1, obstacle2, obstacle3 };
        }
        else if (mapType == MapManager.MapType.nonConvexWithHole)
        // TODO Change values to fit non-convex
        {
            LinkedList<Vector3> Obstacle1Boundary = new LinkedList<Vector3>();
            Obstacle1Boundary.AddLast(new Vector3(-6f, 0f, -3f));
            Obstacle1Boundary.AddLast(new Vector3(-4f, 0f, -1f));
            Obstacle1Boundary.AddLast(new Vector3(-2f, 0f, -1f));
            Obstacle1Boundary.AddLast(new Vector3(-2f, 0f, -2f));
            Obstacle1Boundary.AddLast(new Vector3(-6f, 0f, -3f));
            Obstacle obstacle1 = new Obstacle(Obstacle1Boundary);

            LinkedList<Vector3> Obstacle2Boundary = new LinkedList<Vector3>();
            Obstacle2Boundary.AddLast(new Vector3(-5f, 0f, 3f));
            Obstacle2Boundary.AddLast(new Vector3(-5f, 0f, 2f));
            Obstacle2Boundary.AddLast(new Vector3(-4f, 0f, 2f));
            Obstacle2Boundary.AddLast(new Vector3(-4f, 0f, 3f));
            Obstacle2Boundary.AddLast(new Vector3(-5f, 0f, 3f));
            Obstacle obstacle2 = new Obstacle(Obstacle2Boundary);

            LinkedList<Vector3> Obstacle3Boundary = new LinkedList<Vector3>();
            Obstacle3Boundary.AddLast(new Vector3(9f, 0f, 1f));
            Obstacle3Boundary.AddLast(new Vector3(8f, 0f, 1f));
            Obstacle3Boundary.AddLast(new Vector3(7f, 0f, 0f));
            Obstacle3Boundary.AddLast(new Vector3(8f, 0f, 0f));
            Obstacle3Boundary.AddLast(new Vector3(9f, 0f, 1f));
            Obstacle obstacle3 = new Obstacle(Obstacle3Boundary);

            return new Obstacle[] { obstacle1, obstacle2, obstacle3 };
        }
        else
        {
            LinkedList<Vector3> Obstacle1Boundary = new LinkedList<Vector3>();
            Obstacle1Boundary.AddLast(new Vector3(-6f, 0f, -3f));
            Obstacle1Boundary.AddLast(new Vector3(-4f, 0f, -1f));
            Obstacle1Boundary.AddLast(new Vector3(-2f, 0f, -1f));
            Obstacle1Boundary.AddLast(new Vector3(-2f, 0f, -2f));
            Obstacle1Boundary.AddLast(new Vector3(-6f, 0f, -3f));
            Obstacle obstacle1 = new Obstacle(Obstacle1Boundary);

            LinkedList<Vector3> Obstacle2Boundary = new LinkedList<Vector3>();
            Obstacle2Boundary.AddLast(new Vector3(-5f, 0f, 3f));
            Obstacle2Boundary.AddLast(new Vector3(-5f, 0f, 2f));
            Obstacle2Boundary.AddLast(new Vector3(-4f, 0f, 2f));
            Obstacle2Boundary.AddLast(new Vector3(-4f, 0f, 3f));
            Obstacle2Boundary.AddLast(new Vector3(-5f, 0f, 3f));
            Obstacle obstacle2 = new Obstacle(Obstacle2Boundary);

            LinkedList<Vector3> Obstacle3Boundary = new LinkedList<Vector3>();
            Obstacle3Boundary.AddLast(new Vector3(9f, 0f, 1f));
            Obstacle3Boundary.AddLast(new Vector3(8f, 0f, 1f));
            Obstacle3Boundary.AddLast(new Vector3(7f, 0f, 0f));
            Obstacle3Boundary.AddLast(new Vector3(8f, 0f, 0f));
            Obstacle3Boundary.AddLast(new Vector3(9f, 0f, 1f));
            Obstacle obstacle3 = new Obstacle(Obstacle3Boundary);

            return new Obstacle[] { obstacle1, obstacle2, obstacle3 };
        }
    }

    public static int[] GetCircleNums(MapManager.MapType maptype)
    {
        int[] ToReturn = new int[0];

        switch (maptype)
        {
            case MapManager.MapType.convexWithHole:
                ToReturn = new int[]
                {
                    3,
                    6,
                    1,
                    1,
                    4,
                    2,
                    3,
                    5,
                    5
                };
                break;
            case MapManager.MapType.simpleConvex:
                ToReturn = new int[]
                {
                    11
                };
                break;
        }

        return ToReturn;
    }
}