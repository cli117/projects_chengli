﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FogOfWar : MonoBehaviour
{
    MapManager mapManager;
    private float Cost;
    // Start is called before the first frame update
    void Start()
    {
        mapManager = GameObject.Find("MapManager").GetComponent<MapManager>();
        Cost = Random.Range(0f, 0.5f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PlayerFOV"))
        {
            Destroy(this.gameObject);
            mapManager.IncreaseDestroyed();
            if (mapManager.GetMapType() == MapManager.MapType.nonConvexWithDifferentType)
            {
                mapManager.IncreaseCostByTileType(Cost);
            }
        }
    }
    //private void OnCollisionEnter(Collision collision)
    //{
    //    if (collision.gameObject.CompareTag("PlayerFOV"))
    //    {
    //        Destroy(this.gameObject);
    //        mapManager.IncreaseDestroyed();
    //        if (mapManager.GetMapType() == MapManager.MapType.nonConvexWithDifferentType)
    //        {
    //            mapManager.IncreaseCostByTileType(Cost);
    //        }
    //    }
    //}
}
