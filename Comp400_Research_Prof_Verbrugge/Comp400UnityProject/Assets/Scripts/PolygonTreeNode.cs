﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class PolygonTreeNode
{
    public Polygon polygon;
    public List<PolygonTreeNode> AdjacentPolygons;

    public PolygonTreeNode(Polygon polygon)
    {
        this.polygon = polygon;
        AdjacentPolygons = new List<PolygonTreeNode>();
    }

    public void AddToAdjacent(PolygonTreeNode adjacent)
    {
        AdjacentPolygons.Add(adjacent);
    }
}