﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Triangulation : MonoBehaviour
{
    List<Triangle> delaunay = new List<Triangle>();
    List<Polygon> polygons = new List<Polygon>();
    Polygon p;
    Polygon x;
    Polygon z;
    Polygon q;
    // Start is called before the first frame update
    void Awake()
    {
        Vector3[] v = {
            new Vector3(-11f, 0f, -5f),
            new Vector3(-8f, 0f, 4f),
            new Vector3(0f, 0f, 5f),
            new Vector3(9f, 0f, 4f),
            new Vector3(10f, 0f, 0f),
            new Vector3(8f, 0f, -4f)
        };
        Vector3[] inner = {
        new Vector3(-3f, 0f, -2f),
        new Vector3(-3f, 0f, -1f),
        new Vector3(-2f, 0f, -1f),
        new Vector3(-2f, 0f, -2f)
    };

        Vector3[] inner2 = {
        new Vector3(3f, 0f, 3f),
        new Vector3(3f, 0f, 2f),
        new Vector3(2f, 0f, 2f),
        new Vector3(2f, 0f, 3f)
    };

        Vector3[] inner3 ={
        new Vector3(0f, 0f, 0f),
        new Vector3(1f, 0f, 0f),
        new Vector3(1f, 0f, 1f),
        new Vector3(0f, 0f, 1f)

    };
        p = new Polygon(v, true);
        q = new Polygon(inner, false);
        z = new Polygon(inner2, false);
        x = new Polygon(inner3, false);

        p = HertelMehlhorn.ReverseIfNotCounterClockWise(p);
        q = HertelMehlhorn.ReverseIfNotClockWise(q);
        z = HertelMehlhorn.ReverseIfNotClockWise(z);
        x = HertelMehlhorn.ReverseIfNotClockWise(x);
        LinkedListNode<PolygonVertex> currentNode = p.GetVertices().First;
        //for (int i = 0; i < p.GetVertices().Count; i++)
        //{
        //    print(currentNode.Value.GetPosition());
        //    currentNode = currentNode.Next;
        //}

        polygons = new List<Polygon>
        {
            q,
            x,
            z
        };

        List<Triangle> test = EarClipping.EarClipWithHole(p, polygons);

        delaunay = Delaunay.DelaunayTriangles(test);

        polygons = HertelMehlhorn.MergeTriangles(delaunay);

    }


    private float LengthOfAVector(Vector3 a)
    {
        float xs = Mathf.Pow(a.x, 2);
        float zs = Mathf.Pow(a.z, 2);
        return Mathf.Sqrt(xs + zs);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        foreach (Polygon polygon in polygons)
        {
            LinkedListNode<PolygonVertex> current = polygon.GetVertices().First;
            for (int i = 0; i < polygon.GetVertices().Count; i++)
            {
                if (current.Next != null)
                {
                    Gizmos.DrawLine(current.Value.GetPosition(), current.Next.Value.GetPosition());
                    current = current.Next;
                }
                else
                {
                    Gizmos.DrawLine(current.Value.GetPosition(), polygon.GetVertices().First.Value.GetPosition());
                }
            }
        }

        //foreach (Triangle triangle in delaunay)
        //{
        //    for (int i = 0; i < 3; i++)
        //    {
        //        if (i != 2)
        //        {
        //            Gizmos.DrawLine(triangle.GetVertices()[i], triangle.GetVertices()[i + 1]);
        //        }
        //        else
        //        {
        //            Gizmos.DrawLine(triangle.GetVertices()[i], triangle.GetVertices()[0]);
        //        }
        //    }
        //}
    }
}
