﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MethodSelection : MonoBehaviour
{
    Dropdown dropdown;
    Simulator simulator;
    bool Launch = false;
    // Start is called before the first frame update
    void Start()
    {
        dropdown = GetComponent<Dropdown>();
        simulator = GameObject.Find("Simulator").GetComponent<Simulator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!Launch)
        {
            simulator.PauseAll();
            return;
        }

        if (dropdown.value == 1 && Launch)
        {
            simulator.LaunchGreedy();
        }

        else if (dropdown.value == 3 && Launch)
        {
            simulator.LaunchZigZag();
        }

        else if(dropdown.value == 2 && Launch)
        {
            simulator.LaunchTracing();
        }
    }

    public void SetLaunch()
    {
        Launch = !Launch;
    }
}
