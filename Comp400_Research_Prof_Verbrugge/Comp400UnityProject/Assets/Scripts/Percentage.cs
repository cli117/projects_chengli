﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Percentage : MonoBehaviour
{
    Text costText;
    MapManager mapManager;
    // Start is called be
    void Start()
    {
        costText = GetComponent<Text>();
        mapManager = GameObject.Find("MapManager").GetComponent<MapManager>();
    }

    // Update is called once per frame
    void Update()
    {
        costText.text = "Percentage: " + (mapManager.GetCurrentPercentage() * 100) + "%";
    }
}
