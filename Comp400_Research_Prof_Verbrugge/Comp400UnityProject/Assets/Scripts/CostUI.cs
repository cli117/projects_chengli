﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class CostUI : MonoBehaviour
{
    Text costText;
    MapManager mapManager;
    // Start is called be
    void Start()
    {
        costText = GetComponent<Text>();
        mapManager = GameObject.Find("MapManager").GetComponent<MapManager>();
        //MapManager.Edge a = new MapManager.Edge(new Vector3(-Mathf.Sqrt(3), 0, -1), new Vector3(0, 0, 0));
        //MapManager.Edge b = new MapManager.Edge(new Vector3(0, 0, 0), new Vector3(Mathf.Sqrt(3), 0, -1));
        //Vector3 v1 = a.UnitDirectionVector;
        //Vector3 v2 = b.UnitDirectionVector;
        //Debug.Log((-v1).ToString("F4"));
        //print(v2);
        //HelperFunctions.BoundaryPoint test = new HelperFunctions.BoundaryPoint(new Vector3(0, 0, 0), 1, v1, v2);

    }

    // Update is called once per frame
    void Update()
    {
        costText.text = "Current Cost: " + mapManager.GetCurrentCost();
    }
}
